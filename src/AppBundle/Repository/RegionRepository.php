<?php

namespace AppBundle\Repository;

use AppBundle\Exception\{
    NullHashException, WrongExpressionException
};
use Doctrine\ORM\{
    NonUniqueResultException, Query, QueryBuilder
};

/**
 * RegionRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class RegionRepository extends AbstractStatefulRepository
{

    /**
     * @return Query
     */
    public function getListQuery(): Query
    {
        return $this
            ->findCommon()
            ->getQuery();
    }

    /**
     * @return QueryBuilder
     */
    public function findCommon(): QueryBuilder
    {
        return $this->getEntityManager()->createQueryBuilder()
            ->select('r')
            ->from('AppBundle:Region', 'r');
    }

    /**
     * @return array
     */
    public function getList(): array
    {
        return $this->getListQuery()->getResult();
    }

    /**
     * @return string
     *
     * @throws WrongExpressionException
     * @throws NullHashException
     * @throws NonUniqueResultException
     */
    public function getHash(): string
    {
        $qb = $this
            ->findCommon()
            ->select('r.id', 'r.countryId', 'r.title');

        return $this->getTableHash($qb);
    }

}
