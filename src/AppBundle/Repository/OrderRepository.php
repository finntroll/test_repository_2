<?php

namespace AppBundle\Repository;

use AppBundle\Entity\{
    User, UserOrder
};
use Doctrine\ORM\{
    EntityRepository, QueryBuilder, Query
};

/**
 * UserOrderRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class OrderRepository extends EntityRepository
{

    use TransferableUserRepositoryTrait;

    /**
     * @param string $alias
     * @return QueryBuilder
     */
    public function prepareQueryBuilder(string $alias): QueryBuilder
    {
        return $this->createQueryBuilder($alias);
    }

    /**
     * @param int $userId
     *
     * @return QueryBuilder
     */
    public function findByUserId(int $userId): QueryBuilder
    {
        return $this->prepareQueryBuilder('userOrder')
            ->andWhere('userOrder.userId = :userId')
            ->setParameter('userId', $userId)
            ->orderBy('userOrder.date', 'DESC');
    }

    /**
     * @param int $userId
     * @param int $orderId
     *
     * @return QueryBuilder
     */
    public function findByUserIdAndOrderId(int $userId, int $orderId): QueryBuilder
    {
        $qb = $this->findByUserId($userId);

        return $qb->andWhere('userOrder.id = :orderId')
            ->setParameter('orderId', $orderId);
    }

}
