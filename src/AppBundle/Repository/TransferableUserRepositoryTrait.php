<?php

namespace AppBundle\Repository;
use AppBundle\Entity\User;

/**
 * Trait TransferableUserRepositoryTrait
 *
 * @package AppBundle\Repository
 */
trait TransferableUserRepositoryTrait
{

    /**
     * @param User   $userFrom
     * @param User   $userTo
     */
    public function transferBetweenUsers(User $userFrom, User $userTo): void
    {
        $this->createQueryBuilder('table')
            ->update()
            ->set('table.userId', $userTo->getId())
            ->where('table.user = :user')
            ->setParameter('user', $userFrom)
            ->getQuery()
            ->execute();
    }

}