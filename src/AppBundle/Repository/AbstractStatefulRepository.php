<?php

namespace AppBundle\Repository;

use AppBundle\Exception\{
    NullHashException, WrongExpressionException
};
use AppBundle\Repository\Interfaces\{
    HashableInterface, ListPaginatedInterface
};
use Doctrine\ORM\{
    EntityRepository, NonUniqueResultException, Query\Expr\Select, QueryBuilder
};

/**
 * Class AbstractRepository
 * @package AppBundle\Repository
 */
abstract class AbstractStatefulRepository extends EntityRepository implements HashableInterface, ListPaginatedInterface
{

    /**
     * @param QueryBuilder $qb
     *
     * @return string
     *
     * @throws NullHashException
     * @throws WrongExpressionException
     * @throws NonUniqueResultException
     */
    protected function getTableHash(QueryBuilder $qb): string
    {
        if (\count($qb->getDQLPart('select')) !== 1) {
            throw new WrongExpressionException('Wrong select');
        }

        /** @var Select $selectPart */
        $selectPart = $qb->getDQLPart('select')[0];
        $select = implode(', ', $selectPart->getParts());

        $qb->select("SUM(CRC32(CONCAT_WS('', {$select}))) AS hash");

        $result = $qb->getQuery()->getOneOrNullResult();

        if ($result === null) {
            throw new NullHashException('No data to generate hash string');
        }

        return md5($result['hash']);
    }

}
