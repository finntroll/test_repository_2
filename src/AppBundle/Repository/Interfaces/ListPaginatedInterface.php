<?php
namespace AppBundle\Repository\Interfaces;

use Doctrine\ORM\Query;

/**
 * Interface ListPaginatedInterface
 * @package AppBundle\Repository\Interfaces
 */
interface ListPaginatedInterface
{

    /**
     * @return Query
     */
    public function getListQuery(): Query;

    /**
     * @return array
     */
    public function getList(): array;

}