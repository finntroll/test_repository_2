<?php

namespace AppBundle\Repository\Interfaces;

/**
 * Interface HashableInterface
 * @package AppBundle\Repository\Interfaces
 */
interface HashableInterface
{
    /**
     * @return string
     */
    public function getHash(): string;

}