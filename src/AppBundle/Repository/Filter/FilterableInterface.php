<?php

namespace AppBundle\Repository\Filter;

use AppBundle\Filter\FilterInterface;
use Doctrine\ORM\QueryBuilder;

interface FilterableInterface
{
    public function applyFilter(QueryBuilder $query, FilterInterface $filter);
}
