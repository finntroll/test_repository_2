<?php

namespace AppBundle\Repository;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductReview;
use AppBundle\Entity\User;
use AppBundle\Filter\FilterInterface;
use AppBundle\Repository\Filter\FilterableTrait;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ProductReviewRepository
 *
 * @package AppBundle\Repository
 */
class ProductReviewRepository extends \Doctrine\ORM\EntityRepository
{

    use FilterableTrait;

    /**
     * @param Product              $product
     * @param FilterInterface|null $filter
     * @param User|null            $user
     *
     * @return QueryBuilder
     */
    public function findApprovedByProduct(Product $product, FilterInterface $filter = null, ?User $user = null): QueryBuilder
    {
        if ($model = $product->getProductModel()) {
            return $this->queryApprovedByFilter($filter, $user)
                ->andWhere('pr.product IN (:products)')
                ->setParameter('products', $model->getProduct()->toArray());
        }

        return $this->queryApprovedByFilter($filter, $user)
            ->andWhere('pr.product = :product')
            ->setParameter('product', $product);
    }


    /**
     * @param FilterInterface|null $filter
     *
     * @param User|null            $user
     * @return QueryBuilder
     */
    public function queryApprovedByFilter(FilterInterface $filter = null, ?User $user): QueryBuilder
    {
        $qb = $this->createQueryBuilder('pr');

        if ($user !== null) {
            $qb->where('pr.user = :user')
                ->setParameter('user', $user);
        }

        $qb->orWhere('pr.status = :status')
            ->andWhere('pr.status NOT IN (:deleted)')
            ->setParameter('status', ProductReview::STATUS_APPROVED)
            ->setParameter('deleted', [ProductReview::STATUS_DELETED, ProductReview::STATUS_SPAM]);

        if ($filter !== null) {
            $this->applyFilter($qb, $filter, 'pr');
        }

        $qb->addOrderBy('pr.id', 'DESC');

        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     * @param int          $rating
     * @param string       $alias
     * @return QueryBuilder
     */
    public function filterByRating(QueryBuilder $qb, int $rating, string $alias): QueryBuilder
    {
        return $qb->andWhere($alias . '.productRating = :product_rating')
            ->setParameter('product_rating', $rating);
    }

}
