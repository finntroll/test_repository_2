<?php

namespace AppBundle\Repository;


//use AppBundle\Entity\Product;
use AppBundle\Entity\Category;
use AppBundle\Entity\ProductModel;
use AppBundle\Filter\FilterInterface;
use Doctrine\ORM\EntityRepository;
use AppBundle\Repository\Filter\FilterableTrait;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\Validator\Constraints\DateTime;

/**
 * Class ProductModelRepository
 *
 * @package AppBundle\Repository
 */
class ProductModelRepository extends EntityRepository
{
    use FilterableTrait;

    /**
     * @param string $alias
     *
     * @return QueryBuilder
     */
    public function prepareQueryBuilder(string $alias): QueryBuilder
    {
        return $this->createQueryBuilder($alias);
    }

    /**
     * @param FilterInterface|null $filter
     *
     * @return QueryBuilder
     */
    public function queryApprovedByFilter(FilterInterface $filter = null): QueryBuilder
    {
        $qb = $this->prepareQueryBuilder('productModel')
            ->leftJoin('productModel.category', 'category')
            ->andWhere('productModel.active = :active')
            ->andWhere('productModel.status = :status')
            ->andWhere('productModel.moderation = :moderation')
            ->andWhere('productModel.inStock = :inStock')
            ->andWhere('category.status = :categoryStatus')
            ->setParameters([
                                'active'         => ProductModel::STATUS_ACTIVE,
                                'status'         => ProductModel::STATUS_ACTIVE,
                                'moderation'     => ProductModel::MODERATION_CONFIRMED,
                                'inStock'        => ProductModel::PRODUCT_IN_STOCK,
                                'categoryStatus' => Category::STATUS_PUBLISHED,
                            ]);

        if ($filter) {
            $this->applyFilter($qb, $filter, 'productModel');
        }

        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     * @param integer      $categoryId
     * @param string       $alias
     * @return QueryBuilder
     */
    public function filterByCategoryId(QueryBuilder $qb, $categoryId, $alias)
    {
        return $qb->andWhere($alias . '.category = :category')
            ->setParameter('category', $categoryId);
    }

    /**
     * @param QueryBuilder $qb
     * @param  boolean     $inStock
     * @param  string      $alias
     * @return QueryBuilder
     */
    public function filterByIsInStock(QueryBuilder $qb, $inStock, $alias)
    {
        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     * @param  boolean     $isChinaProduct
     * @param  string      $alias
     * @return QueryBuilder
     */
    public function filterByIsChinaProduct(QueryBuilder $qb, $isChinaProduct, $alias)
    {
        return $qb->andWhere($alias . '.isChinaProduct = :isChinaProduct')
            ->setParameter('isChinaProduct', $isChinaProduct);
    }

    /**
     * @param QueryBuilder $qb
     * @param float        $price
     * @param string       $alias
     * @return QueryBuilder
     */
    public function filterByFromPrice(QueryBuilder $qb, $price, $alias)
    {
        return $qb->andWhere($alias . '.minPrice >= :priceFrom')
            ->setParameter('priceFrom', $price);
    }

    /**
     * @param QueryBuilder $qb
     * @param float        $price
     * @param string       $alias
     * @return QueryBuilder
     */
    public function filterByToPrice(QueryBuilder $qb, $price, $alias)
    {
        return $qb->andWhere($alias . '.minPrice <= :priceTo')
            ->setParameter('priceTo', $price);
    }

    /**
     * @param QueryBuilder $qb
     * @param int[]        $producers
     * @param string       $alias
     * @return QueryBuilder
     */
    public function filterByProducers(QueryBuilder $qb, $producers, $alias)
    {
        return $qb->andWhere($alias . '.producer IN (:producers)')
            ->setParameter('producers', $producers);
    }

    /**
     * @param QueryBuilder $qb
     * @param array        $attributes
     * @param string       $alias
     * @return QueryBuilder
     */
    public function filterByAttributes(QueryBuilder $qb, $attributes, $alias)
    {
        $qb->leftJoin($alias . '.product', 'prod');
        foreach ($attributes as $params) {
            $key      = $params['id'];
            $newAlias = 'n_a_' . $key;
            switch ($params['type']) {
                case 'checkbox':
                    $value      = $params['checkbox'];
                    $falseArray = ['false', 'no', 0, '0', 'n', false];

                    if (\is_string($value)) {
                        $value = strtolower($value);
                    }

                    $value = \in_array($value, $falseArray, true) ? false : true;

                    $qb->leftJoin('prod.attributes', $newAlias)
                        ->andWhere($newAlias . '.attributeId = :' . $newAlias . '_id')
                        ->andWhere($newAlias . '.value = :' . $newAlias . '_value')
                        ->setParameter($newAlias . '_id', $key)
                        ->setParameter($newAlias . '_value', $value);
                    break;
                case 'between':
                    $value = $params['between'];
                    $qb->leftJoin('prod.attributes', $newAlias)
                        ->andWhere($newAlias . '.attributeId = :' . $newAlias . '_id')
                        ->andWhere($newAlias . '.value > :' . $newAlias . '_from')
                        ->andWhere($newAlias . '.value < :' . $newAlias . '_to')
                        ->setParameter($newAlias . '_id', $key)
                        ->setParameter($newAlias . '_from', (int)$value['from'])
                        ->setParameter($newAlias . '_to', (int)$value['to']);
                    break;
                case 'option':
                    $value = $params['option'];
                    $qb->leftJoin('prod.attributes', $newAlias)
                        ->andWhere($newAlias . '.attributeId = :' . $newAlias . '_id')
                        ->andWhere($newAlias . '.value = :' . $newAlias . '_value')
                        ->setParameter($newAlias . '_id', $key)
                        ->setParameter($newAlias . '_value', $value);
                    break;
                case 'options':
                    $value = $params['options'];
                    $qb->leftJoin('prod.attributes', $newAlias)
                        ->andWhere($newAlias . '.attributeId = :' . $newAlias . '_id')
                        ->andWhere($newAlias . '.value IN (:' . $newAlias . '_value)')
                        ->setParameter($newAlias . '_id', $key)
                        ->setParameter($newAlias . '_value', $value);
                    break;
                default:
                    break;
            }
        }

        return $qb;
    }

    /**
     * @param QueryBuilder $qb
     * @param string       $name
     * @param string       $alias
     *
     * @return QueryBuilder
     */
    public function filterByName(QueryBuilder $qb, string $name, string $alias): QueryBuilder
    {
        return $qb->andWhere('MATCH_AGAINST(' . $alias . '.name, :name) > 0.0')
            ->setParameter('name', trim($name));
    }


    /**
     * @param int  $id
     * @param int  $limit
     * @param int  $offset
     * @param bool $lastModified
     *
     * @return array
     */
    public function getModelsDataByLimitOffset(int $id, int $limit = 100, int $offset = 0, $lastModified = false)
    {
        $qb =$this->getEntityManager()
            ->createQueryBuilder()
            ->select('pm')
            ->from('AppBundle:ProductModel', 'pm')
            ->where('pm.id >= (:id)')
            ->setParameters([
                                'id' => $id
                            ]);

        if ($lastModified) {
            $modificationDate = new \DateTime();
            $modificationDate->modify('-1 day');

            $qb->andWhere('pm.updateTime >= :date')
                ->setParameter('date' ,$modificationDate);
        }

        return $qb->setFirstResult($offset)
            ->setMaxResults($limit)
            ->orderBy('pm.id', 'ASC')
            ->getQuery()
            ->getResult();
    }

    /**
     * @param QueryBuilder $qb
     * @param array        $sorting
     * @param string       $alias
     * @return QueryBuilder
     */
    public function filterBySorting(QueryBuilder $qb, array $sorting, string $alias): QueryBuilder
    {
        if ($sorting['param'] === 'name_sorting') {
            $sorting['param'] = 'name';
        }

        if ($sorting['param'] === 'price_total') {
            $sorting['param'] = 'price';
        }

        if ($sorting['param'] === 'update_time') {
            $sorting['param'] = 'updateTime';
        }

        return $qb->orderBy($alias . '.' . $sorting['param'], $sorting['direction']);
    }
}
