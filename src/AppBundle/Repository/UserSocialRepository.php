<?php

namespace AppBundle\Repository;

use AppBundle\Entity\UserSocial;

/**
 * Class UserSocialRepository
 *
 * @package AppBundle\Repository
 */
class UserSocialRepository extends \Doctrine\ORM\EntityRepository
{
    /**
     * @param $uid
     * @param $provider
     * @return null|UserSocial
     */
    public function getSocialAccountByUidAndProvider($uid, $provider)
    {
        /** @noinspection PhpIncompatibleReturnTypeInspection */
        return $this->findOneBy(['uid' => $uid, 'provider' => $provider], ['uid' => 'ASC']);
    }

}
