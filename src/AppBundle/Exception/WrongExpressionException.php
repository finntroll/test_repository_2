<?php

namespace AppBundle\Exception;

/**
 * Class WrongExpressionException
 *
 * @package AppBundle\Exception
 */
class WrongExpressionException extends \RuntimeException implements AppBundleExceptionInterface
{
    /**
     * @var string
     */
    protected $message = 'Wrong hash-expression.';

    /**
     * @var int
     */
    protected $code = 500;

}