<?php

namespace AppBundle\Exception\User;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class DuplicateUserException
 *
 * @package AppBundle\Exception\User
 */
class HttpDuplicateUserException extends HttpException
{
    /**
     * @var string
     */
    protected $message = 'Duplicate conflict: user already exists. ';

    /**
     * @inheritdoc
     */
    public function getStatusCode()
    {
        return Response::HTTP_CONFLICT;
    }

}