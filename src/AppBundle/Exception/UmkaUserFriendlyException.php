<?php
/**
 * Created by PhpStorm.
 * User: valkeru
 * Date: 03.07.18
 * Time: 15:29
 */

namespace AppBundle\Exception;


use Throwable;

class UmkaUserFriendlyException extends \Exception
{
    /**
     * UmkaUserFriendlyException constructor.
     *
     * @param string    $message
     * @param int       $code
     * @param Throwable $previous
     */
    public function __construct(string $message = 'Request failed. Please contact support',
                                int $code = 454,
                                Throwable $previous = null
    )
    {
        parent::__construct($message, $code, $previous);
    }

}
