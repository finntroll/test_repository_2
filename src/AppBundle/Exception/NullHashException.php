<?php

namespace AppBundle\Exception;

/**
 * Class NullHashException
 *
 * @package AppBundle\Exception
 */
class NullHashException extends \RuntimeException implements \AppBundle\Exception\AppBundleExceptionInterface
{

    /**
     * @var string
     */
    protected $message = 'Hash query returned null';

    /**
     * @var int
     */
    protected $code = 500;

}