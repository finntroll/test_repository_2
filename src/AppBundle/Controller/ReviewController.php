<?php

namespace AppBundle\Controller;

use AppBundle\Entity\{
    CommentImage, Product, ProductReview
};
use AppBundle\Filter\ProductReviewFilter;
use AppBundle\Form\ProductReview\ReviewForm;
use AppBundle\Manager\CommentImageManager;
use AppBundle\Manager\ReviewManager;
use AppBundle\Parameters\ParametersNaming;
use AppBundle\Repository\ProductReviewRepository;
use Exception;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\{
    Route, Security
};
use Symfony\Component\DependencyInjection\Exception\{
    ServiceCircularReferenceException, ServiceNotFoundException
};
use Symfony\Component\Form\Exception\AlreadySubmittedException;
use Symfony\Component\HttpFoundation\{
    JsonResponse, Request
};
use Symfony\Component\HttpKernel\Exception\{
    AccessDeniedHttpException, BadRequestHttpException, NotFoundHttpException
};

/**
 * Class ReviewController
 *
 * @package AppBundle\Controller
 *
 * @Route("/reviews")
 */
class ReviewController extends ApiController
{
    /**
     * @var CommentImageManager
     */
    private $imageManager;

    /**
     * ReviewController constructor.
     *
     * @param CommentImageManager $imageManager
     */
    public function __construct(CommentImageManager $imageManager)
    {
        $this->imageManager = $imageManager;
    }

    /**
     * @Route(
     *      "/{id}",
     *      name="review_list",
     *      requirements={"id"="\d+"},
     *      methods={"POST"}
     *     )
     *
     * @Nelmio\Operation(
     *     summary="Getting review list",
     *     tags={"review"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Parameter(
     *          in="header",
     *          name="UM-Access-Token",
     *          description="API token for authorized user",
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          in="path",
     *          name="id",
     *          type="integer",
     *          required=true,
     *          description="Product ID"
     *     ),
     *     @SWG\Parameter(
     *          in="query",
     *          name="page",
     *          type="integer",
     *          description="Page number",
     *          default="1"
     *     ),
     *     @SWG\Parameter(
     *          in="query",
     *          name="count",
     *          type="integer",
     *          description="Elements count on one page",
     *          default="20"
     *     ),
     *     @SWG\Parameter(
     *          in="formData",
     *          name="rating",
     *          type="integer",
     *          description="Rating of reviews"
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="OK",
     *          headers={
     *                      @SWG\Header(header="X-Pagination-Page-Count", type="integer", description="Total page count")
     *                  },
     *          @SWG\Schema(
     *              type="array",
     *              @SWG\Items(type="object",
     *                  @SWG\Property(property="id", type="integer", description="Review ID", example=740),
     *                  @SWG\Property(property="product_id", type="integer", description="Product ID", example=10002),
     *                  @SWG\Property(property="user_name", type="string", description="User name", example="Борахат Сагадиев"),
     *                  @SWG\Property(property="text", type="string", description="Text of review (if available)", example="Товар хороший, быстрый. Нрайца, буду брать ещё."),
     *                  @SWG\Property(property="create_time", type="string", description="Create time", example="2018-05-01 12:24:12"),
     *                  @SWG\Property(property="rating", type="integer", minimum="0", maximum="5", description="Rating", example=5),
     *                  @SWG\Property(
     *                      property="images",
     *                      type="array",
     *                      @SWG\Items(type="object",
     *                          @SWG\Property(property="filename", type="string", example="acb94c286f07b93a8d701f7c0d9fe9067ed506af.jpg")
     *                      )
     *                  )
     *              )
     *          )
     *     ),
     * )
     *
     * @param Product|null $product
     * @param Request      $request
     *
     * @return JsonResponse
     *
     * @throws \LogicException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws NotFoundHttpException
     */
    public function getReviewListAction(?Product $product, Request $request): JsonResponse
    {
        if ($product === null) {
            throw new NotFoundHttpException('Product not found');
        }

        $page    = $request->query->getInt(ParametersNaming::PAGE, self::FIRST_PAGE_NUMBER);
        $perPage = $request->query->getInt(ParametersNaming::COUNT, self::PER_PAGE_COUNT);

        $filter = new ProductReviewFilter();
        $filter->fillFromRequest($request);

        /**
         * @var ProductReviewRepository $repository
         */
        $repository = $this->get('doctrine')->getRepository('AppBundle:ProductReview');

        /** @var SlidingPagination $pagination */
        $pagination = $this->get('knp_paginator')->paginate(
            $repository->findApprovedByProduct($product, $filter, $this->getUser()),
            $page,
            $perPage
        );

        return $this->renderJson(
            $pagination->getItems(),
            JsonResponse::HTTP_OK,
            null,
            [
                self::PAGINATION_COUNT_HEADER => $pagination->getPageCount()
            ]
        );
    }

    /**
     * @Route(
     *      "/{id}",
     *      name="review_create",
     *      requirements={"id"="\d+"},
     *      methods={"PUT"}
     *     )
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @Nelmio\Operation(
     *     summary="Creating of new review",
     *     tags={"review"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Parameter(
     *          in="header",
     *          name="UM-Access-Token",
     *          description="API token for authorized user",
     *          type="string",
     *     ),
     *     @SWG\Parameter(
     *          in="path",
     *          name="id",
     *          type="integer",
     *          required=true,
     *          description="Product ID"
     *     ),
     *     @SWG\Parameter(
     *          in="formData",
     *          name="rating",
     *          type="integer",
     *          minimum=0,
     *          maximum=5,
     *          description="Rating"
     *     ),
     *     @SWG\Parameter(
     *          in="formData",
     *          name="text",
     *          type="string",
     *          description="Text of review"
     *     ),
     *     @SWG\Parameter(
     *          in="formData",
     *          name="images",
     *          type="array",
     *          description="Review images",
     *          maxItems=8,
     *          @SWG\Items(type="string")
     *      ),
     *     @SWG\Response(
     *          response=200,
     *          description="OK",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="id", type="integer", description="Review ID", example=740),
     *              @SWG\Property(property="product_id", type="integer", description="Product ID", example=10002),
     *              @SWG\Property(property="user_name", type="string", description="User name", example="Иван"),
     *              @SWG\Property(property="text", type="string", description="Text of review (if available)", example=""),
     *              @SWG\Property(property="create_time", type="string", description="Create time", example="2018-05-01 12:24:12"),
     *              @SWG\Property(property="rating", type="integer", minimum="0", maximum="5", description="Rating", example=5),
     *              @SWG\Property(property="images", type="array", description="Image file name",
     *                  @SWG\Items(type="object",
     *                      @SWG\Property(property="filename", type="string", example="acb94c286f07b93a8d701f7c0d9fe9067ed506af.jpg")
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response=400,
     *          description="Incorrect params",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="status",
     *                  type="integer",
     *                  example=400
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  property="string",
     *                  example="Invalid parameters"
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response=404,
     *          description="Product not found",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="status",
     *                  type="integer",
     *                  example=404
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  property="string",
     *                  example="Invalid parameters"
     *              )
     *          )
     *     )
     * )
     *
     * @param Product|null  $product
     * @param Request       $request
     * @param ReviewManager $reviewManager
     *
     * @return JsonResponse
     *
     * @throws AlreadySubmittedException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \LogicException
     * @throws BadRequestHttpException
     * @throws NotFoundHttpException
     * @throws \Gumlet\ImageResizeException
     * @throws \InvalidArgumentException
     * @throws \MongoResultException
     */
    public function createReviewAction(?Product $product, Request $request, ReviewManager $reviewManager): JsonResponse
    {
        if ($product === null) {
            throw new NotFoundHttpException('Product not found');
        }

        $user   = $this->getUser();
        $rating = $request->request->get('rating');
        $text   = $request->request->get('text');

        if ($rating === null && $text === null) {
            throw new BadRequestHttpException('Rating or text required.');
        }

        $review = $reviewManager->createReview($product, $user);

        $form = $this->createForm(ReviewForm::class, $review);
        $form->submit($request->request->all(), false);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $commentImages = [];
            /** @var string[]|string $files */
            if ($files = $request->request->get('images')) {
                if (\is_string($files)) {
                    $files = explode(',', $files);
                }

                foreach ($files as $filename) {
                    if (!preg_match('#[a-z0-9]+(?:\.jpe?g|\.png)#', $filename)) {
                        continue;
                    }

                    if (!$this->imageManager->imageExists($filename)) {
                        continue;
                    }

                    $commentImages[] = (new CommentImage())->setFilename($filename)->setReview($review);
                }
            }

            $review->setImages($commentImages);
            $em->persist($review);
            $em->flush();

            return $this->renderJson($review);
        }

        return $this->errorJson(
            (string)$form->getErrors(true),
            JsonResponse::HTTP_BAD_REQUEST
        );
    }

    /**
     * @Route(
     *      "/{id}",
     *      name="review_delete",
     *      requirements={"id"="\d+"},
     *      methods={"DELETE"}
     *     )
     * @Nelmio\Operation(
     *     summary="Deleting of review",
     *     tags={"review"},
     *     @SWG\Parameter(
     *          in="header",
     *          name="UM-Access-Token",
     *          description="API token for authorized user",
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          in="path",
     *          name="id",
     *          required=true,
     *          type="integer",
     *          description="Review ID"
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="No Content"
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="status",
     *                  type="integer",
     *                  example=404
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  property="string",
     *                  example="Invalid parameters"
     *              )
     *          )
     *      )
     * )
     * @Security("has_role('ROLE_USER')")
     *
     * @param ProductReview|null $review
     * @param ReviewManager      $reviewManager
     *
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws AccessDeniedHttpException
     * @throws NotFoundHttpException
     * @throws \LogicException
     */
    public function deleteReviewAction(?ProductReview $review, ReviewManager $reviewManager): JsonResponse
    {
        if ($review === null) {
            throw new NotFoundHttpException('Review not found.');
        }

        $user = $this->getUser();

        if ($review->getUser() === null || $review->getUser()->getId() !== $user->getId()) {
            throw new AccessDeniedHttpException('You can\'t delete this review.');
        }

        try {
            $reviewManager->deleteReview($review);
        } catch (Exception $exception) {
            return $this->errorJson($exception);
        }

        return new JsonResponse();
    }

    /**
     * @Route(
     *      "/{id}",
     *      name="review_update",
     *      requirements={"id"="\d+"},
     *      methods={"PATCH"}
     *     )
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @Nelmio\Operation(
     *     summary="Updating of review",
     *     tags={"review"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Parameter(
     *          in="header",
     *          name="UM-Access-Token",
     *          description="API token for authorized user",
     *          type="string",
     *     ),
     *     @SWG\Parameter(
     *          in="path",
     *          name="id",
     *          type="integer",
     *          required=true,
     *          description="Review ID"
     *     ),
     *     @SWG\Parameter(
     *          in="formData",
     *          name="product_id",
     *          type="integer",
     *          description="Product ID"
     *     ),
     *     @SWG\Parameter(
     *          in="formData",
     *          name="rating",
     *          type="integer",
     *          minimum=0,
     *          maximum=5,
     *          description="Rating"
     *     ),
     *     @SWG\Parameter(
     *          in="formData",
     *          name="text",
     *          type="string",
     *          description="Text of review"
     *     ),
     *     @SWG\Parameter(
     *          in="formData",
     *          name="images",
     *          type="array",
     *          description="Image filename",
     *          @SWG\Items(type="string")
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="OK",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="id", type="integer", description="Review ID", example=740),
     *              @SWG\Property(property="product_id", type="integer", description="Product ID", example=10002),
     *              @SWG\Property(property="user_name", type="string", description="User name", example="Иван"),
     *              @SWG\Property(property="text", type="string", description="Text of review (if available)", example=""),
     *              @SWG\Property(property="create_time", type="string", description="Create time", example="2018-05-01 12:24:12"),
     *              @SWG\Property(property="rating", type="integer", minimum="0", maximum="5", description="Rating", example=5),
     *              @SWG\Property(property="images", type="array",
     *                  @SWG\Items(type="object",
     *                      @SWG\Property(property="filename", type="string", example="f10766c1e5594f90cb1538c8f977ede33810cad1.png")
     *                  )
     *              )
     *          )
     *     ),
     *     @SWG\Response(
     *          response="404",
     *          description="Not found",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="status",
     *                  type="integer",
     *                  example=404
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  property="string",
     *                  example="Invalid parameters"
     *              )
     *          )
     *      )
     * )
     *
     * @param ProductReview|null $review
     * @param Request            $request
     *
     * @return JsonResponse
     *
     * @throws AccessDeniedHttpException
     * @throws BadRequestHttpException
     * @throws \LogicException
     * @throws AlreadySubmittedException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws NotFoundHttpException
     */
    public function updateReviewAction(?ProductReview $review, Request $request): JsonResponse
    {
        if ($review === null) {
            throw new NotFoundHttpException('Review not found.');
        }

        if ($this->getUser()->getId() !== $review->getUser()->getId() || $review->getIsApproved()) {
            throw new AccessDeniedHttpException('You can\'t update this review.');
        }

        $productId = $request->get('product_id');
        if ($productId !== null) {
            $product = $this->getDoctrine()->getRepository('AppBundle:Product')->find($productId);

            if ($product === null) {
                throw new BadRequestHttpException('Product with this ID not found.');
            }

            $review->setProduct($product);
        }

        $form = $this->createForm(ReviewForm::class, $review);
        $form->submit($request->request->all(), false);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $existedImages = $review->getImages();
            $formImages = $request->request->get('images') ?? [];
            $imageNames = [];
            if (\is_string($formImages)) {
                $formImages = explode(',', $formImages);
            }

            foreach ($existedImages as $existedImage) {
                if (!\in_array($existedImage->getFilename(), $formImages, true)) {
                    $review->removeImage($existedImage);
                    $em->remove($existedImage);
                }

                $imageNames[] = $existedImage->getFilename();
            }

            $newImages = array_diff($formImages, $imageNames);
            foreach ($newImages as $newImage) {
                $commentImage = new CommentImage();
                $commentImage->setFilename($newImage)->setReview($review);
                $review->addImage($commentImage);
            }

            $em->persist($review);
            $em->flush();

            return $this->renderJson($review);
        }

        return $this->errorJson(
            (string)$form->getErrors(true),
            JsonResponse::HTTP_BAD_REQUEST
        );
    }

}
