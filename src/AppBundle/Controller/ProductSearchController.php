<?php

namespace AppBundle\Controller;

use AppBundle\Manager\ImageManager;
use AppBundle\Manager\ProductManager;
use AppBundle\Filter\ProductSearchFilter;
use AppBundle\Manager\ProductModelManager;
use AppBundle\Parameters\ParametersNaming;
use AppBundle\Serializer\SerializationGroups;
use AppBundle\Service\SolrManager;
use JMS\Serializer\SerializationContext;
use Knp\Component\Pager\PaginatorInterface;
use Psr\Log\LoggerInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Swagger\Annotations as SWG;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Bridge\Monolog\Logger;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

/**
 *
 * Class ProductSearchController
 *
 * @package AppBundle\Controller
 * @Route("/search")
 */
class ProductSearchController extends ApiController
{
    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * @var ProductModelManager
     */
    private $productModelManager;

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * ProductSearchController constructor
     *
     * @param ProductManager      $productManager
     * @param ProductModelManager $productModelManager
     * @param PaginatorInterface  $paginator
     * @param LoggerInterface     $logger
     */
    public function __construct(ProductManager $productManager,
                                ProductModelManager $productModelManager,
                                PaginatorInterface $paginator,
                                LoggerInterface $logger)
    {
        $this->productManager      = $productManager;
        $this->paginator           = $paginator;
        $this->productModelManager = $productModelManager;
        $this->logger              = $logger;
    }

    /**
     * @Nelmio\Operation(
     *     summary="Get products catalog by filters",
     *     tags={"new_catalog"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         type="integer",
     *         description="Page number"
     *     ),
     *     @SWG\Parameter(
     *         name="count",
     *         in="query",
     *         type="integer",
     *         description="Per page number"
     *     ),
     *     @SWG\Parameter(
     *         name="inStock",
     *         in="formData",
     *         type="boolean",
     *         description="Is product in stock"
     *     ),
     *     @SWG\Parameter(
     *         name="category",
     *         in="formData",
     *         type="integer",
     *         description="Category id"
     *     ),
     *     @SWG\Parameter(
     *         name="priceFrom",
     *         in="formData",
     *         type="number",
     *         description="Lowest price"
     *     ),
     *     @SWG\Parameter(
     *         name="priceTo",
     *         in="formData",
     *         type="number",
     *         description="Highest price"
     *     ),
     *     @SWG\Parameter(
     *         name="producers[]",
     *         in="formData",
     *         type="array",
     *         collectionFormat="multi",
     *         description="producers Id array",
     *         @SWG\Items(
     *             type="integer"
     *         )
     *     ),
     *     @SWG\Parameter(
     *         name="attributes",
     *         in="formData",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="query",
     *          in="formData",
     *          type="string",
     *          description="Search keywords"
     *     ),
     *     @SWG\Parameter(
     *         name="sorting[param]",
     *         in="formData",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sorting[direction]",
     *         in="formData",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     *
     * @Route(
     *      path="",
     *      name="get.search.list",
     *      methods={"POST"}
     * )
     *
     * @param Request      $request
     * @param ImageManager $imageManager
     * @return JsonResponse
     */
    public function getSearchListAction(Request $request, ImageManager $imageManager): JsonResponse
    {
        $solrClient  = $this->get('solarium.client');
        $solrManager = new SolrManager(
            $solrClient,
            $imageManager,
            $this->getParameter('solr.word_boost'),
            $this->getParameter('solr.fuzzy')
        );

        $offset = $request->query->getInt(ParametersNaming::PAGE, self::FIRST_PAGE_NUMBER);
        $limit  = $request->query->getInt(ParametersNaming::COUNT, self::PER_PAGE_COUNT);
        $params = $request->request->all();

        try {
            $result     = $solrManager->getCatalogData($params, $limit, ($offset - self::FIRST_PAGE_NUMBER) * $limit);
            $resultData = $solrManager->getFormattedProductData($result, $solrManager::PRODUCT_DATA);
            if (!empty($resultData)) {
                $response = [];

                foreach ($resultData as $item) {
                    $response[] = ['id' => (int)$item['id'], 'type' => 'product', 'product' => $item];
                }

                return $this->renderJson(
                    $response,
                    JsonResponse::HTTP_OK,
                    null,
                    [
                        self::PAGINATION_COUNT_HEADER => ceil($result->getNumFound() / $limit)
                    ]
                );
            }
        } catch (\Throwable $e) {
            $this->logger->error(sprintf('Solr search failed: %s', $e->getMessage()));
        }

        $filter = new ProductSearchFilter();
        $filter->fillFromRequest($request);

        $products = $this->productManager->queryApprovedByFilter($filter);

        $pagination = $this->paginator->paginate($products, $offset, $limit);

        $context = SerializationContext::create()->setGroups(SerializationGroups::PRODUCT_PREVIEW);

        $productItems = $pagination->getItems();
        $response     = [];

        foreach ($productItems as $item) {
            $response[] = ['id' => $item->getId(), 'type' => 'product', 'product' => $item];
        }

        return $this->renderJson(
            $response,
            JsonResponse::HTTP_OK,
            $context,
            [
                self::PAGINATION_COUNT_HEADER => $pagination->getPageCount()
            ]
        );
    }

    /**
     * @Nelmio\Operation(
     *     summary="Get models catalog by filters from Solr Server",
     *     tags={"new_catalog"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Parameter(
     *          name="name",
     *          in="formData",
     *          type="string",
     *          description="Search keywords"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="query",
     *         type="integer",
     *         description="Page number"
     *     ),
     *     @SWG\Parameter(
     *         name="count",
     *         in="query",
     *         type="integer",
     *         description="Per page number"
     *     ),
     *     @SWG\Parameter(
     *         name="isChinaProduct",
     *         in="formData",
     *         type="boolean",
     *         description="Is china product"
     *     ),
     *     @SWG\Parameter(
     *         name="category",
     *         in="formData",
     *         type="integer",
     *         description="Category id"
     *     ),
     *     @SWG\Parameter(
     *         name="priceFrom",
     *         in="formData",
     *         type="number",
     *         description="Lowest price"
     *     ),
     *     @SWG\Parameter(
     *         name="priceTo",
     *         in="formData",
     *         type="number",
     *         description="Highest price"
     *     ),
     *     @SWG\Parameter(
     *         name="producers[]",
     *         in="formData",
     *         type="array",
     *         collectionFormat="multi",
     *         description="producers Id array",
     *         @SWG\Items(
     *             type="integer"
     *         )
     *     ),
     *     @SWG\Parameter(
     *         name="attributes",
     *         in="formData",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sorting[param]",
     *         in="formData",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sorting[direction]",
     *         in="formData",
     *         type="string"
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK"
     *     )
     * )
     *
     * @Route(
     *      path="/model",
     *      name="get.search.model.list",
     *      methods={"POST"}
     * )
     *
     * @param Request      $request
     * @param ImageManager $imageManager
     * @return JsonResponse
     */
    public function getSearchModelSolrAction(Request $request, ImageManager $imageManager): JsonResponse
    {
        $solrClient = $this->get('solarium.client.model');
        $wordBoost  = $this->getParameter('solr.word_boost');
        $fuzzy      = $this->getParameter('solr.fuzzy');

        $solrManager = new SolrManager($solrClient, $imageManager, $wordBoost, $fuzzy);

        $offset = $request->query->getInt(ParametersNaming::PAGE, self::FIRST_PAGE_NUMBER);
        $limit  = $request->query->getInt(ParametersNaming::COUNT, self::PER_PAGE_COUNT);
        $params = $request->request->all();

        try {
            $result = $solrManager->getModelData($params, $limit, ($offset - self::FIRST_PAGE_NUMBER) * $limit);
        } catch (\Exception $e) {
            $this->logger->error("Solr search failed:\n{$e->getMessage()}");
            $result = null;
        }

        if ($result !== null && $docs = $result->getData()['response']['docs']) {
            $resultData = $solrManager->getFormattedProductData($result, $solrManager::MODEL_DATA);

            $response = [];
            foreach ($resultData as $item) {
                $response[] = ['id' => (int)$item['id'], 'type' => 'model', 'model' => $item];
            }

            return $this->renderJson(
                $response,
                JsonResponse::HTTP_OK,
                null,
                [
                    self::PAGINATION_COUNT_HEADER => ceil($result->getNumFound() / $limit)
                ]
            );
        }

        $filter = new ProductSearchFilter();
        $filter->fillFromRequest($request);
        $products = $this->productModelManager->queryApprovedByFilter($filter);

        $pagination = $this->paginator->paginate($products, $offset, $limit);

        $context = SerializationContext::create()->setGroups(SerializationGroups::MODEL_CATALOG);

        $modelItems = $pagination->getItems();
        $response   = [];

        foreach ($modelItems as $item) {
            $response[] = ['id' => $item->getId(), 'type' => 'model', 'model' => $item];
        }

        return $this->renderJson(
            $response,
            JsonResponse::HTTP_OK,
            $context,
            [
                self::PAGINATION_COUNT_HEADER => $pagination->getPageCount()
            ]
        );
    }
}
