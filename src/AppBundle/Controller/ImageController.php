<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Swagger\Annotations as SWG;
use AppBundle\Manager\CommentImageManager;
use Symfony\Component\HttpFoundation\Request;
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

/**
 * Class ImageController
 *
 * @package AppBundle\Controller
 * @Route("/image")
 */
class ImageController extends ApiController
{
    private $manager;

    public function __construct(CommentImageManager $imageManager)
    {
        $this->manager = $imageManager;
    }

    /**
     * @Nelmio\Operation(
     *     summary="test",
     *     tags={"image_upload"},
     *     consumes={"multipart/form-data"},
     *     @SWG\Parameter(
     *          in="header",
     *          name="UM-Access-Token",
     *          description="API token for authorized user",
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="image",
     *         in="formData",
     *         type="file"
     *     ),
     *     @SWG\Response(
     *          response=403,
     *          description="Full authentication is required to access this resource.",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(
     *                 property="filename", type="string"
     *             )
     *         )
     *     )
     * )
     *
     * @Route(
     *      path="",
     *      methods={"POST"}
     * )
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     *
     * @return Response
     * @throws \Gumlet\ImageResizeException
     * @throws \MongoResultException
     * @throws \InvalidArgumentException
     */
    public function actionIndex(Request $request): Response
    {
        /**
         * @var UploadedFile $file
         */
        $file = $request->files->get('image');

        if ($file === null) {
            return $this->errorJson('Image file is not uploaded', Response::HTTP_BAD_REQUEST);
        }

        $filename = $this->manager->uploadImage($file, true);

        return new JsonResponse([
                                    'filename' => $filename . '.' . strtolower($file->getClientOriginalExtension())
                                ]);
    }
}
