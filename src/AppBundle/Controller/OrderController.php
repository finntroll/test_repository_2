<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Product;
use AppBundle\Entity\UserOrder;
use AppBundle\Manager\OrderManager;
use AppBundle\Parameters\ParametersNaming;
use AppBundle\Service\CartService;
use Knp\Component\Pager\PaginatorInterface;
use Exception;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\{
    Route, Security
};
use Swagger\Annotations as SWG;
use Symfony\Component\DependencyInjection\Exception\{
    ServiceCircularReferenceException, ServiceNotFoundException
};
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\HttpFoundation\{
    JsonResponse, Request, Response
};
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * @Route("/orders")
 *
 * Class OrderController
 *
 * @package AppBundle\Controller
 */
class OrderController extends ApiController
{

    /**
     * @var CartService $cartService
     */
    private $cartService;

    /**
     * @var PaginatorInterface
     */
    private $paginator;

    /**
     * OrderController constructor.
     *
     * @param CartService        $cartService
     * @param PaginatorInterface $paginator
     */
    public function __construct(CartService $cartService, PaginatorInterface $paginator)
    {
        $this->cartService = $cartService;
        $this->paginator   = $paginator;
    }

    /**
     * @Route("", name="order_list", methods={"GET"})
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request      $request
     * @param OrderManager $orderManager
     *
     * @return JsonResponse
     *
     * @throws LogicException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     *
     * @SWG\Parameter(
     *     in="header",
     *     name="Um-Access-Token",
     *     description="API token for authorized user",
     *     type="string"
     * )
     * @SWG\Parameter(
     *      name="page",
     *      in="query",
     *      type="integer",
     *      description="Номер страницы",
     *      default="1"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="OK
     *                  Headers: X-Pagination-Page-Count (integer): Общее количество страниц.",
     * @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(
     *                              property="id",
     *                              type="integer",
     *                              example=14212,
     *                              description="Идентификатор заказа"
     *                          ),
     *              @SWG\Property(
     *                              property="status",
     *                              type="string",
     *                              enum={"new", "yandex_accept", "in_work", "in_courier_service", "finished", "deleted"},
     *                              description="Статус заказа"
     *                          ),
     *              @SWG\Property(
     *                              property="date_time",
     *                              type="integer",
     *                              example=1513014209,
     *                              description="Unix-time оформления заказа (UTC)"
     *                          ),
     *              @SWG\Property(
     *                              property="cost",
     *                              type="float",
     *                              example=2521.2,
     *                              description="Конечная стоимость заказа"
     *                          )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="orders")
     */
    public function getListAction(Request $request, OrderManager $orderManager): JsonResponse
    {
        $userId = $this->getUser()->getId();

        $page = $request->query->getInt(ParametersNaming::PAGE, self::FIRST_PAGE_NUMBER);

        $pagination = $this->paginator->paginate(
            $orderManager->findByUserId($userId),
            $page,
            self::PER_PAGE_COUNT
        );

        return $this->renderJson(
            $pagination->getItems(),
            JsonResponse::HTTP_OK,
            null,
            [
                self::PAGINATION_COUNT_HEADER => $pagination->getPageCount()
            ]
        );
    }

    /**
     * @Nelmio\Operation(
     *     summary="Submit order",
     *     tags={"orders"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Parameter(
     *         in="header",
     *         name="Um-Access-Token",
     *         description="API token for authorized user",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="address.city",
     *         in="formData",
     *         type="string",
     *         required=true,
     *         description="Город"
     *     ),
     *     @SWG\Parameter(
     *         name="address.country",
     *         in="formData",
     *         required=true,
     *         type="string",
     *         description="Страна"
     *     ),
     *     @SWG\Parameter(
     *         name="address.full_address",
     *         in="formData",
     *         required=true,
     *         type="string",
     *         description="Адрес в рамках города"
     *     ),
     *     @SWG\Parameter(
     *         name="address.region",
     *         in="formData",
     *         required=true,
     *         type="string",
     *         description="Регион"
     *     ),
     *     @SWG\Parameter(
     *         name="address.zip_code",
     *         in="formData",
     *         required=true,
     *         type="integer",
     *         maxLength=6,
     *         description="Почтовый индекс"
     *     ),
     *     @SWG\Parameter(
     *         name="contact.email",
     *         in="formData",
     *         required=true,
     *         type="string",
     *         description="Email"
     *     ),
     *     @SWG\Parameter(
     *         name="contact.full_name",
     *         in="formData",
     *         required=true,
     *         type="string",
     *         description="Полное имя"
     *     ),
     *     @SWG\Parameter(
     *         name="contact.phone",
     *         in="formData",
     *         required=true,
     *         type="string",
     *         description="Телефон",
     *         minLength=11,
     *     ),
     *     @SWG\Parameter(
     *         name="contact.tin",
     *         in="formData",
     *         type="string",
     *         description="ИНН",
     *         minLength=10,
     *         maxLength=12,
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *         @SWG\Schema(@Model(type=UserOrder::class))
     *     ),
     *     @SWG\Response(
     *          response=400,
     *          description="Various errors",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="status", type="integer", example=400),
     *              @SWG\Property(property="message", type="string")
     *          )
     *     ),
     *     @SWG\Response(
     *          response=404,
     *          description="Product not found",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(
     *                  property="status",
     *                  type="integer",
     *                  example=404
     *              ),
     *              @SWG\Property(
     *                  property="message",
     *                  type="string",
     *                  example="Not found"
     *              )
     *          )
     *     )
     * )
     *
     * @Route(
     *     "",
     *     name="order_submit",
     *     methods={"PUT"}
     * )
     *
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request      $request
     * @param OrderManager $orderManager
     *
     * @return JsonResponse
     *
     * @throws LogicException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    public function submitOrder(Request $request, OrderManager $orderManager): JsonResponse
    {
        $user       = $this->getUser();
        $parameters = $request->request->all();

        $formattedParams = [];
        foreach ($parameters as $key => $value) {
            $normalisedKeys                                          = explode('_', $key, 2);
            $formattedParams[$normalisedKeys[0]][$normalisedKeys[1]] = $value;
        }

        if (!isset($formattedParams['address']) || !isset($formattedParams['contact']) || empty($formattedParams['contact'])
            || empty($formattedParams['address'])) {
            return $this->errorJson('Wrong parameters.', Response::HTTP_BAD_REQUEST);
        }

        $cartProducts = $this->cartService->getProductList($user);

        if (empty($cartProducts)) {
            return $this->errorJson('Cart is empty.', Response::HTTP_BAD_REQUEST);
        }

        $productRepository = $this->getDoctrine()->getRepository('AppBundle:Product');
        $orderProducts     = [];

        foreach ($cartProducts as $cartProduct) {
            if (null === $cartProduct->getCount() || null === $cartProduct->getProductId()) {
                return $this->errorJson('Wrong data.', Response::HTTP_BAD_REQUEST);
            }

            if ((int)$cartProduct->getCount() < 1) {
                return $this->errorJson('Wrong count.', Response::HTTP_BAD_REQUEST);
            }

            /**
             * @var Product $product
             */
            $product = $productRepository->find($cartProduct->getProductId());

            if ((int)$product->getQuantity() < (int)$cartProduct->getCount()) {
                return $this->errorJson('Not enough products.', Response::HTTP_BAD_REQUEST);
            }

            if (null === $product) {
                return $this->errorJson('Product not found.', Response::HTTP_NOT_FOUND);
            }

            $orderProducts[] = ['product' => $product, 'count' => $cartProduct->getCount()];
        }

        try {
            $order = $orderManager->createNewOrder($user, $formattedParams, $orderProducts);
        } catch (Exception $exception) {
            if ($exception instanceof HttpException) {
                throw $exception;
            }

            return $this->errorJson('Internal error.');
        }

        $message = \Swift_Message::newInstance()
            ->setSubject('Новый заказ №' . $order->getId() . ' в магазине ' . $this->container->getParameter('host_name'))
            ->setFrom($this->container->getParameter('mailer_user'))
            ->setTo($order->getEmail())
            ->setBody(
                $this->renderView(
                    '@AppBundle/Resources/views/user_email.html.twig',
                    [
                        'order'     => $order,
                        'host_name' => $this->container->getParameter('host_name')
                    ]),
                'text/html'
            );

        $this->get('mailer')->send($message);

        $this->cartService->clearUserCart($user);

        return $this->renderJson(
            $order,
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("/{orderId}", name="order",  requirements={"orderId"="\d+"}, methods={"GET"})
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request      $request
     * @param OrderManager $orderManager
     *
     * @return JsonResponse
     *
     * @throws LogicException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     *
     * @SWG\Parameter(
     *     in="header",
     *     name="Um-Access-Token",
     *     description="API token for authorized user",
     *     type="string"
     * )
     * @SWG\Parameter(
     *      name="orderId",
     *      in="path",
     *      type="integer",
     *      description="Номер заказа"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="OK"
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Order not found",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="status", type="integer", example=404),
     *          @SWG\Property(property="message", type="string", example="Not found")
     *     )
     * )
     *
     * @SWG\Tag(name="orders")
     */
    public function getOrder(Request $request, OrderManager $orderManager): JsonResponse
    {
        $userId = $this->getUser()->getId();

        $order = $orderManager->findByOrderId($userId, $request->get('orderId'));

        if ($order === null) {
            throw new HttpException(404, 'Order not found');
        }

        return $this->renderJson(
            $order,
            JsonResponse::HTTP_OK
        );
    }

    /**
     * @Route("/{orderId}", name="orderDelete",  requirements={"orderId"="\d+"}, methods={"DELETE"})
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request      $request
     * @param OrderManager $orderManager
     *
     * @return JsonResponse
     *
     * @throws LogicException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     *
     * @SWG\Parameter(
     *     in="header",
     *     name="Um-Access-Token",
     *     description="API token for authorized user",
     *     type="string"
     * )
     * @SWG\Parameter(
     *      name="orderId",
     *      in="path",
     *      type="integer",
     *      description="Номер заказа"
     * )
     *
     * @SWG\Response(
     *     response="200",
     *     description="OK"
     * )
     *
     * @SWG\Response(
     *     response=404,
     *     description="Order not found",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="status",
     *              type="integer",
     *              example=404
     *          ),
     *          @SWG\Property(
     *              property="message",
     *              type="string",
     *              example="Order not found"
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="orders")
     */
    public function deleteOrder(Request $request, OrderManager $orderManager): JsonResponse
    {
        $userId = $this->getUser()->getId();

        $order = $orderManager->changeOrderStatus($userId, $request->get('orderId'), UserOrder::STATUS_DELETED);

        if ($order === null) {
            throw new HttpException(404, 'Order not found');
        }

        return $this->renderJson(
            $order,
            JsonResponse::HTTP_OK
        );
    }
}
