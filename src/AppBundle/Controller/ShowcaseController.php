<?php

namespace AppBundle\Controller;

use AppBundle\Manager\ShowcaseManager;
use AppBundle\Parameters\ParametersNaming;
use AppBundle\Serializer\SerializationGroups;
use JMS\Serializer\SerializationContext;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\Exception\{
    ServiceCircularReferenceException, ServiceNotFoundException
};
use Symfony\Component\HttpFoundation\{
    JsonResponse, Request
};
use AppBundle\Entity\Showcase;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * Class ShowcaseController
 * @package AppBundle\Controller
 * @Route("/showcase")
 */
class ShowcaseController extends ApiController
{

    /**
     * @var ShowcaseManager
     */
    private $manager;

    /**
     * ShowcaseController constructor.
     * @param ShowcaseManager $showcaseManager
     */
    public function __construct(ShowcaseManager $showcaseManager)
    {
        $this->manager = $showcaseManager;
    }

    /**
     * @param int $id
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @Route("/{id}", name="showcase_view", requirements={"id"="\d+"}, methods={"GET"})
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \LogicException
     *
     * @SWG\Parameter(
     *      name="page",
     *      in="query",
     *      type="integer",
     *      description="Page number"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="OK
                        Headers : X-Pagination-Page-Count (integer) : Общее количество страниц.",
     *      @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="id", type="integer"),
     *              @SWG\Property(property="name", type="string"),
     *              @SWG\Property(property="is_china_product", type="boolean"),
     *              @SWG\Property(property="in_stock", type="boolean"),
     *              @SWG\Property(property="rating", type="integer"),
     *              @SWG\Property(property="in_favorites", type="boolean"),
     *              @SWG\Property(property="is_out_of_stock", type="boolean"),
     *              @SWG\Property(property="price", type="float"),
     *              @SWG\Property(property="discount_price", type="float"),
     *              @SWG\Property(property="images", type="array",
     *                  @SWG\Items(type="object",
     *                      @SWG\Property(property="preview", type="string"),
     *                      @SWG\Property(property="large", type="string"),
     *                      @SWG\Property(property="full", type="string"),
     *                      @SWG\Property(property="sort", type="integer")
     *                  )
     *              )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="showcase")
     */
    public function viewAction(int $id, Request $request): JsonResponse
    {
        $page = $request->query->getInt(ParametersNaming::PAGE, self::FIRST_PAGE_NUMBER);

        /** @var SlidingPagination $pagination */
        $pagination = $this->get('knp_paginator')->paginate(
            $this->manager->getProductsQueryById($id),
            $page,
            self::PER_PAGE_COUNT
        );

        $context = SerializationContext::create()->setGroups([SerializationGroups::PRODUCT_PREVIEW]);

        return $this->renderJson(
            $pagination->getItems(),
            JsonResponse::HTTP_OK,
            $context,
            [
                'X-Pagination-Page-Count' => $pagination->getPageCount()
            ]
        );
    }

}
