<?php

namespace AppBundle\Controller;

use AppBundle\Exception\{
    NullHashException, WrongExpressionException
};
use AppBundle\Parameters\ParametersNaming;
use AppBundle\Repository\AbstractStatefulRepository;
use AppBundle\ValidationModel\CitySearchValidationModel;
use Exception;
use JMS\Serializer\Context;
use JMS\Serializer\SerializationContext;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\Exception\{
    ServiceCircularReferenceException, ServiceNotFoundException
};
use Symfony\Component\HttpFoundation\{
    JsonResponse, Request, Response
};
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Exception\ValidatorException;

/**
 * Class DictController
 *
 * @package AppBundle\Controller
 * @Route("/dict")
 */
class DictController extends ApiController
{

    /**
     *
     */
    private const GROUPS_PER_PAGE_COUNT = 1000;

    /**
     *
     */
    private const ATTRIBUTES_PER_PAGE_COUNT = 1500;

    /**
     *
     */
    private const OPTIONS_PER_PAGE_COUNT = 5000;

    /**
     *
     */
    private const CATEGORIES_PER_PAGE_COUNT = 250;

    /**
     * @Route("/attr/groups", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \LogicException
     *
     * @SWG\Parameter(
     *      name="page",
     *      in="query",
     *      type="integer",
     *      description="Page number"
     * )
     *
     * @SWG\Response(
     *      response=200,
     *      description="OK
     *               Headers : UM-Object-Hash (string) : Хэш коллекции категорий.
     *                         X-Pagination-Page-Count (int) : Количество страниц ",
     *      @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="id", type="integer"),
     *              @SWG\Property(property="title", type="string"),
     *              @SWG\Property(property="sort", type="integer")
     *          )
     *      )
     *  )
     *
     * @SWG\Tag(name="dictionary")
     */
    public function getAttributeGroupsAction(Request $request): JsonResponse
    {
        $page = $request->query->getInt('page', self::FIRST_PAGE_NUMBER);

        $repository = $this->getDoctrine()->getRepository('AppBundle:AttributeGroup');

        return $this->paginatedJsonResponse($repository, $page, self::GROUPS_PER_PAGE_COUNT);
    }

    /**
     * @Route("/attr/attributes", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \LogicException
     *
     * @SWG\Parameter(
     *      name="page",
     *      in="query",
     *      type="integer",
     *      description="Page number"
     * )
     *
     * @SWG\Response(
     *      response=200,
     *      description="OK
     *               Headers : UM-Object-Hash (string) : Хэш коллекции категорий.
     *                         X-Pagination-Page-Count (int) : Количество страниц ",
     *      @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="id", type="integer"),
     *              @SWG\Property(property="group_id", type="integer"),
     *              @SWG\Property(property="title", type="string"),
     *              @SWG\Property(property="type", type="integer"),
     *              @SWG\Property(property="unit", type="string"),
     *              @SWG\Property(property="sort", type="integer"),
     *              @SWG\Property(property="is_filter", type="boolean")
     *          )
     *      )
     *  )
     *
     * @SWG\Tag(name="dictionary")
     */
    public function getAttributesAction(Request $request): JsonResponse
    {
        $page = $request->query->getInt('page', self::FIRST_PAGE_NUMBER);

        $repository = $this->getDoctrine()->getRepository('AppBundle:Attribute');

        return $this->paginatedJsonResponse($repository, $page, self::ATTRIBUTES_PER_PAGE_COUNT);
    }

    /**
     * @Route("/attr/options", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \LogicException
     *
     * @SWG\Parameter(
     *      name="page",
     *      in="query",
     *      type="integer",
     *      description="Page number"
     * )
     *
     * @SWG\Response(
     *      response=200,
     *      description="OK
     *               Headers : UM-Object-Hash (string) : Хэш коллекции категорий.
     *                         X-Pagination-Page-Count (int) : Количество страниц ",
     *      @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="id", type="integer"),
     *              @SWG\Property(property="attribute_id", type="integer"),
     *              @SWG\Property(property="value", type="string")
     *          )
     *      )
     *  )
     *
     * @SWG\Tag(name="dictionary")
     */
    public function getAttributeOptionsAction(Request $request): JsonResponse
    {
        $page = $request->query->getInt('page', self::FIRST_PAGE_NUMBER);

        $repository = $this->getDoctrine()->getRepository('AppBundle:AttributeOption');

        return $this->paginatedJsonResponse($repository, $page, self::OPTIONS_PER_PAGE_COUNT);
    }

    /**
     * @Route("/categories", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \LogicException
     *
     * @SWG\Response(
     *      response=200,
     *      description="OK
                         Headers : UM-Object-Hash (string) : Хэш коллекции категорий.",
     *      @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="id", type="integer"),
     *              @SWG\Property(property="title", type="string"),
     *              @SWG\Property(property="image", type="string"),
     *              @SWG\Property(property="sort", type="integer"),
     *              @SWG\Property(property="parent_id", type="integer")
     *          )
     *      )
     *  )
     *
     * @SWG\Tag(name="dictionary")
     */
    public function getCategoriesAction(Request $request): JsonResponse
    {
        $page = $request->query->getInt(ParametersNaming::PAGE, self::FIRST_PAGE_NUMBER);

        $repository = $this->getDoctrine()->getRepository('AppBundle:Category');

        return $this->paginatedJsonResponse($repository, $page, self::CATEGORIES_PER_PAGE_COUNT);
    }

    /**
     * @Route("/producers", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \LogicException
     *
     * @SWG\Parameter(
     *      name="page",
     *      in="query",
     *      type="integer",
     *      description="Page number"
     * )
     *
     * @SWG\Response(
     *      response=200,
     *      description="OK
                         Headers : X-Pagination-Page-Count (integer) : Общее количество страниц.
                         UM-Object-Hash (string) : Хэш списка производителей.",
     *      @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="id", type="integer"),
     *              @SWG\Property(property="name", type="string"),
     *              @SWG\Property(property="is_popular", type="boolean")
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="dictionary")
     */
    public function getProducersAction(Request $request): JsonResponse
    {
        $page = $request->query->getInt(ParametersNaming::PAGE, self::FIRST_PAGE_NUMBER);

        $repository = $this->getDoctrine()->getRepository('AppBundle:Producer');

        return $this->paginatedJsonResponse($repository, $page, self::ATTRIBUTES_PER_PAGE_COUNT);
    }

    /**
     * @Route("/countries", name="countries_list", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \LogicException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     *
     * @SWG\Parameter(
     *      name="page",
     *      in="query",
     *      type="integer",
     *      description="Page number"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="OK
                        Headers : X-Pagination-Page-Count (integer) : Общее количество страниц.
                        UM-Object-Hash (string) : Хэш списка стран.",
     *      @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="id", type="integer"),
     *              @SWG\Property(property="title", type="string")
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="dictionary")
     */
    public function getCountriesAction(Request $request): JsonResponse
    {
        $page = $request->query->getInt(ParametersNaming::PAGE, self::FIRST_PAGE_NUMBER);

        $repository = $this->getDoctrine()->getRepository('AppBundle:Country');

        return $this->paginatedJsonResponse($repository, $page);
    }

    /**
     * @Route("/regions", name="regions_list", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \LogicException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     *
     * @SWG\Parameter(
     *      name="page",
     *      in="query",
     *      type="integer",
     *      description="Page number"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="OK
                        Headers : X-Pagination-Page-Count (integer) : Общее количество страниц.
                        UM-Object-Hash (string) : Хэш списка регионов.",
     *      @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="id", type="integer"),
     *              @SWG\Property(property="country_id", type="integer"),
     *              @SWG\Property(property="title", type="string")
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="dictionary")
     */
    public function getRegionsAction(Request $request): JsonResponse
    {
        $page = $request->query->getInt(ParametersNaming::PAGE, self::FIRST_PAGE_NUMBER);

        $repository = $this->getDoctrine()->getRepository('AppBundle:Region');

        return $this->paginatedJsonResponse($repository, $page, self::CATEGORIES_PER_PAGE_COUNT);
    }

    /**
     * @Route("/cities/important", name="cities_important_list", methods={"GET"})
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \LogicException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     *
     * @SWG\Parameter(
     *      name="page",
     *      in="query",
     *      type="integer",
     *      description="Page number"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="OK
                        Headers : X-Pagination-Page-Count (integer) : Общее количество страниц.
                        UM-Object-Hash (string) : Хэш списка городов.",
     *      @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="id", type="integer"),
     *              @SWG\Property(property="area", type="string || null"),
     *              @SWG\Property(property="country_id", type="integer"),
     *              @SWG\Property(property="region_id", type="integer"),
     *              @SWG\Property(property="title", type="string")
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="dictionary")
     */
    public function getImportantCitiesAction(Request $request): JsonResponse
    {
        $page = $request->query->getInt(ParametersNaming::PAGE, self::FIRST_PAGE_NUMBER);

        $repository = $this->getDoctrine()->getRepository('AppBundle:City');

        return $this->paginatedJsonResponse($repository, $page, self::CATEGORIES_PER_PAGE_COUNT);
    }

    /**
     * @Route("/cities/search", name="cities_search", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws \LogicException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     *
     * @SWG\Parameter(
     *         name="filter",
     *         in="body",
     *         description="json request params",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="filter_name", type="string"),
     *             @SWG\Property(property="region_id", type="integer"),
     *             @SWG\Property(property="limit", type="integer")
     *         )
     * )
     *
     * @SWG\Response(
     *      response=200,
     *      description="OK
                        Headers : X-Pagination-Page-Count (integer) : Общее количество страниц.",
     *      @SWG\Schema(
     *          type="array",
     *          @SWG\Items(
     *              type="object",
     *              @SWG\Property(property="id", type="integer"),
     *              @SWG\Property(property="country_id", type="integer"),
     *              @SWG\Property(property="region_id", type="integer"),
     *              @SWG\Property(property="title", type="string")
     *          )
     *      )
     * )
     *
     * @SWG\Response(
     *     response=400,
     *     description="Missing parameters",
     *     @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="status",
     *              type="integer",
     *              example=400
     *          ),
     *          @SWG\Property(
     *              property="message",
     *              type="string"
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="dictionary")
     */
    public function getCitiesBySearchAction(Request $request): JsonResponse
    {
        $model = CitySearchValidationModel::makeInstance($request);
        $validator = $this->get('validator');
        /** @var ConstraintViolationList $violations */
        $violations = $validator->validate($model);
        if (\count($violations) !== 0) {
            $message = $model->makeErrorString($violations);
            throw new ValidatorException($message);
        }

        $params = $this->getJsonBody();
        if (!$params->containsKey('filter_name')
            || !$params->containsKey('region_id')
            || !$params->containsKey('limit'))
        {
            return $this->errorJson('Required parameters missed', Response::HTTP_BAD_REQUEST);
        }

        $repository = $this->getDoctrine()->getRepository('AppBundle:City');

        $result = $repository->getByParams($params);

        return $this->renderJson($result);
    }

    /**
     * @param AbstractStatefulRepository $repository
     * @param int                        $page
     * @param int                        $count
     *
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \LogicException
     */
    protected function paginatedJsonResponse(
        AbstractStatefulRepository $repository,
        int $page,
        int $count = self::PER_PAGE_COUNT
    ): JsonResponse
    {
        /** @var SlidingPagination $pagination */
        $pagination = $this->get('knp_paginator')->paginate(
            $repository->getListQuery(),
            $page,
            $count
        );

        try {
            $hash = $repository->getHash();
        } catch (WrongExpressionException $exception) {
            return $this->errorJson($exception);
        } catch (NullHashException $exception) {
            return $this->errorJson($exception);
        } catch (Exception $exception) {
            $this->get('logger')->error($exception->getTraceAsString());

            return $this->errorJson('Error in receiving data.');
        }

        return $this->renderJson(
            $pagination->getItems(),
            JsonResponse::HTTP_OK,
            null,
            [
                self::PAGINATION_COUNT_HEADER => $pagination->getPageCount(),
                self::HASH_HEADER             => $hash
            ]
        );
    }

}
