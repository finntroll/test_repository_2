<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Entity\User;
use AppBundle\Manager\FavoriteManager;
use AppBundle\Parameters\ParametersNaming;
use AppBundle\Serializer\SerializationGroups;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Swagger\Annotations as SWG;
use Exception;
use JMS\Serializer\SerializationContext;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\{
    Route, Security
};
use AppBundle\Entity\Product;
use Symfony\Component\DependencyInjection\Exception\{
    ServiceCircularReferenceException, ServiceNotFoundException
};
use Symfony\Component\HttpFoundation\{
    JsonResponse, Request, Response
};

class FavoriteController extends ApiController
{

    /**
     * @Route("/favorites", name="favorite_list", methods={"GET"})
     * @Security("has_role('ROLE_USER')")
     *
     * @SWG\Get(
     *     tags={"favorites"},
     *     description="Get favorites list ",
     *     produces={"application/json"},
     *     @SWG\Parameter(in="header", name="Um-Access-Token", description="API token for authorized user", type="string"),
     *     @SWG\Parameter(in="query", name="count", type="integer"),
     *     @SWG\Parameter(in="query", name="page", type="integer"),
     *     @SWG\Response(
     *          response=403,
     *          description="Full authentication is required to access this resource.",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Array of favored products by current user
     *                  Headers: X-Pagination-Page-Count: Page count",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(type="object",
     *                 @SWG\Property(property="items", type="array",
     *                     @SWG\Items(type="object",
     *                         @SWG\Property(property="count", type="integer"),
     *                         @SWG\Property(
     *                             property="product",
     *                             type="object",
     *                             @SWG\Property(property="id", type="integer"),
     *                             @SWG\Property(property="name", type="string"),
     *                             @SWG\Property(property="is_china_product", type="boolean"),
     *                             @SWG\Property(property="rating", type="integer"),
     *                             @SWG\Property(property="in_favorite", type="boolean"),
     *                             @SWG\Property(property="is_out_of_stock", type="boolean"),
     *                             @SWG\Property(property="price", type="float"),
     *                             @SWG\Property(property="discount_price", type="float"),
     *                             @SWG\Property(property="images", type="array",
     *                                 @SWG\Items(type="object",
     *                                     @SWG\Property(property="preview", type="string"),
     *                                     @SWG\Property(property="large", type="string"),
     *                                     @SWG\Property(property="full", type="string"),
     *                                     @SWG\Property(property="sort", type="integer")
     *                                 )
     *                             )
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     )
     * )
     *
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws LogicException
     */
    public function getFavorites(Request $request): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $page  = $request->query->getInt(ParametersNaming::PAGE, self::FIRST_PAGE_NUMBER);
        $count = $request->query->getInt(ParametersNaming::COUNT, self::PER_PAGE_COUNT);

        $favorites = $user->getFavorites();
        /** @var Product $favorite */
        foreach ($favorites as $favorite) {
            if (($category = $favorite->getCategory()) && $category->getStatus() !== Category::STATUS_PUBLISHED) {
                $favorites->removeElement($favorite);
            }
        }

        /** @var SlidingPagination $pagination */
        $pagination = $this->get('knp_paginator')->paginate(
            $user->getFavorites(),
            $page,
            $count
        );

        $context = SerializationContext::create()->setGroups([SerializationGroups::PRODUCT_PREVIEW]);

        return $this->renderJson(
            (array)$pagination->getItems(),
            JsonResponse::HTTP_OK,
            $context,
            [
                self::PAGINATION_COUNT_HEADER => $pagination->getPageCount()
            ]
        );
    }

    /**
     *
     * @Route("/favorites", name="add_favorite", methods={"PUT"})
     * @Security("has_role('ROLE_USER')")
     * @SWG\Put(
     *     tags={"favorites"},
     *     description="Add product to favorites",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(in="header", name="Um-Access-Token", description="API token for authorized user", type="string"),
     *     @SWG\Parameter(in="formData", name="product_id", description="Product ID for add to user's favorites", type="integer"),
     *     @SWG\Response(
     *          response=404,
     *          description="Product not found.",
     *     ),
     *     @SWG\Response(
     *          response=403,
     *          description="Full authentication is required to access this resource.",
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="OK"
     *     )
     * )
     *
     * @param Request         $request
     * @param FavoriteManager $favoriteManager
     *
     * @return Response
     *
     * @throws LogicException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    public function addFavorite(Request $request, FavoriteManager $favoriteManager): Response
    {
        $productId = $request->get('product_id');

        if ($productId === null) {
            return $this->errorJson('Product ID missed', Response::HTTP_BAD_REQUEST);
        }

        $repository = $this->getDoctrine()->getRepository('AppBundle:Product');
        $product    = $repository->find($productId);

        if (!$product) {
            return $this->errorJson('Product not found.', Response::HTTP_NOT_FOUND);
        }

        try {
            $favoriteManager->addFavorite($this->getUser(), $product);
        } catch (UniqueConstraintViolationException $exception) {
            return new Response();
        } catch (Exception $exception) {
            return $this->errorJson($exception);
        }

        return new Response();
    }

    /**
     * @Route("/favorites", name="delete_favorite", methods={"DELETE"})
     * @Security("has_role('ROLE_USER')")
     *
     * @SWG\Delete(
     *     tags={"favorites"},
     *     path="/favorites",
     *     description="Delete product from favorites",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(in="header", name="Um-Access-Token", description="API token for authorized user", type="string"),
     *     @SWG\Parameter(in="formData", name="product_id", description="Product ID for delete from user's favorites", type="integer"),
     *     @SWG\Response(
     *          response=404,
     *          description="Product not found.",
     *     ),
     *     @SWG\Response(
     *          response=403,
     *          description="Full authentication is required to access this resource.",
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="OK"
     *     )
     * )
     *
     * @param Request         $request
     * @param FavoriteManager $favoriteManager
     *
     * @return JsonResponse
     *
     * @throws LogicException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    public function removeFavorite(Request $request, FavoriteManager $favoriteManager): Response
    {
        $productId = $request->get('product_id');

        $repository = $this->getDoctrine()->getRepository('AppBundle:Product');
        $product    = $repository->find($productId);

        if (!$product) {
            return $this->errorJson('Product not found.', Response::HTTP_NOT_FOUND);
        }

        try {
            $favoriteManager->removeFavorite($this->getUser(), $product);
        } catch (Exception $exception) {
            return $this->errorJson($exception);
        }

        return new Response();
    }


    /**
     * @Route("/favorites/{product_id}", name="delete_favorite_old", requirements={"product_id"="\d+"}, methods={"DELETE"})
     * @Security("has_role('ROLE_USER')")
     *
     * @SWG\Delete(
     *     tags={"favorites"},
     *     path="/favorites/{product_id}",
     *     deprecated=true,
     *     description="Delete product from favorites",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(in="header", name="Um-Access-Token", description="API token for authorized user", type="string"),
     *     @SWG\Parameter(in="path", name="product_id", description="Product ID for delete from user's favorites", type="integer"),
     *     @SWG\Response(
     *          response=404,
     *          description="Product not found.",
     *     ),
     *     @SWG\Response(
     *          response=403,
     *          description="Full authentication is required to access this resource.",
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="OK"
     *     )
     * )
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    public function removeFavoriteOld(Request $request, FavoriteManager $favoriteManager): JsonResponse
    {
        $productId = $request->get('product_id');

        $repository = $this->getDoctrine()->getRepository('AppBundle:Product');
        $product    = $repository->find($productId);

        if (!$product) {
            return $this->errorJson('Product not found.', Response::HTTP_NOT_FOUND);
        }

        try {
            $favoriteManager->removeFavorite($this->getUser(), $product);
        } catch (Exception $exception) {
            return $this->errorJson($exception);
        }

        return new JsonResponse(null, Response::HTTP_NO_CONTENT);
    }

}
