<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Parameters\ParametersNaming;
use AppBundle\Serializer\SerializationGroups;
use AppBundle\Service\CartService;
use JMS\Serializer\SerializationContext;
use LogicException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\{
    Route, Security
};
use Swagger\Annotations as SWG;
use Symfony\Component\DependencyInjection\Exception\{
    ServiceCircularReferenceException, ServiceNotFoundException
};
use Symfony\Component\HttpFoundation\{
    JsonResponse, Request, Response
};
use Symfony\Component\HttpKernel\Exception\HttpException;

/**
 * Class CartController
 *
 * @package AppBundle\Controller
 */
class CartController extends ApiController
{

    /**
     *
     */
    private const PARAM_PRODUCT_ID = 'product_id';

    /**
     * @Route("/cart", name="get_cart", methods={"GET"})
     * @Security("has_role('ROLE_USER')")
     *
     * @SWG\Get(
     *     tags={"cart"},
     *     description="Getting product list from cart",
     *     produces={"application/json"},
     *     @SWG\Parameter(in="header", name="Um-Access-Token", description="API token for authorized user", type="string"),
     *     @SWG\Response(
     *          response=403,
     *          description="Full authentication is required to access this resource.",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="OK",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(type="object",
     *                 @SWG\Property(property="items", type="array",
     *                     @SWG\Items(type="object",
     *                         @SWG\Property(property="count", type="integer"),
     *                         @SWG\Property(
     *                             property="product",
     *                             type="object",
     *                             @SWG\Property(property="id", type="integer"),
     *                             @SWG\Property(property="name", type="string"),
     *                             @SWG\Property(property="is_china_product", type="boolean"),
     *                             @SWG\Property(property="in_stock", type="boolean"),
     *                             @SWG\Property(property="rating", type="integer"),
     *                             @SWG\Property(property="in_favorite", type="boolean"),
     *                             @SWG\Property(property="is_out_of_stock", type="boolean"),
     *                             @SWG\Property(property="price", type="integer"),
     *                             @SWG\Property(property="images", type="array",
     *                                 @SWG\Items(type="object",
     *                                     @SWG\Property(property="preview", type="string"),
     *                                     @SWG\Property(property="large", type="string"),
     *                                     @SWG\Property(property="full", type="string"),
     *                                     @SWG\Property(property="sort", type="integer")
     *                                 )
     *                             )
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     )
     * )
     *
     * @param CartService $cartService
     *
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws LogicException
     */
    public function getCartAction(CartService $cartService): JsonResponse
    {
        /** @var User $user */
        $user = $this->getUser();

        $context = SerializationContext::create()->setGroups([SerializationGroups::PRODUCT_PREVIEW]);

        return $this->renderJson(
            $cartService->getFormattedCart($cartService->getProductList($user)),
            JsonResponse::HTTP_OK,
            $context
        );
    }

    /**
     * @Route("/cart", name="put_cart", methods={"PUT"})
     * @Security("has_role('ROLE_USER')")
     *
     * @SWG\Put(
     *     tags={"cart"},
     *     description="Add product to cart",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(in="header", name="Um-Access-Token", description="API token for authorized user", type="string"),
     *     @SWG\Parameter(in="formData", name="product_id", type="integer", description="Product ID for adding to cart"),
     *     @SWG\Parameter(in="formData", name="count", type="integer", description="Count of product"),
     *     @SWG\Response(
     *          response=404,
     *          description="Product not found.",
     *     ),
     *     @SWG\Response(
     *          response=403,
     *          description="Full authentication is required to access this resource.",
     *     ),
     *     @SWG\Response(
     *          response=400,
     *          description="Parameters missed. || Wrong count.",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Array products in cart by current user",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(type="object",
     *                 @SWG\Property(property="items", type="array",
     *                     @SWG\Items(type="object",
     *                         @SWG\Property(property="count", type="integer"),
     *                         @SWG\Property(
     *                             property="product",
     *                             type="object",
     *                             @SWG\Property(property="id", type="integer"),
     *                             @SWG\Property(property="name", type="string"),
     *                             @SWG\Property(property="is_china_product", type="boolean"),
     *                             @SWG\Property(property="in_stock", type="boolean"),
     *                             @SWG\Property(property="rating", type="integer"),
     *                             @SWG\Property(property="in_favorite", type="boolean"),
     *                             @SWG\Property(property="is_out_of_stock", type="boolean"),
     *                             @SWG\Property(property="price", type="integer"),
     *                             @SWG\Property(property="images", type="array",
     *                                 @SWG\Items(type="object",
     *                                     @SWG\Property(property="preview", type="string"),
     *                                     @SWG\Property(property="large", type="string"),
     *                                     @SWG\Property(property="full", type="string"),
     *                                     @SWG\Property(property="sort", type="integer")
     *                                 )
     *                             )
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     )
     * )
     *
     * @param Request     $request
     * @param CartService $cartService
     *
     * @return JsonResponse
     *
     * @throws LogicException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    public function addToCartAction(Request $request, CartService $cartService): JsonResponse
    {
        $productId = $request->get(self::PARAM_PRODUCT_ID);
        $count     = $request->get(ParametersNaming::COUNT);

        if (null === $count || null === $productId) {
            return $this->errorJson('Parameters missed.', Response::HTTP_BAD_REQUEST);
        }

        if ((int)$count < 1) {
            return $this->errorJson('Wrong count.', Response::HTTP_BAD_REQUEST);
        }

        $repository = $this->getDoctrine()->getRepository('AppBundle:Product');
        $product    = $repository->find($productId);

        if (null === $product) {
            return $this->errorJson('Product not found.', Response::HTTP_NOT_FOUND);
        }

        $cartProducts = $cartService->addProduct($this->getUser(), $product, $count);

        $context = SerializationContext::create()->setGroups([SerializationGroups::PRODUCT_PREVIEW]);

        return $this->renderJson(
            $cartService->getFormattedCart($cartProducts),
            JsonResponse::HTTP_OK,
            $context
        );
    }

    /**
     * @Route("/cart", name="cart_update", methods={"POST"})
     * @Security("has_role('ROLE_USER')")
     *
     * @SWG\Post(
     *     tags={"cart"},
     *     description="Update product count",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(in="header", name="Um-Access-Token", description="API token for authorized user", type="string"),
     *     @SWG\Parameter(in="formData", name="product_id", type="integer", description="Product ID for adding to cart"),
     *     @SWG\Parameter(in="formData", name="count", type="integer", description="Count of product"),
     *     @SWG\Response(
     *          response=404,
     *          description="Product not found.",
     *     ),
     *     @SWG\Response(
     *          response=403,
     *          description="Full authentication is required to access this resource.",
     *     ),
     *     @SWG\Response(
     *          response=400,
     *          description="Wrong parameters. || Wrong count.",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Array products in cart by current user",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(type="object",
     *                 @SWG\Property(property="items", type="array",
     *                     @SWG\Items(type="object",
     *                         @SWG\Property(property="count", type="integer"),
     *                         @SWG\Property(
     *                             property="product",
     *                             type="object",
     *                             @SWG\Property(property="id", type="integer"),
     *                             @SWG\Property(property="name", type="string"),
     *                             @SWG\Property(property="is_china_product", type="boolean"),
     *                             @SWG\Property(property="in_stock", type="boolean"),
     *                             @SWG\Property(property="rating", type="integer"),
     *                             @SWG\Property(property="in_favorite", type="boolean"),
     *                             @SWG\Property(property="is_out_of_stock", type="boolean"),
     *                             @SWG\Property(property="price", type="integer"),
     *                             @SWG\Property(property="images", type="array",
     *                                 @SWG\Items(type="object",
     *                                     @SWG\Property(property="preview", type="string"),
     *                                     @SWG\Property(property="large", type="string"),
     *                                     @SWG\Property(property="full", type="string"),
     *                                     @SWG\Property(property="sort", type="integer")
     *                                 )
     *                             )
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     ),
     *     @SWG\Response(
     *          response="454",
     *          description="UMKA User-Friendly error",
     *          @SWG\Schema(
     *              type="object",
     *              @SWG\Property(property="message", type="string")
     *          )
     *     )
     * )
     *
     * @param Request     $request
     * @param CartService $cartService
     *
     * @return JsonResponse
     *
     * @throws HttpException
     * @throws \LogicException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    public function updateAction(Request $request, CartService $cartService): JsonResponse
    {
        $productId = $request->get(self::PARAM_PRODUCT_ID);
        $count     = $request->get(ParametersNaming::COUNT);

        if (null === $count || null === $productId) {
            return $this->errorJson('Wrong parameters.', Response::HTTP_BAD_REQUEST);
        }

        if ((int)$count < 1) {
            return $this->errorJson('Wrong count.', Response::HTTP_BAD_REQUEST);
        }

        $repository = $this->getDoctrine()->getRepository('AppBundle:Product');
        $product    = $repository->find($productId);

        if (null === $product) {
            return $this->errorJson('Product not found.', Response::HTTP_NOT_FOUND);
        }

        $cartProducts = $cartService->updateProduct($this->getUser(), $product, $count);

        $context = SerializationContext::create()->setGroups([SerializationGroups::PRODUCT_PREVIEW]);

        return $this->renderJson(
            $cartService->getFormattedCart($cartProducts),
            JsonResponse::HTTP_OK,
            $context
        );
    }

    /**
     * @Route("/cart", name="delete_cart", methods={"DELETE"})
     * @Security("has_role('ROLE_USER')")
     *
     * @SWG\Delete(
     *     tags={"cart"},
     *     description="Delete product from cart",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(in="header", name="Um-Access-Token", description="API token for authorized user", type="string"),
     *     @SWG\Parameter(in="formData", name="product_id", type="integer", description="Product ID for deleting from cart"),
     *     @SWG\Response(
     *          response=404,
     *          description="Product not found.",
     *     ),
     *     @SWG\Response(
     *          response=403,
     *          description="Full authentication is required to access this resource.",
     *     ),
     *     @SWG\Response(
     *          response=400,
     *          description="Parameters missed.",
     *     ),
     *     @SWG\Response(
     *         response=200,
     *         description="Array products in cart by current user",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(type="object",
     *                 @SWG\Property(property="items", type="array",
     *                     @SWG\Items(type="object",
     *                         @SWG\Property(property="count", type="integer"),
     *                         @SWG\Property(
     *                             property="product",
     *                             type="object",
     *                             @SWG\Property(property="id", type="integer"),
     *                             @SWG\Property(property="name", type="string"),
     *                             @SWG\Property(property="is_china_product", type="boolean"),
     *                             @SWG\Property(property="in_stock", type="boolean"),
     *                             @SWG\Property(property="rating", type="integer"),
     *                             @SWG\Property(property="in_favorite", type="boolean"),
     *                             @SWG\Property(property="is_out_of_stock", type="boolean"),
     *                             @SWG\Property(property="price", type="integer"),
     *                             @SWG\Property(property="images", type="array",
     *                                 @SWG\Items(type="object",
     *                                     @SWG\Property(property="preview", type="string"),
     *                                     @SWG\Property(property="large", type="string"),
     *                                     @SWG\Property(property="full", type="string"),
     *                                     @SWG\Property(property="sort", type="integer")
     *                                 )
     *                             )
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     )
     * )
     *
     * @param Request     $request
     * @param CartService $cartService
     *
     * @return JsonResponse
     *
     * @throws LogicException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    public function removeFromCartAction(Request $request, CartService $cartService): JsonResponse
    {
        $productId = $request->get(self::PARAM_PRODUCT_ID);

        if (null === $productId) {
            return $this->errorJson('Parameters missed.', Response::HTTP_BAD_REQUEST);
        }

        $repository = $this->getDoctrine()->getRepository('AppBundle:Product');
        $product    = $repository->find($productId);

        if (null === $product) {
            return $this->errorJson('Product not found.', Response::HTTP_NOT_FOUND);
        }

        $cartProducts = $cartService->deleteProduct($this->getUser(), $product);

        $context = SerializationContext::create()->setGroups([SerializationGroups::PRODUCT_PREVIEW]);

        return $this->renderJson(
            $cartService->getFormattedCart($cartProducts),
            JsonResponse::HTTP_OK,
            $context
        );
    }

}
