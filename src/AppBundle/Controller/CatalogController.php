<?php

namespace AppBundle\Controller;

use AppBundle\Entity\Category;
use AppBundle\Filter\CatalogSearchFilter;
use AppBundle\Manager\ImageManager;
use AppBundle\Manager\ProductManager;
use AppBundle\Parameters\ParametersNaming;
use AppBundle\Serializer\SerializationGroups;
use AppBundle\Service\SolrManager;
use AppBundle\ValidationModel\CategoryFilterValidationModel;
use JMS\Serializer\SerializationContext;
use Knp\Bundle\PaginatorBundle\Pagination\SlidingPagination;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\Exception\{
    ServiceCircularReferenceException, ServiceNotFoundException
};
use Nelmio\ApiDocBundle\Annotation as Nelmio;
use Symfony\Component\HttpFoundation\{
    JsonResponse, Request
};
use Swagger\Annotations as SWG;
use Symfony\Component\Validator\{
    ConstraintViolationList, Exception\ValidatorException, Validator\TraceableValidator, Validator\ValidatorInterface
};

/**
 * Class CatalogControllerController
 *
 * @package AppBundle\Controller
 * @Route("/catalog")
 */
class CatalogController extends ApiController
{
    /**
     * @var TraceableValidator
     */
    private $validator;

    /**
     * CatalogController constructor.
     *
     * @param ValidatorInterface $validator
     */
    public function __construct(ValidatorInterface $validator)
    {
        $this->validator = $validator;
    }

    /**
     * @Route(
     *     "/{id}",
     *     name="category_view",
     *     requirements={"id"="\d+"},
     *     methods={"POST"}
     *     )
     *
     * @param Category       $category
     * @param Request        $request
     * @param ProductManager $productManager
     *
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws \LogicException
     * @throws \Doctrine\ORM\NonUniqueResultException
     *
     * @SWG\Parameter(
     *     in="header",
     *     name="Um-Access-Token",
     *     description="API token for authorized user",
     *     type="string"
     * ),
     * @SWG\Parameter(
     *      name="page",
     *      in="query",
     *      type="integer",
     *      description="Page number"
     * )
     *
     * @SWG\Parameter(
     *     name="count",
     *     in="query",
     *     type="integer",
     *     description="Per page number"
     * )
     *
     * @SWG\Parameter(
     *        name="JSON params body",
     *        in="body",
     *        description="json select request object",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="in_stock", type="boolean"),
     *             @SWG\Property(property="producers", type="array",
     *                  @SWG\Items(type="integer")
     *              ),
     *             @SWG\Property(property="price", type="object",
     *                  @SWG\Property(property="from", type="float|null"),
     *                  @SWG\Property(property="to", type="float|null")
     *              ),
     *             @SWG\Property(property="attributes", type="array",
     *                  @SWG\Items(
     *                      type="object",
     *                      @SWG\Property(property="id", type="integer"),
     *                      @SWG\Property(property="type", type="enum"),
     *                      @SWG\Property(property="options", type="array",
     *                          @SWG\Items(type="string|integer")
     *                      ),
     *                      @SWG\Property(property="option", type="string|integer"),
     *                      @SWG\Property(property="checkbox", type="boolean|null"),
     *                      @SWG\Property(property="between", type="object",
     *                          @SWG\Property(property="from", type="integer|null"),
     *                          @SWG\Property(property="to", type="integer|null")
     *                      )
     *                  )
     *              ),
     *             @SWG\Property(property="sorting", type="object",
     *                  @SWG\Property(property="direction", type="ASC|DESC"),
     *                  @SWG\Property(property="param", type="string")
     *              )
     *         )
     *   )
     * @SWG\Response(
     *     response=404,
     *     description="AppBundle\\Entity\\Category object not found.",
     * ),
     * @SWG\Response(
     *     response=200,
     *     description="OK
     *                  Headers : X-Pagination-Page-Count (integer) : Общее количество страниц.",
     *     @SWG\Schema(
     *         type="array",
     *         @SWG\Items(type="object",
     *             @SWG\Property(property="items", type="array",
     *                 @SWG\Items(type="object",
     *                     @SWG\Property(property="count", type="integer"),
     *                     @SWG\Property(
     *                         property="product",
     *                         type="object",
     *                         @SWG\Property(property="id", type="integer"),
     *                         @SWG\Property(property="name", type="string"),
     *                         @SWG\Property(property="is_china_product", type="boolean"),
     *                         @SWG\Property(property="in_stock", type="boolean"),
     *                         @SWG\Property(property="rating", type="integer"),
     *                         @SWG\Property(property="in_favorites", type="boolean"),
     *                         @SWG\Property(property="is_out_of_stock", type="boolean"),
     *                         @SWG\Property(property="price", type="float"),
     *                         @SWG\Property(property="discount_price", type="float"),
     *                         @SWG\Property(property="images", type="array",
     *                             @SWG\Items(type="object",
     *                                 @SWG\Property(property="preview", type="string"),
     *                                 @SWG\Property(property="large", type="string"),
     *                                 @SWG\Property(property="full", type="string"),
     *                                 @SWG\Property(property="sort", type="integer")
     *                             )
     *                         )
     *                     )
     *                 )
     *             )
     *         )
     *     )
     * )
     *
     * @SWG\Tag(name="catalog")
     */
    public function viewAction(Category $category, Request $request, ProductManager $productManager): JsonResponse
    {
        $model         = CategoryFilterValidationModel::makeInstance($request);
        /** @var ConstraintViolationList $violationList */
        $violationList = $this->validator->validate($model);
        if (\count($violationList) !== 0) {
            $error = $model->makeErrorString($violationList);
            throw new ValidatorException($error);
        }

        $page    = $request->query->getInt(ParametersNaming::PAGE, self::FIRST_PAGE_NUMBER);
        $perPage = $request->query->getInt(ParametersNaming::COUNT, self::PER_PAGE_COUNT);

        $params = $this->getJsonBody();
        $filter = new CatalogSearchFilter();
        $filter->fillFromArrayCollection($params);

        $queryBuilder = $productManager->getProductQueryByCategoryWithAdditional($category, $filter);

        /** @var SlidingPagination $pagination */
        $pagination = $this->get('knp_paginator')->paginate(
            $queryBuilder->getQuery(),
            $page,
            $perPage
        );

        $context = SerializationContext::create()->setGroups(SerializationGroups::PRODUCT_PREVIEW);
        return $this->renderJson(
            $pagination->getItems(),
            JsonResponse::HTTP_OK,
            $context,
            [
                self::PAGINATION_COUNT_HEADER => $pagination->getPageCount()
            ]
        );
    }

    /**
     * @Nelmio\Operation(
     *     summary="Get products catalog by filters from Solr Server",
     *     tags={"catalog"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     deprecated=true,
     *     @SWG\Parameter(
     *          in="header",
     *          name="Um-Access-Token",
     *          description="API token for authorized user",
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="page",
     *         in="formData",
     *         type="integer",
     *         description="Page number"
     *     ),
     *     @SWG\Parameter(
     *         name="perPage",
     *         in="formData",
     *         type="integer",
     *         description="Per page number"
     *     ),
     *     @SWG\Parameter(
     *         name="isChinaProduct",
     *         in="formData",
     *         type="boolean",
     *         description="Is china product"
     *     ),
     *     @SWG\Parameter(
     *         name="category",
     *         in="formData",
     *         type="integer",
     *         description="Category id"
     *     ),
     *     @SWG\Parameter(
     *         name="priceFrom",
     *         in="formData",
     *         type="number",
     *         description="Lowest price"
     *     ),
     *     @SWG\Parameter(
     *         name="priceTo",
     *         in="formData",
     *         type="number",
     *         description="Highest price"
     *     ),
     *     @SWG\Parameter(
     *         name="producers[]",
     *         in="formData",
     *         type="array",
     *         collectionFormat="multi",
     *         description="producers Id array",
     *         @SWG\Items(
     *             type="integer"
     *         )
     *     ),
     *     @SWG\Parameter(
     *         name="attributes",
     *         in="formData",
     *         type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="sorting",
     *         in="formData",
     *         type="string"
     *     ),
     *
     *     @SWG\Response(
     *         response=200,
     *         description="OK
     *             Headers : X-Pagination-Page-Count (integer) : Общее количество страниц.",
     *         @SWG\Schema(
     *             type="array",
     *             @SWG\Items(type="object",
     *                 @SWG\Property(property="id", type="integer"),
     *                 @SWG\Property(property="price", type="integer"),
     *                 @SWG\Property(property="discount_price", type="integer"),
     *                 @SWG\Property(property="image", type="array",
     *                      @SWG\Items(type="object",
     *                         @SWG\Property(property="preview", type="string"),
     *                         @SWG\Property(property="large", type="string"),
     *                         @SWG\Property(property="full", type="string"),
     *                         @SWG\Property(property="sort", type="integer")
     *                      )
     *                 ),
     *                 @SWG\Property(property="name", type="string")
     *             )
     *         )
     *     )
     * )
     *
     * @Route(
     *      path="",
     *      name="catalog_view",
     *      methods={"POST"}
     * )
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function getSearchCatalogAction(Request $request): JsonResponse
    {
        $solrClient   = $this->get('solarium.client');
        $imageManager = new ImageManager(
            $this->getParameter('image_host'),
            $this->getParameter('image_upload_dir'),
            $this->getParameter('image_thumbs_dir'));
        $solrManager  = new SolrManager($solrClient, $imageManager);

        $offset = $request->request->getInt(ParametersNaming::PAGE, self::FIRST_PAGE_NUMBER);
        $limit  = $request->request->get(ParametersNaming::PER_PAGE, self::PER_PAGE_COUNT);
        $request->request->remove(ParametersNaming::PAGE);
        $request->request->remove(ParametersNaming::PER_PAGE);
        $params = $request->request->all();

        $offset = ($offset - self::FIRST_PAGE_NUMBER) * $limit;

        $result     = $solrManager->getCatalogData($params, $limit, $offset);
        $resultData = $solrManager->getFormattedProductData($result);

        return $this->renderJson(
            $resultData,
            JsonResponse::HTTP_OK,
            null,
            [
                self::PAGINATION_COUNT_HEADER => ceil($result->getNumFound() / $limit)
            ]
        );
    }
}
