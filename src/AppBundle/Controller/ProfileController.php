<?php

namespace AppBundle\Controller;

use AppBundle\Entity\{
    User, UserSocial, UserToken
};
use Symfony\Component\HttpFoundation\{
    JsonResponse, Request, Response
};
use Swagger\Annotations as SWG;
use AppBundle\Form\User\ProfileForm;
use Nelmio\ApiDocBundle\Annotation\Model;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Component\Form\Exception\AlreadySubmittedException;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;

/**
 * Class ProfileController
 *
 * @package AppBundle\Controller
 */
class ProfileController extends ApiController
{
    /**
     * @Route("/profile", name="user_info", methods={"GET"})
     * @Security("has_role('ROLE_USER')")
     *
     * @SWG\Get(
     *     tags={"authorization"},
     *     description="Return current profile info",
     *     produces={"application/json"},
     *     @SWG\Parameter(in="header", name="Um-Access-Token", description="API token for authorized user", type="string"),
     *     @SWG\Response(
     *          response=200,
     *          description="Array of favored products by current user",
     *          @Model(type=User::class)
     *     )
     * )
     *
     * @return JsonResponse
     * @throws ServiceCircularReferenceException
     */
    public function profileAction(): JsonResponse
    {
        /** @noinspection ExceptionsAnnotatingAndHandlingInspection */
        return $this->renderJson($this->getUser());
    }

    /**
     * @Route("/profile", name="update_user", methods={"PUT"})
     * @Security("has_role('ROLE_USER')")
     *
     * @param Request $request
     *
     * @SWG\Put(
     *     tags={"authorization"},
     *     description="Update user information",
     *     produces={"application/json"},
     *     consumes={"application/x-www-form-urlencoded"},
     *     @SWG\Parameter(in="header", name="Um-Access-Token", description="API token for authorized user", type="string"),
     *     @SWG\Parameter(
     *         name="first_name",
     *         in="formData",
     *         description="First name",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *         name="last_name",
     *         in="formData",
     *         description="Last name",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *         name="middle_name",
     *         in="formData",
     *         description="Middle name",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *         name="nick_name",
     *         in="formData",
     *         description="Nick name",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="Registered user email",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *         name="gender",
     *         in="formData",
     *         description="User's gender: 1 - male, 2 - female, 0 - unknown",
     *         type="string",
     *         enum={0, 1, 2}
     *     ),
     *     @SWG\Parameter(
     *          name="birth_date[year]",
     *          in="formData",
     *          description="User's birth year",
     *          type="string",
     *          format="date-time",
     *     ),
     *     @SWG\Parameter(
     *          name="birth_date[month]",
     *          in="formData",
     *          description="User's birth month",
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *          name="birth_date[day]",
     *          in="formData",
     *          description="User's birth day",
     *          type="string"
     *     ),
     *     @SWG\Parameter(
     *         name="phone",
     *         in="formData",
     *         description="Registered user's phone",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Response(
     *          response=204,
     *          description=""
     *     )
     * )
     *
     * @return JsonResponse
     * @throws ServiceCircularReferenceException
     * @throws \LogicException
     * @throws AlreadySubmittedException
     */
    public function editProfileAction(Request $request): JsonResponse
    {
        $user = $this->getDoctrine()->getRepository('AppBundle:User')->find($this->getUser()->getId());

        $form = $this->createForm(ProfileForm::class, $user);
        $form->submit($request->request->all(), false);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($user);
            $em->flush();

            return $this->renderJson(null, Response::HTTP_NO_CONTENT);
        }

        $errorString = (string)$form->getErrors(true, false);
        return new JsonResponse($errorString, Response::HTTP_BAD_REQUEST);
    }
}
