<?php

namespace AppBundle\Controller;

use AppBundle\Entity\{
    User, UserSocial, UserToken
};
use AppBundle\Exception\User\HttpDuplicateUserException;
use AppBundle\Manager\UserManager;
use AppBundle\Service\{
    AuthService, Social\SocialAuthService, Social\SocialProviderInterface
};
use Doctrine\DBAL\ConnectionException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\{
    Route, Security
};
use Symfony\Component\DependencyInjection\{
    Exception\ServiceCircularReferenceException, Exception\ServiceNotFoundException
};
use Symfony\Component\HttpFoundation\{
    JsonResponse, Request, Response
};
use Symfony\Component\HttpKernel\{
    Exception\HttpException, Exception\HttpExceptionInterface, Exception\NotFoundHttpException, Exception\UnauthorizedHttpException
};
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * Class AuthController
 *
 * @package AppBundle\Controller
 */
class AuthController extends ApiController
{
    /** @var UserManager */
    protected $userManager;
    /** @var AuthService */
    protected $authService;
    /** @var SocialAuthService */
    private $socialAuthService;

    /**
     * UserController constructor.
     *
     * @param UserManager       $userManager
     * @param AuthService       $authService
     * @param SocialAuthService $socialAuthService
     */
    public function __construct(UserManager $userManager, AuthService $authService, SocialAuthService $socialAuthService)
    {
        $this->userManager       = $userManager;
        $this->authService       = $authService;
        $this->socialAuthService = $socialAuthService;
    }

    public const REQ_PARAM_EMAIL           = 'email';
    public const REQ_PARAM_PASSWORD        = 'password';
    public const REQ_PARAM_DEVICE_ID       = 'device_id';
    public const REQ_PARAM_NEW_DEVICE_ID   = 'new_device_id';
    public const REQ_PARAM_GOOGLE_ACCOUNT  = 'google_id';
    public const REQ_PARAM_SOCIAL_PROVIDER = 'provider';
    public const REQ_PARAM_SOCIAL_TOKEN    = 'token';

    /**
     * @Route("/auth/login", name="user_login_by_email", methods={"POST"})
     *
     * @SWG\Post(
     *     tags={"authorization"},
     *     description="Authorization by default email + pass scheme.",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="email",
     *         in="formData",
     *         description="Registered user email",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *         name="password",
     *         in="formData",
     *         description="User password",
     *         required=false,
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="Return json-object. Field access_token should be placed to header 'Um-Access-Token'. Field 'cookie_token' contains site cookie name and value separated by ':' symbol ",
     *          @Model(type=UserToken::class)
     *     )
     * )
     *
     * @param Request     $request
     * @param UserManager $userManager
     *
     * @return JsonResponse
     *
     * @internal param UserManager $userManager
     * @internal param AuthService $authService
     *
     * @throws UnauthorizedHttpException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws NotFoundHttpException
     * @throws \RuntimeException
     * @throws \LogicException
     * @throws ConnectionException
     */
    public function loginByEmailAction(Request $request, UserManager $userManager): JsonResponse
    {
        $email    = $request->get(static::REQ_PARAM_EMAIL);
        $password = $request->get(static::REQ_PARAM_PASSWORD);

        if (!$email || !$password) {
            return $this->errorJson('Not enough data', 400);
        }

        $user = $this->userManager->getUserByEmailAndPassword($email, $password);

        if (($deviceIdUser = $this->getUser()) && $deviceIdUser->getId() !== $user->getId()) {
            $userManager->mergeUsers($deviceIdUser, $user);
        }
        return $this->loginResponseByUser($user);
    }

    /**
     * @Route("/auth/google_id", name="user_login_android", methods={"POST"})
     *
     *
     * @SWG\Post(
     *     tags={"authorization"},
     *     description="Authorization by device ID and gmail account",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     deprecated=true,
     *     @SWG\Parameter(
     *         name="device_id",
     *         in="formData",
     *         description="Android device ID value",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *         name="google_id",
     *         in="formData",
     *         description="Google account name or email",
     *         required=false,
     *         type="string",
     *         required=false,
     *         schema={}
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="Return json-object. Field access_token should be placed to header 'Um-Access-Token'. Field 'cookie_token' contains site cookie name and value separated by ':' symbol ",
     *          @Model(type=UserToken::class)
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws HttpDuplicateUserException
     */
    public function loginByAndroidIdAction(Request $request): JsonResponse
    {
        $deviceId          = $request->get(static::REQ_PARAM_DEVICE_ID);
        $googleAccountName = $request->get(static::REQ_PARAM_GOOGLE_ACCOUNT);

        if (null === $deviceId) {
            return $this->errorJson(new HttpException(Response::HTTP_FAILED_DEPENDENCY, 'Can not identify user device'));
        }

        $user = $this->userManager->getOrCreateAndroidUser($deviceId, $googleAccountName);
        return $this->loginResponseByUser($user);
    }

    /**
     * @Route("/auth/device_id", name="user_login_mobile", methods={"POST"})
     *
     * @SWG\Post(
     *     tags={"authorization"},
     *     description="Authorization by device ID",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="device_id",
     *         in="formData",
     *         description="Mobile device ID value",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="Return json-object. Field access_token should be placed to header 'Um-Access-Token'. Field 'cookie_token' contains site cookie name and value separated by ':' symbol ",
     *          @Model(type=UserToken::class)
     *     ),
     *     @SWG\Response(
     *          response="424",
     *          description="Device identify failed",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="integer", example=424),
     *             @SWG\Property(property="message", type="string", example="Can not identify user device")
     *         )
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws HttpDuplicateUserException
     */
    public function loginByMobileIdAction(Request $request): JsonResponse
    {
        $deviceId = $request->get(static::REQ_PARAM_DEVICE_ID);

        if (null === $deviceId) {
            return $this->errorJson('Can not identify user device', Response::HTTP_FAILED_DEPENDENCY);
        }

        $user = $this->userManager->getOrCreateAndroidUser($deviceId);
        return $this->loginResponseByUser($user);
    }

    /**
     * @Route("/auth/device_id", name="update_user_device_id", methods={"PUT"})
     *
     * @SWG\Put(
     *     tags={"authorization"},
     *     description="Update user device ID",
     *     consumes={"application/x-www-form-urlencoded"},
     *     produces={"application/json"},
     *     @SWG\Parameter(
     *         name="device_id",
     *         in="formData",
     *         description="Old Google ID value",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Parameter(
     *         name="new_device_id",
     *         in="formData",
     *         description="Mobile device ID value",
     *         type="string",
     *         schema={}
     *     ),
     *     @SWG\Response(
     *          response=200,
     *          description="Return json-object. Field access_token should be placed to header 'Um-Access-Token'. Field 'cookie_token' contains site cookie name and value separated by ':' symbol ",
     *          @Model(type=UserToken::class)
     *     ),
     *     @SWG\Response(
     *          response="424",
     *          description="Device identify failed",
     *         @SWG\Schema(
     *             type="object",
     *             @SWG\Property(property="status", type="integer", example=424),
     *             @SWG\Property(property="message", type="string", example="Can not identify user device")
     *         )
     *     )
     * )
     *
     * @param Request $request
     * @return JsonResponse
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws HttpDuplicateUserException
     */
    public function updateUserDeviceIdAction(Request $request): JsonResponse
    {
        $deviceId    = $request->get(static::REQ_PARAM_DEVICE_ID);
        $newDeviceId = $request->get(static::REQ_PARAM_NEW_DEVICE_ID);

        if (null === $deviceId || null === $newDeviceId) {
            return $this->errorJson('Can not identify user device', Response::HTTP_FAILED_DEPENDENCY);
        }

        $user = $this->userManager->updateMobileUser($deviceId, $newDeviceId);
        return $this->loginResponseByUser($user);
    }

    /**
     * @Route("/auth/oauth", name="user_login_oauth", methods={"POST"})
     *
     * @param Request $request
     * @return JsonResponse
     * @SWG\Parameter(
     *         name="provider",
     *         in="formData",
     *         description="OAuth2 data provider name",
     *         type="string"
     * )
     * @SWG\Parameter(
     *         name="token",
     *         in="formData",
     *         description="OAuth2 authorization token",
     *         required=false,
     *         type="string"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="!Need testing! Field access_token should be placed to header 'Um-Access-Token'. Field 'cookie_token' contains site cookie name and value separated by ':' symbol",
     *     @SWG\Schema(@Model(type=UserToken::class))
     * )
     *
     * @SWG\Response(
     *      response="412",
     *      description="Invalid parameters",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="status", type="integer", example=412),
     *          @SWG\Property(property="message", type="string", example="Can not identify user device")
     *      )
     * )
     *
     * @SWG\Response(
     *      response="404",
     *      description="User not found",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(property="status", type="integer", example=404),
     *          @SWG\Property(property="message", type="string", example="Error: can not fetch data by user")
     *      )
     * )
     *
     * @SWG\Tag(name="authorization")
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws HttpDuplicateUserException
     * @throws \Exception
     */
    public function loginByOauth2Action(Request $request): JsonResponse
    {
        $email = 'buyer@gmail.com';
        $userRepository = $this->getDoctrine()->getRepository('AppBundle:User');

        $user = $userRepository->findActiveByEmail($email);
        if (null === $user) {
            throw new NotFoundHttpException('User not found');
        }

        return $this->loginResponseByUser($user);

        $socialProviderName = $request->get(static::REQ_PARAM_SOCIAL_PROVIDER);
        $token              = $request->get(static::REQ_PARAM_SOCIAL_TOKEN);
        $email              = $request->get(static::REQ_PARAM_EMAIL);

        if (null === $socialProviderName || null === $token) {
            return $this->errorJson('Invalid parameters: provider or token', Response::HTTP_PRECONDITION_FAILED);
        }

        if ($socialProviderName === SocialProviderInterface::VK_LOGIN && null === $email) {
            return $this->errorJson('Invalid parameters: email missed', Response::HTTP_PRECONDITION_FAILED);
        }

        /** @var UserSocial|null $user */
        $userSocial = $this->socialAuthService->authenticate($socialProviderName, $token, $email);
        if (null === $userSocial) {
            return $this->errorJson('Error: can not fetch data by user.', Response::HTTP_NOT_FOUND);
        }
        return $this->loginResponseByUser($userSocial->getUser());
    }

    /**
     * @param User $user
     * @return JsonResponse
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     */
    private function loginResponseByUser(User $user): JsonResponse
    {
        try {
            $token = $this->authService->authenticate($user);
            return $this->renderJson($token);
        } catch (HttpExceptionInterface $e) {
            return $this->errorJson($e->getMessage(), $e->getStatusCode());
        } catch (\Exception $e) {
            return $this->errorJson($e->getMessage(), 520);
        }
    }
}
