<?php

namespace AppBundle\Controller;

use AppBundle\Parameters\DeliveryDates;
use AppBundle\Repository\{
    AttributeGroupRepository, AttributeOptionRepository, AttributeRepository, CategoryRepository, CityRepository, CountryRepository, ProducerRepository, RegionRepository
};
use Exception;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\Exception\{
    ServiceCircularReferenceException, ServiceNotFoundException
};
use Symfony\Component\HttpFoundation\JsonResponse;
use Swagger\Annotations as SWG;

/**
 * Class StateController
 *
 * @package AppBundle\Controller
 * @Route("/state")
 */
class StateController extends ApiController
{
    /**
     * @Route("", name="state_index", methods={"GET"})
     *
     * @param CategoryRepository        $categoryRepository
     * @param CountryRepository         $countryRepository
     * @param RegionRepository          $regionRepository
     * @param CityRepository            $cityRepository
     * @param ProducerRepository        $producerRepository
     * @param AttributeGroupRepository  $groupRepository
     * @param AttributeRepository       $attributeRepository
     * @param AttributeOptionRepository $optionRepository
     *
     * @return JsonResponse
     *
     * @throws ServiceCircularReferenceException
     * @throws ServiceNotFoundException
     *
     * @SWG\Response(
     *      response=200,
     *      description="OK",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="object_hashes",
     *              type="object",
     *              @SWG\Property(property="attributes", type="string"),
     *              @SWG\Property(property="categories", type="string"),
     *              @SWG\Property(property="cities", type="string"),
     *              @SWG\Property(property="countries", type="string"),
     *              @SWG\Property(property="producers", type="string"),
     *              @SWG\Property(property="regions", type="string"),
     *              @SWG\Property(property="attr_groups", type="string"),
     *              @SWG\Property(property="attr_attributes", type="string"),
     *              @SWG\Property(property="attr_options", type="string")
     *          ),
     *          @SWG\Property(
     *              property="delivery_times",
     *              type="object",
     *              @SWG\Property(
     *                  property="global_crossborder",
     *                  type="object",
     *                  @SWG\Property(property="from", type="integer", example=5),
     *                  @SWG\Property(property="to", type="integer", example=21)
     *              ),
     *              @SWG\Property(
     *                  property="global_fast",
     *                  type="object",
     *                  @SWG\Property(property="from", type="integer", example=15),
     *                  @SWG\Property(property="to", type="integer", example=60)
     *              )
     *          ),
     *          @SWG\Property(
     *              property="attentions",
     *              @SWG\Schema(
     *                  type="array",
     *                  @SWG\Items(
     *                      @SWG\Property(
     *                          property="id",
     *                          type="integer"
     *                      ),
     *                      @SWG\Property(
     *                          property="text",
     *                          type="string"
     *                      ),
     *                      @SWG\Property(
     *                          property="type",
     *                          type="string",
     *                          enum={"error", "warning", "success", "info"}
     *                      )
     *                  )
     *              )
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="state")
     */
    public function indexAction(
        CategoryRepository $categoryRepository,
        CountryRepository $countryRepository,
        RegionRepository $regionRepository,
        CityRepository $cityRepository,
        ProducerRepository $producerRepository,
        AttributeGroupRepository $groupRepository,
        AttributeRepository $attributeRepository,
        AttributeOptionRepository $optionRepository
    ): JsonResponse
    {

        try {
            $hashList = [
                'categories'      => $categoryRepository->getHash(),
                'cities'          => $cityRepository->getHash(),
                'countries'       => $countryRepository->getHash(),
                'producers'       => $producerRepository->getHash(),
                'regions'         => $regionRepository->getHash(),
                'attr_groups'     => $groupRepository->getHash(),
                'attr_attributes' => $attributeRepository->getHash(),
                'attr_options'    => $optionRepository->getHash()
            ];

        } catch (Exception $exception) {
            $this->get('logger')->error($exception->getTraceAsString());

            return $this->errorJson('Error in receiving data.');
        }

        $deliveryTimes = [
            'global_fast'        => [
                'from' => DeliveryDates::FAST_DAYS_COUNT_FROM,
                'to'   => DeliveryDates::FAST_DAYS_COUNT_TO
            ],
            'global_crossborder' => [
                'from' => DeliveryDates::CROSSBORDER_DAYS_COUNT_FROM,
                'to'   => DeliveryDates::CROSSBORDER_DAYS_COUNT_TO
            ]
        ];

        return $this->renderJson([
                                     'object_hashes'  => $hashList,
                                     'delivery_times' => $deliveryTimes
                                 ]);
    }

}
