<?php

namespace AppBundle\Controller;

use AppBundle\Repository\ProductRepository;
use LogicException;
use Doctrine\ORM\NonUniqueResultException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\DependencyInjection\Exception\{
    ServiceCircularReferenceException, ServiceNotFoundException
};
use Symfony\Component\HttpFoundation\{
    JsonResponse, Request
};
use Symfony\Component\HttpKernel\Exception\HttpException;
use AppBundle\Entity\Product;
use Nelmio\ApiDocBundle\Annotation\Model;
use Swagger\Annotations as SWG;

/**
 * Class ProductController
 *
 * @package AppBundle\Controller
 * @Route("/product")
 */
class ProductController extends ApiController
{
    /**
     * @Route("/{id}", name="product_view", requirements={"id"="\d+"}, methods={"GET"})
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws LogicException
     * @throws NonUniqueResultException
     *
     * @SWG\Parameter(
     *     in="header",
     *     name="Um-Access-Token",
     *     description="API token for authorized user",
     *     type="string"
     * )
     *
     * @SWG\Response(
     *     response=200,
     *     description="OK",
     *     @SWG\Schema(@Model(type=Product::class))
     * )
     *
     * @SWG\Response(
     *      response=404,
     *      description="Not found",
     *      @SWG\Schema(
     *          type="object",
     *          @SWG\Property(
     *              property="status",
     *              type="integer",
     *              example=404
     *          ),
     *          @SWG\Property(
     *              property="message",
     *              type="string",
     *              example="Product not found"
     *          )
     *      )
     * )
     *
     * @SWG\Tag(name="product")
     */
    public function view(Request $request): JsonResponse
    {
        /**
         * @var ProductRepository $repository
         */
        $repository = $this->getDoctrine()->getRepository('AppBundle:Product');
        $product    = $repository->findById($request->get('id'));
        $user       = $this->getUser();

        if ($product === null) {
            throw new HttpException(404, 'Product not found');
        }

        $reviews = $product->getReviews();
        foreach ($reviews as $review) {
            if ($review->getIsDeleted()) {
                $product->removeReview($review);
            }

            if ($review->getUser() !== $user && !$review->getIsApproved()) {
                $product->removeReview($review);
            }
        }

        return $this->renderJson($product);
    }

    /**
     * @Route("/similar/{id}", name="similar_products", requirements={"id"="\d+"}, methods={"GET"})
     * @param Request $request
     *
     * @return JsonResponse
     *
     * @throws \Symfony\Component\HttpKernel\Exception\HttpException
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws LogicException
     * @throws NonUniqueResultException
     *
     * @SWG\Response(
     *     response=200,
     *     description="OK",
     * )
     *
     * @SWG\Tag(name="product")
     */
    public function similar(Request $request): JsonResponse
    {
        /**
         * @var ProductRepository $repository
         */
        $repository  = $this->getDoctrine()->getRepository('AppBundle:Product');
        $similarJson = $repository->findSimilarById($request->get('id'));

        if ($similarJson === null) {
            throw new HttpException(404, 'Similar products not found');
        }

        return $this->renderJson($similarJson);
    }
}
