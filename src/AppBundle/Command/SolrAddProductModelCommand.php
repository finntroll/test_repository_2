<?php

namespace AppBundle\Command;


use AppBundle\Entity\Category;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductModel;
use AppBundle\Repository\CategoryRepository;
use AppBundle\Repository\ProductModelRepository;
use Doctrine\ORM\EntityManager;
use Solarium\Client;
use Symfony\Component\Console\Helper\QuestionHelper;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;

class SolrAddProductModelCommand extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('solr:add:model')
            ->setDescription('Add models with products to Solr')
            ->setHelp('Add models with products to Solr');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var Client $client
         */
        $client = $this->getContainer()->get('solarium.client.model');

        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');

        $helper = $this->getHelper('question');

        $operationQuestion = new Question([
                                              "What do you want to do? \n",
                                              "Press: \n",
                                              "[0] - nothing \n",
                                              "[1] - update \n",
                                              "[2] - delete \n",
                                              "[3] - updated in last period of time \n"
                                          ], 3);
        $operation         = $helper->ask($input, $output, $operationQuestion);

        if ($operation == 1 || $operation == 3) {
            /**
             * @var ProductModelRepository $repository
             */
            $repository = $em->getRepository('AppBundle:ProductModel');

            $stepQuestion = new Question('Enter limit of operation : [1000]', 1000);
            $step         = (int)$helper->ask($input, $output, $stepQuestion);

            $offsetQuestion = new Question('Enter offset : [0]', 0);
            $offset         = (int)$helper->ask($input, $output, $offsetQuestion);

            $categoriesData = $this->getCategoriesChain($em);

            $lastModified = false;
            if ($operation == 3) {
                $lastModified = true;
            }

            do {
                $modelEntity = $repository->getModelsDataByLimitOffset(1, $step, $offset, $lastModified);
                $update      = $client->createUpdate();

                /**
                 * @var ProductModel $model
                 */
                foreach ($modelEntity as $model) {
                    $doc     = $update->createDocument();
                    $doc->id = $model->getId();

                    $doc->name           =
                    $doc->name_sorting = $model->getName();
                    $doc->description    = htmlspecialchars($model->getDescription() ?: '');
                    $doc->price_total    = (float)$model->getMinPrice();
                    $doc->price          = (float)($model->getPrice() ?? $model->getMinPrice());
                    $doc->discount_price = (float)$model->getDiscountPrice();
                    $doc->discount       = (int)$model->getDiscount();
                    $doc->producer_name  = $model->getProducer()->getName();
                    $doc->producer_id    = $model->getProducer()->getId();
                    $doc->update_time    = $model->getUpdateTime();
                    if ($model->getCategory()) {
                        $doc->category_name     = $model->getCategory()->getTitle();
                        $doc->category_id       = $model->getCategory()->getId();
                    }
                    $image        = $model->getImage();
                    $chinaProduct = $model->isChinaProduct();

                    $doc->image            = $image;
                    $doc->is_china_product = $chinaProduct;

                    $products = $model->getProduct();

                    $externalId = '';
                    $activeProducts = $model->getActiveProductIds();
                    $reformatAttributesArray = [];
                    $category_chain    = $categoriesData[$model->getCategory()->getId()]['chain'];
                    $category_id_chain = $categoriesData[$model->getCategory()->getId()]['id_chain'];

                    /**
                     * @var Product $product
                     */
                    foreach ($products as $product) {
                        if ($chinaProduct && $image == $product->getImage()) {
                            $externalId = $product->getExternalId();
                        }

                        if (in_array($product->getId(), $activeProducts)) {
                            $additionalProductCategories = $product->getProductCategory();
                            if (!empty($additionalProductCategories)) {
                                foreach ($additionalProductCategories as $additionalCategory) {
                                    $category_chain    = array_merge($category_chain, $categoriesData[$additionalCategory->getCategory()->getId()]['chain']);
                                    $category_id_chain = array_merge($category_id_chain, $categoriesData[$additionalCategory->getCategory()->getId()]['id_chain']);
                                }
                            }

                            $attributes = $product->getAttributes();
                            foreach ($attributes as $attribute) {
                                $reformatAttributesArray[$attribute->getAttributeId()][] = $attribute->getValue();
                            }
                        }
                    }

                    $category_chain    = array_unique($category_chain);
                    $category_id_chain = array_unique($category_id_chain);

                    $doc->category_chain    = $category_chain;
                    $doc->category_id_chain = $category_id_chain;
                    $doc->products          = $activeProducts;
                    $doc->status            = (int)$model->getStatus();
                    $doc->in_stock          = (bool)$model->getInStock();
                    $doc->moderation_status = (int)$model->getModeration();
                    $doc->external_id       = $externalId;
                    $doc->in_draft_category = $model->getCategory()->getStatus() === Category::STATUS_DRAFT;

                    foreach ($reformatAttributesArray as $attributeId => $attributeValues) {
                        $name       = 'attr_' . $attributeId;
                        $doc->$name = array_unique($attributeValues);
                    }

                    $update->addDocument($doc);
                }

                $update->addCommit();
                $result = $client->update($update);

                if ($result->getStatus() === 0) {
                    $offset += $step;
                    $output->writeln('offset: ' . $offset);
                } else {
                    $output->writeln('Import failed!');
                }
            } while (!empty($result) && !empty($modelEntity));
        } elseif ($operation == 2) {
            $range = $this->getRangeIds($input, $output, $helper);

            if (!empty($range)) {
                $update = $client->createUpdate();

                for ($id = $range['from']; $id <= $range['to']; $id++) {
                    $update->addDeleteById($id);
                }

                $update->addCommit();
                $result = $client->update($update);

                if ($result->getStatus() === 0) {
                    $output->writeln('Models from ' . $range['from'] . ' to ' . $range['to'] . ' ids -
                        successfully deleted from solr!');
                } else {
                    $output->writeln('Delete failed!');
                }
            } else {
                $output->writeln('Wrong range!');
            }
        } else {
            $output->writeln('Operation exited!');
        }
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @param QuestionHelper  $helper
     *
     * @return array
     */
    private function getRangeIds(InputInterface $input, OutputInterface $output, QuestionHelper $helper)
    {
        $fromQuestion = new Question('Enter from id' . ': [1] ', 1);
        $fromId       = (int)$helper->ask($input, $output, $fromQuestion);

        $toQuestion = new Question('Enter to id' . ': [100] ', 100);
        $toId       = (int)$helper->ask($input, $output, $toQuestion);

        $confirmation = new ConfirmationQuestion('Is it okay: ' . $fromId . ' .. ' . $toId . ' ? [true] ', true);
        $confirm      = $helper->ask($input, $output, $confirmation);

        if ($confirm && $fromId < $toId) {
            return ['from' => $fromId, 'to' => $toId];
        }
    }

    /**
     * @param EntityManager $em
     *
     * @return array
     */
    private function getCategoriesChain($em)
    {
        /**
         * @var CategoryRepository $categoryRepository
         */
        $categoryRepository = $em->getRepository('AppBundle:Category');

        $categoriesData = $categoryRepository->getCategoriesData();

        $newKeys = array_column($categoriesData, 'id');

        $categoriesData = array_combine($newKeys, $categoriesData);

        foreach ($categoriesData as &$categoryData) {
            $categoryData['id_chain'][] = $categoryData['id'];
            $categoryData['chain'][]    = $categoryData['title'];
            $parentId                   = $categoryData['parentId'];

            while (!empty($parentId)) {
                $categoryData['id_chain'][] = $categoriesData[$parentId]['id'];
                $categoryData['chain'][]    = $categoriesData[$parentId]['title'];
                $parentId                   = $categoriesData[$parentId]['parentId'];
            }
        }

        return $categoriesData;
    }
}
