<?php

namespace AppBundle\Command;

use AppBundle\Parameters\RedisParameter;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\{
    Input\InputInterface, Output\OutputInterface
};
use Symfony\Component\Cache\Simple\RedisCache;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;

/**
 * Class CategoryAddFieldsCommand
 *
 * @package AppBundle\Command
 */
class CategoryAddFieldsCommand extends ContainerAwareCommand
{

    /**
     *
     */
    protected function configure(): void
    {
        $this
            ->setName('category:update-add-fields')
            ->setDescription('Updating additional fields (producers, attributes) and saving in Redis')
            ->setHelp('Updating additional fields (producers, attributes) and saving in Redis');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @throws ServiceCircularReferenceException
     * @throws \LogicException
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $output->write([
            'Updating..',
            'Getting categories information..'
                       ]);

        $categoryManager = $this->getContainer()->get('AppBundle\Manager\CategoryManager');
        $redis = $this->getContainer()->get('snc_redis.cache');
        $cache = new RedisCache($redis);

        $data = $categoryManager->getCategoriesIdsWithAddFields();

        $cache->set(RedisParameter::CACHE_CATEGORIES_WITH_ADD_NAME, $data, RedisParameter::CACHE_CATEGORIES_WITH_ADD_TIME_SECONDS);

        $output->writeln('Done!');
    }

}