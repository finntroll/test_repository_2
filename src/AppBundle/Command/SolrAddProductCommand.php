<?php

namespace AppBundle\Command;

use AppBundle\Entity\{
    Category, Product, ProductAttributeValue, Rate
};
use AppBundle\Manager\ProductManager;
use AppBundle\Repository\{
    CategoryRepository, ProductRepository
};
use Doctrine\ORM\EntityManager;
use Solarium\Core\Client\Client;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\{
    Helper\QuestionHelper, Input\InputInterface, Output\OutputInterface, Question\ConfirmationQuestion, Question\Question
};

/**
 * Class SolrAddProductCommand
 *
 * @package AppBundle\Command
 */
class SolrAddProductCommand extends ContainerAwareCommand
{
    /**
     *
     */
    protected function configure()
    {
        $this
            ->setName('solr:add:product')
            ->setDescription('Helps to add products in range [:from .. :to] to solr')
            ->setHelp('Helps to add products in range [:from .. :to] to solr');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return int|null|void
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var Client $client
         */
        $client = $this->getContainer()->get('solarium.client');

        $em = $this->getContainer()->get('doctrine.orm.default_entity_manager');

        $productManager = $this->getContainer()->get(ProductManager::class);

        $helper = $this->getHelper('question');

        $operationQuestion = new Question([
                                              "What do you want to do? \n",
                                              "Press: \n",
                                              "[0] - nothing \n",
                                              "[1] - update \n",
                                              "[2] - delete \n",
                                              "[3] - updated in last period of time \n"
                                          ], 3);
        $operation         = $helper->ask($input, $output, $operationQuestion);

        if ($operation == 1 || $operation == 3) {
            /**
             * @var ProductRepository $repository
             */
            $repository = $em->getRepository('AppBundle:Product');

            $stepQuestion = new Question('Enter limit of 1 operation' . ': [10000] ', 10000);
            $step         = (int)$helper->ask($input, $output, $stepQuestion);

            $offsetQuestion = new Question('Enter offset' . ': [0] ', 0);
            $offset         = (int)$helper->ask($input, $output, $offsetQuestion);

            $converter = $this->getContainer()->get('AppBundle\Service\CurrencyConverter');
            $rate      = $converter->getRate(Rate::RUB);

            $categoriesData = $this->getCategoriesChain($em);

            $lastModified = false;
            if ($operation == 3) {
                $lastModified = true;
            }

            do {
                $productsEntity = $repository->getProductsDataByIdLimitOffset(1, $step, $offset, $lastModified);

                $update = $client->createUpdate();
                /**
                 * @var Product $product
                 */
                foreach ($productsEntity as $product) {
                    $doc     = $update->createDocument();
                    $doc->id = $product->getId();

                    $productCharge = $productManager->getExtraChargeByProduct($product);
                    $discountPrice = $product->getDiscountPrice() ? $product->getDiscountPrice() * (1 + $productCharge / 100) : 0;
                    $basePrice     = $product->getPrice() * (1 + $productCharge / 100);
                    $totalPrice    = $product->getDiscountPrice() ? $discountPrice : $basePrice * (1 - ($product->getDiscount() ?: 0) / 100);

                    $doc->price          = round($basePrice * $rate, -1) ?: 10;
                    $doc->price_total    = round($totalPrice * $rate, -1) ?: 10;
                    $doc->discount_price = round($discountPrice * $rate, -1) ?: 0;

                    $doc->image             = $product->getImage();
                    $doc->name              = $product->getName();
                    $doc->description       = htmlspecialchars($product->getDescription() ?? '');
                    $doc->full_name         =
                    $doc->full_name_sorting = $product->getFullName();
                    $doc->in_stock          = $product->isInStock();
                    $doc->is_china_product  = $product->isChinaProduct();
                    $doc->producer_name     = $product->getProducerName();
                    $doc->producer_id       = $product->getProducerId();
                    $doc->category_id       = $product->getCategory()->getId();
                    $doc->category_name     = $product->getCategory()->getTitle();
                    $doc->status            = $product->getStatus();
                    $doc->promotion_status  = $product->getIsDisabled();
                    $doc->moderation_status = $product->getModeration();
                    $doc->external_id       = $product->getExternalId();
                    $doc->update_time       = $product->getUpdateTime();
                    $doc->in_draft_category = $product->getCategory()->getStatus() === Category::STATUS_DRAFT;

                    $category_chain    = $categoriesData[$product->getCategory()->getId()]['chain'];
                    $category_id_chain = $categoriesData[$product->getCategory()->getId()]['id_chain'];

                    $additionalProductCategories = $product->getProductCategory();
                    if (!empty($additionalProductCategories)) {
                        foreach ($additionalProductCategories as $additionalCategory) {
                            $category_chain    = array_merge($category_chain, $categoriesData[$additionalCategory->getCategory()->getId()]['chain']);
                            $category_id_chain = array_merge($category_id_chain, $categoriesData[$additionalCategory->getCategory()->getId()]['id_chain']);
                        }
                        $category_chain    = array_unique($category_chain);
                        $category_id_chain = array_unique($category_id_chain);
                    }

                    $doc->category_chain    = $category_chain;
                    $doc->category_id_chain = $category_id_chain;

                    $reformatAttributesArray = array();
                    /**
                     * @var ProductAttributeValue $attribute
                     */
                    $attributes = $product->getAttributes();
                    foreach ($attributes as $attribute) {
                        $reformatAttributesArray[$attribute->getAttributeId()][] = $attribute->getValue();
                    }

                    foreach ($reformatAttributesArray as $attributeId => $attributeValues) {
                        $name       = 'attr_' . $attributeId;
                        $doc->$name = $attributeValues;
                    }

                    $update->addDocument($doc);
                }

                $update->addCommit();
                $result = $client->update($update);

                if ($result->getStatus() === 0) {
                    $offset += $step;
                    $output->writeln('offset: ' . $offset);
                } else {
                    $output->writeln('Import failed!');
                }
            } while (!empty($result) && !empty($productsEntity));
        } elseif ($operation == 2) {
            $range = $this->getRangeIds($input, $output, $helper);

            if (!empty($range)) {
                $update = $client->createUpdate();

                for ($id = $range['from']; $id <= $range['to']; $id++) {
                    $update->addDeleteById($id);
                }

                $update->addCommit();
                $result = $client->update($update);

                if ($result->getStatus() === 0) {
                    $output->writeln('Products from ' . $range['from'] . ' to ' . $range['to'] . ' ids -
                        successfully deleted from solr!');
                } else {
                    $output->writeln('Delete failed!');
                }
            } else {
                $output->writeln('Wrong range!');
            }
        }
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     * @param QuestionHelper  $helper
     *
     * @return array
     */
    private function getRangeIds(InputInterface $input, OutputInterface $output, QuestionHelper $helper)
    {
        $fromQuestion = new Question('Enter from id' . ': [1] ', 1);
        $fromId       = (int)$helper->ask($input, $output, $fromQuestion);

        $toQuestion = new Question('Enter to id' . ': [100] ', 100);
        $toId       = (int)$helper->ask($input, $output, $toQuestion);

        $confirmation = new ConfirmationQuestion('Is it okay: ' . $fromId . ' .. ' . $toId . ' ? [true] ', true);
        $confirm      = $helper->ask($input, $output, $confirmation);

        if ($confirm && $fromId < $toId) {
            return ['from' => $fromId, 'to' => $toId];
        }
    }

    /**
     * @param EntityManager $em
     *
     * @return array
     */
    private function getCategoriesChain($em)
    {
        /**
         * @var CategoryRepository $categoryRepository
         */
        $categoryRepository = $em->getRepository('AppBundle:Category');

        $categoriesData = $categoryRepository->getCategoriesData();

        $newKeys = array_column($categoriesData, 'id');

        $categoriesData = array_combine($newKeys, $categoriesData);

        foreach ($categoriesData as &$categoryData) {
            $categoryData['id_chain'][] = $categoryData['id'];
            $categoryData['chain'][]    = $categoryData['title'];
            $parentId                   = $categoryData['parentId'];

            while (!empty($parentId)) {
                $categoryData['id_chain'][] = $categoriesData[$parentId]['id'];
                $categoryData['chain'][]    = $categoriesData[$parentId]['title'];
                $parentId                   = $categoriesData[$parentId]['parentId'];
            }
        }

        return $categoriesData;
    }
}
