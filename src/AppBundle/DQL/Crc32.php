<?php

namespace AppBundle\DQL;

use Doctrine\ORM\Query\{
    AST\Functions\FunctionNode, Lexer, Parser, QueryException, SqlWalker
};

/**
 * Class Crc32
 * @package AppBundle\DQL
 */
class Crc32 extends FunctionNode
{
    /**
     * @var string
     */
    public $stringPrimary;

    /**
     * @param SqlWalker $sqlWalker
     *
     * @return string
     */
    public function getSql(SqlWalker $sqlWalker): string
    {
        return 'CRC32(' .
            $sqlWalker->walkStringPrimary($this->stringPrimary) .
            ')';
    }

    /**
     * @param Parser $parser
     *
     * @throws QueryException
     */
    public function parse(Parser $parser): void
    {
        $parser->match(Lexer::T_IDENTIFIER);
        $parser->match(Lexer::T_OPEN_PARENTHESIS);
        $this->stringPrimary = $parser->StringPrimary();
        $parser->match(Lexer::T_CLOSE_PARENTHESIS);
    }
}