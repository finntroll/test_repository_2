<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Category
 *
 * @ORM\Table(name="um_store_category")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CategoryRepository")
 * @Serializer\AccessorOrder("custom", custom={"id", "parent_id", "title", "image", "sort"})
 */
class Category implements EntityInterface
{
    /**
     *
     */
    public const STATUS_DRAFT = 0;
    /**
     *
     */
    public const STATUS_PUBLISHED = 1;

    /**
     * @var int
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"catalog"})
     */
    private $id;

    /**
     * @var int|null
     *
     * @ORM\Column(name="parent_id", type="integer", nullable=true)
     *
     * @Serializer\Type("integer")
     */
    private $parentId;

    /**
     * @var self|null
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\Category", fetch="EAGER")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id")
     * @Serializer\Exclude()
     */
    private $parent;

    /**
     * @var string
     *
     * @ORM\Column(name="name",length=250)
     * @Assert\NotBlank()
     * @Assert\Length(max="250")
     * @Serializer\Groups({"catalog"})
     */
    private $title;

    /**
     * @var int
     * @ORM\Column(type="smallint")
     * @Serializer\Exclude()
     */
    private $status = self::STATUS_PUBLISHED;

    /**
     * @var string
     *
     * @ORM\Column(length=250, nullable=true)
     * @Assert\Length(max="250")
     * @Serializer\Exclude()
     */
    private $image;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @Assert\Type("integer")
     */
    private $sort;

    /**
     * @var boolean
     * @ORM\Column(name="is_group", type="boolean", nullable=true)
     * @Serializer\Exclude()
     */
    private $isGroup = false;

    /**
     * @var bool
     * @ORM\Column(name="show_products", type="boolean", nullable=true)
     * @Serializer\Exclude()
     */
    private $showProducts = true;

    /**
     * @var PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Product", mappedBy="category")
     *
     * @Serializer\Exclude()
     */
    private $products;

    /**
     * @var ProductModel|null
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductModel", mappedBy="category")
     */
    private $productModel;

    /**
     * @var DateTime|null
     * @ORM\Column(name="create_time", type="datetime", nullable=true)
     * @Serializer\Exclude()
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createTime;

    /**
     * @var DateTime|null
     * @ORM\Column(name="update_time", type="datetime", nullable=true)
     * @Serializer\Exclude()
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updateTime;

    /**
     * @var ProductCategory
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductCategory", mappedBy="category")
     *
     * @Serializer\Exclude()
     */
    private $productCategory;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_compilation", type="boolean")
     */
    private $isCompilation;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int|null $parentId
     *
     * @return self
     */
    public function setParentId(?int $parentId): self
    {
        $this->parentId = $parentId;
        return $this;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param int $status
     * @return self
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param null|string $image
     *
     * @return self
     */
    public function setImage(?string $image): self
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param integer $sort
     *
     * @return self
     */
    public function setSort($sort): self
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @return bool
     */
    public function isGroup(): bool
    {
        return $this->isGroup;
    }

    /**
     * @param bool $isGroup
     * @return self
     */
    public function setIsGroup(bool $isGroup): self
    {
        $this->isGroup = $isGroup;
        return $this;
    }

    /**
     * @return bool
     */
    public function isShowProducts(): bool
    {
        return $this->showProducts;
    }

    /**
     * @param bool $showProducts
     * @return self
     */
    public function setShowProducts(bool $showProducts): self
    {
        $this->showProducts = $showProducts;
        return $this;
    }

    /**
     * @return Category|null
     */
    public function getParent(): ?Category
    {
        return $this->parent;
    }

    /**
     * @param Category|null $parent
     * @return self
     */
    public function setParent(?Category $parent): self
    {
        $this->parent = $parent;
        $this->setParentId($parent === null ? null : $parent->getId());
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getCreateTime(): ?DateTime
    {
        return $this->createTime;
    }

    /**
     * @param DateTime|null $createTime
     * @return self
     */
    public function setCreateTime(?DateTime $createTime): self
    {
        $this->createTime = $createTime;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdateTime(): ?DateTime
    {
        return $this->updateTime;
    }

    /**
     * @param DateTime|null $updateTime
     * @return self
     */
    public function setUpdateTime(?DateTime $updateTime): self
    {
        $this->updateTime = $updateTime;
        return $this;
    }

    /**
     * @return PersistentCollection
     */
    public function getProducts(): PersistentCollection
    {
        return $this->products;
    }

    /**
     * @return ProductModel|null
     */
    public function getProductModel(): ?ProductModel
    {
        return $this->productModel;
    }

    /**
     * @param ProductModel|null $productModel
     */
    public function setProductModel(?ProductModel $productModel): void
    {
        $this->productModel = $productModel;
    }
}

