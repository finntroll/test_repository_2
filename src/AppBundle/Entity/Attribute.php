<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Attribute
 *
 * @ORM\Table(name="um_attribute")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttributeRepository")
 */
class Attribute implements EntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var AttributeGroup|null
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\AttributeGroup", inversedBy="attributes")
     * @Serializer\Exclude()
     */
    private $group;

    /**
     * @var int
     *
     * @ORM\Column(name="group_id", type="integer", nullable=true)
     */
    private $groupId;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\AttributeOption", mappedBy="attribute")
     * @Serializer\Exclude()
     */
    private $options;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     * @Serializer\Exclude()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="title", type="string", length=255)
     */
    private $title;

    /**
     * @var int|null
     *
     * @ORM\Column(name="type", type="smallint", nullable=true)
     */
    private $type;

    /**
     * @var string|null
     *
     * @ORM\Column(name="unit", type="string", length=30, nullable=true)
     */
    private $unit;

    /**
     * @var string|null
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Serializer\Exclude()
     */
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="sort", type="integer")
     */
    private $sort = 0;


    /**
     * Attribute constructor.
     */
     public function __construct()
     {
         $this->options = new ArrayCollection();
     }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $title
     *
     * @return self
     */
    public function setTitle(string $title): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string
     */
    public function getTitle(): string
    {
        return $this->title;
    }

    /**
     * @param int|null $type
     *
     * @return self
     */
    public function setType(?int $type = null): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getType(): ?int
    {
        return $this->type;
    }

    /**
     * @param string|null $unit
     *
     * @return self
     */
    public function setUnit(?string $unit = null): self
    {
        $this->unit = $unit;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getUnit(): ?string
    {
        return $this->unit;
    }

    /**
     * @param string|null $description
     *
     * @return self
     */
    public function setDescription(?string $description = null): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param int $sort
     *
     * @return self
     */
    public function setSort(int $sort): self
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @return AttributeGroup|null
     */
    public function getGroup(): ?AttributeGroup
    {
        return $this->group;
    }

    /**
     * @param AttributeGroup|null $group
     *
     * @return self
     */
    public function setGroup(?AttributeGroup $group): self
    {
        $this->group = $group;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getOptions(): Collection
    {
        return $this->options;
    }

    /**
     * @param AttributeOption $option
     *
     * @return Attribute
     */
    public function addOption(AttributeOption $option): self
    {
        $this->options[] = $option;
        return $this;
    }

    /**
     * @param AttributeOption $option
     */
    public function removeOption(AttributeOption $option): void
    {
        $this->options->removeElement($option);
    }

}
