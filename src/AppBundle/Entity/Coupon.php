<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\{
    ArrayCollection, Collection
};
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Coupon
 *
 * @ORM\Table(name="um_coupon")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Coupon")
 */
class Coupon implements EntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var UserOrder|null
     *
     * @ORM\OneToOne(targetEntity="AppBundle\Entity\UserOrder")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @var Promocode
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Promocode", inversedBy="coupons")
     * @ORM\JoinColumn(name="promocode_id", referencedColumnName="id")
     */
    private $promocode;

    /**
     * @var User
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="coupons")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    private $user;

    /**
     * @var integer
     * @ORM\Column(name="discount", type="integer", length=11)
     * @Assert\Type("integer")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $discount = 0;

    /**
     * @var DateTime
     * @ORM\Column(name="date_use_to", type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $dateUseTo;

    /**
     * @var integer
     * @ORM\Column(name="is_used", type="integer", length=1)
     * @Assert\Type("integer")
     */
    private $isUsed = 0;

    /**
     * @var string|null
     *
     * @ORM\Column(name="url_hash", length=32, nullable=true)
     * @Assert\NotBlank()
     * @Assert\Length(max="32")
     */
    private $urlHash;

    /**
     * @var integer|null
     * @ORM\Column(name="payment_type_id", type="integer", length=11, nullable=true)
     * @Assert\Type("integer")
     */
    private $paymentTypeId;

    /**
     * @var float|null
     * @ORM\Column(name="minimal_order_sum", type="decimal", length=19, scale=3, nullable=true)
     * @Assert\Type("float")
     */
    private $minimalOrderSum;

    /**
     * @var integer
     * @ORM\Column(name="percent", type="integer", length=4)
     * @Assert\Type("integer")
     */
    private $percent = 0;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return UserOrder|null
     */
    public function getOrder(): ?UserOrder
    {
        return $this->order;
    }

    /**
     * @param UserOrder|null $order
     *
     * @return self
     */
    public function setOrder(?UserOrder $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return Promocode
     */
    public function getPromocode(): Promocode
    {
        return $this->promocode;
    }

    /**
     * @param Promocode $promocode
     *
     * @return self
     */
    public function setPromocode(Promocode $promocode): self
    {
        $this->promocode = $promocode;

        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return self
     */
    public function setUser(User $user): self
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return int
     */
    public function getDiscount(): int
    {
        return $this->discount;
    }

    /**
     * @param int $discount
     *
     * @return self
     */
    public function setDiscount(int $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateUseTo(): DateTime
    {
        return $this->dateUseTo;
    }

    /**
     * @param DateTime $dateUseTo
     *
     * @return self
     */
    public function setDateUseTo(DateTime $dateUseTo): self
    {
        $this->dateUseTo = $dateUseTo;

        return $this;
    }

    /**
     * @return int
     */
    public function getisUsed(): int
    {
        return $this->isUsed;
    }

    /**
     * @param int $isUsed
     *
     * @return self
     */
    public function setIsUsed(int $isUsed): self
    {
        $this->isUsed = $isUsed;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getUrlHash(): ?string
    {
        return $this->urlHash;
    }

    /**
     * @param null|string $urlHash
     *
     * @return self
     */
    public function setUrlHash(?string $urlHash): self
    {
        $this->urlHash = $urlHash;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPaymentTypeId(): ?int
    {
        return $this->paymentTypeId;
    }

    /**
     * @param int|null $paymentTypeId
     *
     * @return self
     */
    public function setPaymentTypeId(?int $paymentTypeId): self
    {
        $this->paymentTypeId = $paymentTypeId;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getMinimalOrderSum(): ?float
    {
        return $this->minimalOrderSum;
    }

    /**
     * @param float|null $minimalOrderSum
     *
     * @return self
     */
    public function setMinimalOrderSum(?float $minimalOrderSum): self
    {
        $this->minimalOrderSum = $minimalOrderSum;

        return $this;
    }

    /**
     * @return int
     */
    public function getPercent(): int
    {
        return $this->percent;
    }

    /**
     * @param int $percent
     *
     * @return self
     */
    public function setPercent(int $percent): self
    {
        $this->percent = $percent;

        return $this;
    }
}
