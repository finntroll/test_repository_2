<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * City
 *
 * @ORM\Table(name="um_location_city")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\CityRepository")
 */
class City implements EntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="area_ru", type="string", length=150, nullable=true)
     */
    private $area;

    /**
     * @var int
     *
     * @ORM\Column(name="country_id", type="integer")
     */
    private $countryId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="region_id", type="integer", nullable=true)
     */
    private $regionId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="title_ru", type="string", length=150, nullable=true)
     */
    private $title;

    /**
     * @var bool
     *
     * @ORM\Column(name="important", type="boolean", nullable=true)
     *
     * @Serializer\Exclude()
     */
    private $important = false;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string|null $area
     *
     * @return self
     */
    public function setArea(?string $area = null): self
    {
        $this->area = $area;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getArea(): ?string
    {
        return $this->area;
    }

    /**
     * @param int $countryId
     *
     * @return self
     */
    public function setCountryId(int $countryId): self
    {
        $this->countryId = $countryId;
        return $this;
    }

    /**
     * @return int
     */
    public function getCountryId(): int
    {
        return $this->countryId;
    }

    /**
     * @param int|null $regionId
     *
     * @return self
     */
    public function setRegionId(?int $regionId = null): self
    {
        $this->regionId = $regionId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getRegionId(): ?int
    {
        return $this->regionId;
    }

    /**
     * @param string|null $title
     *
     * @return self
     */
    public function setTitle(?string $title = null): self
    {
        $this->title = $title;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTitle(): ?string
    {
        return $this->title;
    }

    /**
     * @return bool
     */
    public function getImportant(): bool
    {
        return $this->important;
    }

    /**
     * @param bool $important
     *
     * @return self
     */
    public function setImportant(bool $important): self
    {
        $this->important = $important;
        return $this;
    }

}
