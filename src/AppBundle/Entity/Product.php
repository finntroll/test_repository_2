<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\{
    ArrayCollection, Collection
};
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Product
 *
 * @Serializer\AccessorOrder(
 *     "custom",
 *     custom={"id", "name", "price", "discount_price", "image"}
 *     )
 *
 * @ORM\Table(name="um_store_product", indexes={@ORM\Index(columns={"full_name"}, flags={"fulltext"})})
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 * @ORM\HasLifecycleCallbacks()
 * @UniqueEntity("sku")
 */
class Product implements EntityInterface
{
    /**
     *
     */
    public const STATUS_NOT_ACTIVE = 0;

    /**
     *
     */
    public const STATUS_ACTIVE = 1;

    /**
     *
     */
    public const STATUS_ARCHIVE = 2;

    /**
     *
     */
    public const STATUS_WITHDRAWN_FROM_SALE = 3;

    /**
     * Disabled product on promotion
     */
    public const DISABLE_ON_PROMOTION = 1;

    /**
     * Enabled product when promotion end
     */
    public const NOT_DISABLED_ON_PROMOTION = 0;

    /**
     *
     */
    public const MODERATION_CONFIRMED = 1;

    /**
     *
     */
    public const MODERATION_REJECTED = 2;

    /**
     * @inheritdoc
     */
    public const PRODUCT_IN_STOCK = 1;

    /**
     * @inheritdoc
     */
    public const PRODUCT_NOT_IN_STOCK = 0;

    /**
     * @inheritdoc
     */
    public const MINIMAL_QUANTITY_IN_STOCK = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"product_preview"})
     */
    private $id;

    /**
     * @var int
     * @ORM\Column(name="type_id", type="integer", nullable=true)
     * @Assert\Type("integer")
     * @Assert\NotBlank()
     */
    private $typeId;

    /**
     * @var int
     * @ORM\Column(name="category_id", type="integer", nullable=true)
     * @Serializer\Type("integer")
     */
    private $categoryId;

    /**
     * @var Category|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category", inversedBy="products")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Exclude()
     */
    private $category;

    /**
     * @var ArrayCollection
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductAttributeValue", mappedBy="product")
     * @Serializer\Type("array")
     * @Serializer\Exclude()
     */
    private $attributes;

    /**
     * @var string
     * @ORM\Column(length=100, nullable=true)
     * @Assert\Length(max="100")
     */
    private $sku;

    /**
     * @var string
     *
     * @ORM\Column(length=512)
     * @Serializer\Exclude(if="object.getFullName() === null")
     * @Assert\NotBlank()
     * @Assert\Length(max="512")
     * @Serializer\Groups({"product_preview"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="full_name", length=1024, nullable=true)
     * @Serializer\SerializedName("name")
     * @Serializer\Groups({"product_preview"})
     * @Assert\Length(max="1024")
     */
    private $fullName;

    /**
     * @var float
     * @ORM\Column(type="decimal", length=19, scale=3)
     * @Assert\Type("float")
     * @Serializer\Exclude()
     */
    private $price = 0.0;

    /**
     * @var float
     * @ORM\Column(name="discount_price", type="decimal", nullable=true, length=19, scale=3)
     * @Assert\Type("float")
     * @Serializer\Exclude()
     */
    private $discountPrice;

    /**
     * @var float
     * @ORM\Column(type="decimal", nullable=true, length=19, scale=3)
     * @Assert\Type("float")
     */
    private $discount;

    /**
     * @var float|null
     * @ORM\Column(type="decimal", nullable=true, length=10, scale=2)
     */
    private $extraCharge;

    /**
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    private $description;

    /**
     * @var Producer|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Producer")
     * @ORM\JoinColumn(nullable=true)
     * @Serializer\Exclude()
     */
    private $producer;

    /**
     * @var ProductModel|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProductModel", inversedBy="product")
     * @ORM\JoinColumn(name="product_model_id", referencedColumnName="id")
     * @Serializer\Exclude()
     */
    private $productModel;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\OrderProduct", mappedBy="product")
     * @Serializer\Exclude()
     */
    private $orderProducts;

    /**
     * @var int|null
     * @ORM\Column(name="producer_id", type="integer", nullable=true)
     */
    private $producerId;

    /**
     * @var float
     * @ORM\Column(type="decimal", nullable=true, length=19, scale=3)
     * @Assert\Type("float")
     */
    private $width;

    /**
     * @var float
     * @ORM\Column(type="decimal", nullable=true, length=19, scale=3)
     * @Assert\Type("float")
     */
    private $height;

    /**
     * @var float
     * @ORM\Column(type="decimal", nullable=true, length=19, scale=3)
     * @Assert\Type("float")
     */
    private $length;

    /**
     * @var float
     * @ORM\Column(type="decimal", nullable=true, length=19, scale=3)
     * @Assert\Type("float")
     */
    private $weight;

    /**
     * @var string|null
     * @ORM\Column(nullable=true, length=250)
     * @Assert\Length(max="250")
     * @Serializer\Exclude()
     */
    private $image;

    /**
     * @var null|ProductImage
     * @Serializer\Exclude()
     */
    private $mainImage;

    /**
     * @var array
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductImage", mappedBy="product")
     * @Serializer\Exclude()
     */
    private $images;

    /**
     * @var bool
     * @ORM\Column(name="is_china_product", type="boolean")
     * @Serializer\Groups({"product_preview"})
     */
    private $isChinaProduct;

    /**
     * @var integer
     * @ORM\Column(name="status", type="integer", length=4)
     * @Assert\Type("integer")
     * @Assert\GreaterThanOrEqual(0)
     * @Assert\LessThanOrEqual(3)
     * @Serializer\Exclude()
     */
    private $status = 1;

    /**
     * @var bool
     * @ORM\Column(name="in_stock", type="boolean")
     * @Serializer\Groups({"product_preview"})
     */
    private $inStock;

    /**
     * @var integer
     * @ORM\Column(name="quantity", type="integer", nullable=true)
     */
    private $quantity;

    /**
     * @var integer
     * @ORM\Column(name="is_disabled", type="integer", length=1, nullable=true)
     * @Assert\Type("integer")
     * @Serializer\Exclude()
     */
    private $isDisabled = 0;

    /**
     * @var int
     * @ORM\Column(name="moderation", type="integer", length=4)
     * @Assert\Type("integer")
     * @Serializer\Exclude()
     */
    private $moderation = 0;


    /**
     * @var string
     * @ORM\Column(name="external_id", length=100, nullable=true)
     * @Assert\Length(max="100")
     */
    private $externalId;

    /**
     * @var string
     *
     * @ORM\Column(name="model_name", length=255, nullable=false)
     * @Assert\Length(max="255")
     */
    private $modelName;

    /**
     * @var ProductReview[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductReview", mappedBy="product")
     */
    private $reviews;

    /**
     * @var ProductCategory[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductCategory", mappedBy="product")
     *
     * @Serializer\Exclude()
     */
    private $productCategory;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="create_time", type="datetime")
     *
     * @Serializer\Exclude()
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createTime;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="update_time", type="datetime")
     *
     * @Serializer\Exclude()
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updateTime;

    /**
     * @var float
     * @ORM\Column(type="decimal", nullable=false, length=7, scale=3)
     * @Assert\Type("float")
     * @Serializer\Groups({"product_preview"})
     */
    private $rating;

    /**
     * Product constructor.
     */
    public function __construct()
    {
        $this->attributes = new ArrayCollection();
        $this->reviews    = new ArrayCollection();
    }

    /**
     * @ORM\PostLoad
     */
    public function makeMainImage()
    {
        if ($this->image) {
            $this->mainImage = new ProductImage();
            $this->mainImage
                ->setProduct($this)
                ->setName($this->image);
        }
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $fullName
     *
     * @return Product
     */
    public function setFullName(?string $fullName): self
    {
        $this->fullName = $fullName;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getFullName(): ?string
    {
        return $this->fullName;
    }

    /**
     * @return int
     */
    public function getTypeId(): int
    {
        return $this->typeId;
    }

    /**
     * @param int $typeId
     * @return self
     */
    public function setTypeId(int $typeId): self
    {
        $this->typeId = $typeId;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category $category
     * @return self
     */
    public function setCategory(Category $category): self
    {
        $this->category = $category;
        $this->setCategoryId($category->getId());
        return $this;
    }

    /**
     * @return int
     */
    public function getCategoryId(): int
    {

        return $this->categoryId;
    }

    /**
     * @param int $categoryId
     * @return self
     */
    public function setCategoryId(int $categoryId): self
    {
        $this->categoryId = $categoryId;
        return $this;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     * @return self
     */
    public function setSku(string $sku): self
    {
        $this->sku = $sku;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     * @return self
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;
        return $this;
    }

    /**
     * @return null|float
     */
    public function getDiscountPrice(): ?float
    {
        return $this->discountPrice ?? 0;
    }

    /**
     * @param null|float $discountPrice
     * @return self
     */
    public function setDiscountPrice(?float $discountPrice): self
    {
        $this->discountPrice = $discountPrice;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiscount(): float
    {
        return $this->discount ?? 0;
    }

    /**
     * @param float $discount
     * @return self
     */
    public function setDiscount(float $discount): self
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return string
     */
    public function getDescription(): string
    {
        return $this->description;
    }

    /**
     * @param string $description
     * @return self
     */
    public function setDescription(string $description): self
    {
        $this->description = $description;
        return $this;
    }

    /**
     * @return null|float
     */
    public function getWidth(): ?float
    {
        return $this->width;
    }

    /**
     * @param null|float $width
     * @return self
     */
    public function setWidth(?float $width): self
    {
        $this->width = $width;
        return $this;
    }

    /**
     * @return null|float
     */
    public function getHeight(): ?float
    {
        return $this->height;
    }

    /**
     * @param null|float $height
     * @return self
     */
    public function setHeight(?float $height): self
    {
        $this->height = $height;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getLength(): ?float
    {
        return $this->length;
    }

    /**
     * @param float|null $length
     * @return Product
     */
    public function setLength(?float $length): self
    {
        $this->length = $length;
        return $this;
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     * @return self
     */
    public function setWeight(float $weight): self
    {
        $this->weight = $weight;
        return $this;
    }

    /**
     * @return mixed
     */
    public function getProducer(): ?Producer
    {
        return $this->producer;
    }

    /**
     * @param Producer $producer
     * @return self
     */
    public function setProducer(Producer $producer): self
    {
        $this->producer = $producer;
        $this->setProducerId($producer->getId());
        return $this;
    }

    /**
     * @return null|string
     */
    public function getProducerName(): ?string
    {
        return $this->producer ? $this->producer->getName() : null;
    }

    /**
     * @return Collection|ProductImage[]
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @return mixed
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param mixed $image
     * @return self
     */
    public function setImage($image): self
    {
        $this->image = $image;
        return $this;
    }

    /**
     * @return bool
     */
    public function isChinaProduct(): bool
    {
        return $this->isChinaProduct;
    }

    /**
     * @param bool $isChinaProduct
     * @return self
     */
    public function setIsChinaProduct(bool $isChinaProduct): self
    {
        $this->isChinaProduct = $isChinaProduct;
        return $this;
    }

    /**
     * @param ProductImage[] $images
     * @return self
     */
    public function setImages(array $images): self
    {
        $this->images = $images;
        return $this;
    }

    /**
     * @return ProductImage
     */
    public function getMainImage(): ?ProductImage
    {
        return $this->mainImage;
    }

    /**
     * @param ProductImage $mainImage
     * @return self
     */
    public function setMainImage(ProductImage $mainImage): self
    {
        $this->mainImage = $mainImage;
        return $this;
    }

    /**
     * @return null|string
     */
    public function getExternalId(): ?string
    {
        return $this->externalId;
    }

    /**
     * @param null|string $externalId
     * @return self
     */
    public function setExternalId(?string $externalId): self
    {
        $this->externalId = $externalId;
        return $this;
    }

    /**
     * @return bool
     */
    public function isInStock(): bool
    {
        return $this->inStock;
    }

    /**
     * @param bool $inStock
     * @return self
     */
    public function setInStock(bool $inStock): self
    {
        $this->inStock = $inStock;
        return $this;
    }

    /**
     * @return float|null
     */
    public function getExtraCharge(): ?float
    {
        return $this->extraCharge;
    }

    /**
     * @param float|null $extraCharge
     * @return self
     */
    public function setExtraCharge(?float $extraCharge): self
    {
        $this->extraCharge = $extraCharge;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getProducerId(): ?int
    {
        return $this->producerId;
    }

    /**
     * @param int|null $producerId
     * @return self
     */
    public function setProducerId(?int $producerId): self
    {
        $this->producerId = $producerId;
        return $this;
    }

    /**
     * Add attribute
     *
     * @param ProductAttributeValue $attribute
     *
     * @return self
     */
    public function addAttribute(ProductAttributeValue $attribute): self
    {
        $this->attributes[] = $attribute;
        return $this;
    }

    /**
     * Remove attribute
     *
     * @param ProductAttributeValue $attribute
     */
    public function removeAttribute(ProductAttributeValue $attribute): void
    {
        $this->attributes->removeElement($attribute);
    }

    /**
     * Get attributes
     *
     * @return Collection
     */
    public function getAttributes(): Collection
    {
        return $this->attributes;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return int
     */
    public function getIsDisabled(): int
    {
        return $this->isDisabled;
    }

    /**
     * @param int $isDisabled
     *
     * @return self
     */
    public function setIsDisabled(int $isDisabled): self
    {
        $this->isDisabled = $isDisabled;

        return $this;
    }

    /**
     * @return int
     */
    public function getModeration(): int
    {
        return $this->moderation;
    }

    /**
     * @param int $moderation
     *
     * @return self
     */
    public function setModeration(int $moderation): self
    {
        $this->moderation = $moderation;

        return $this;
    }

    /**
     * @return ProductModel|null
     */
    public function getProductModel(): ?ProductModel
    {
        return $this->productModel;
    }

    /**
     * @param ProductModel|null $productModel
     *
     * @return self
     */
    public function setProductModel(?ProductModel $productModel): self
    {
        $this->productModel = $productModel;

        return $this;
    }

    /**
     * @return string
     */
    public function getModelName(): string
    {
        return $this->modelName;
    }

    /**
     * @param string $modelName
     *
     * @return self
     */
    public function setModelName(string $modelName): self
    {
        $this->modelName = $modelName;

        return $this;
    }

    /**
     * @return ProductReview[]
     */
    public function getReviews(): array
    {
        return $this->reviews->toArray();
    }

    /**
     * @param ProductReview $review
     * @return self
     */
    public function addReview(ProductReview $review): self
    {
        $this->reviews->add($review);
        return $this;
    }

    /**
     * @param ProductReview $review
     * @return self
     */
    public function removeReview(ProductReview $review): self
    {
        $this->reviews->removeElement($review);
        return $this;
    }

    /**
     * @param ProductReview[] $reviews
     * @return $this
     */
    public function setReviews(array $reviews)
    {
        $this->reviews = $reviews;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getQuantity(): ?int
    {
        return $this->quantity;
    }

    /**
     * @param int|null $quantity
     *
     * @return self
     */
    public function setQuantity(?int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreateTime(): DateTime
    {
        return $this->createTime;
    }

    /**
     * @param DateTime $createTime
     *
     * @return self
     */
    public function setCreateTime(DateTime $createTime): self
    {
        $this->createTime = $createTime;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdateTime(): DateTime
    {
        return $this->updateTime;
    }

    /**
     * @param DateTime $updateTime
     *
     * @return self
     */
    public function setUpdateTime(DateTime $updateTime): self
    {
        $this->updateTime = $updateTime;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getOrderProducts(): Collection
    {
        return $this->orderProducts;
    }

    /**
     * @param Collection $orderProducts
     *
     * @return self
     */
    public function setOrderProducts(Collection $orderProducts): self
    {
        $this->orderProducts = $orderProducts;

        return $this;
    }

    /**
     * @return float
     */
    public function getRating(): float
    {
        return $this->rating;
    }

    /**
     * @param float $rating
     *
     * @return self
     */
    public function setRating(float $rating): self
    {
        $this->rating = $rating;

        return $this;
    }

    /**
     * @return ProductCategory[]|null
     */
    public function getProductCategory(): ?array
    {
        return $this->productCategory->toArray();
    }

    /**
     * @param ProductCategory|null $productCategory
     *
     * @return self
     */
    public function setProductCategory(?ProductCategory $productCategory): self
    {
        $this->productCategory = $productCategory;

        return $this;
    }
}
