<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\{
    ArrayCollection, Collection
};
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Promocode
 *
 * @ORM\Table(name="um_promocode")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PromocodeRepository")
 */
class Promocode implements EntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Coupon", mappedBy="promocode")
     * @Serializer\Exclude()
     */
    private $coupons;

    /**
     * @var string
     *
     * @ORM\Column(name="number", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="active", type="boolean")
     */
    private $active;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", length=11)
     */
    private $value;

    /**
     * @var integer
     *
     * @ORM\Column(type="integer", length=11)
     */
    private $count;

    /**
     * @var DateTime
     * @ORM\Column(name="date_from", type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $dateFrom;

    /**
     * @var DateTime
     * @ORM\Column(name="date_to", type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $dateTo;

    /**
     * @var DateTime
     * @ORM\Column(name="date_use_from", type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $dateUseFrom;

    /**
     * @var DateTime
     * @ORM\Column(name="date_use_to", type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $dateUseTo;

    /**
     * @var string
     *
     * @ORM\Column(name="type", length=255)
     * @Assert\NotBlank()
     * @Assert\Length(max="255")
     */
    private $type;

    /**
     * @var integer
     * @ORM\Column(name="count_per_user", type="integer", length=11)
     */
    private $countPerUser;

    /**
     * @var string|null
     * @ORM\Column(name="public_value", type="string", length=255, nullable=true)
     */
    private $publicValue;

    /**
     * @var integer|null
     * @ORM\Column(name="payment_type_id", type="integer", length=11, nullable=true)
     */
    private $paymentType;

    /**
     * @var integer|null
     * @ORM\Column(name="minimal_order_sum", type="integer", length=11, nullable=true)
     */
    private $minOrderSum;

    /**
     * @var integer|null
     * @ORM\Column(name="percent", type="integer", length=4, nullable=true)
     */
    private $percent;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return bool
     */
    public function isActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     *
     * @return self
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return int
     */
    public function getValue(): int
    {
        return $this->value;
    }

    /**
     * @param int $value
     *
     * @return self
     */
    public function setValue(int $value): self
    {
        $this->value = $value;

        return $this;
    }

    /**
     * @return int
     */
    public function getCount(): int
    {
        return $this->count;
    }

    /**
     * @param int $count
     *
     * @return self
     */
    public function setCount(int $count): self
    {
        $this->count = $count;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateFrom(): DateTime
    {
        return $this->dateFrom;
    }

    /**
     * @param DateTime $dateFrom
     *
     * @return self
     */
    public function setDateFrom(DateTime $dateFrom): self
    {
        $this->dateFrom = $dateFrom;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateTo(): DateTime
    {
        return $this->dateTo;
    }

    /**
     * @param DateTime $dateTo
     *
     * @return self
     */
    public function setDateTo(DateTime $dateTo): self
    {
        $this->dateTo = $dateTo;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateUseFrom(): DateTime
    {
        return $this->dateUseFrom;
    }

    /**
     * @param DateTime $dateUseFrom
     *
     * @return self
     */
    public function setDateUseFrom(DateTime $dateUseFrom): self
    {
        $this->dateUseFrom = $dateUseFrom;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getDateUseTo(): DateTime
    {
        return $this->dateUseTo;
    }

    /**
     * @param DateTime $dateUseTo
     *
     * @return self
     */
    public function setDateUseTo(DateTime $dateUseTo): self
    {
        $this->dateUseTo = $dateUseTo;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return self
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return int
     */
    public function getCountPerUser(): int
    {
        return $this->countPerUser;
    }

    /**
     * @param int $countPerUser
     *
     * @return self
     */
    public function setCountPerUser(int $countPerUser): self
    {
        $this->countPerUser = $countPerUser;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPublicValue(): ?string
    {
        return $this->publicValue;
    }

    /**
     * @param null|string $publicValue
     *
     * @return self
     */
    public function setPublicValue(?string $publicValue): self
    {
        $this->publicValue = $publicValue;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPaymentType(): ?int
    {
        return $this->paymentType;
    }

    /**
     * @param int|null $paymentType
     *
     * @return self
     */
    public function setPaymentType(?int $paymentType): self
    {
        $this->paymentType = $paymentType;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getMinOrderSum(): ?int
    {
        return $this->minOrderSum;
    }

    /**
     * @param int|null $minOrderSum
     *
     * @return self
     */
    public function setMinOrderSum(?int $minOrderSum): self
    {
        $this->minOrderSum = $minOrderSum;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPercent(): ?int
    {
        return $this->percent;
    }

    /**
     * @param int|null $percent
     *
     * @return self
     */
    public function setPercent(?int $percent): self
    {
        $this->percent = $percent;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getCoupons(): Collection
    {
        return $this->coupons;
    }

    /**
     * @param Collection $coupons
     *
     * @return self
     */
    public function setCoupons(Collection $coupons): self
    {
        $this->coupons = $coupons;

        return $this;
    }

}
