<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Rate
 *
 * @ORM\Table(name="um_price_rate")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RateRepository")
 */
class Rate implements EntityInterface
{

    /**
     *
     */
    public const RUB = 'RUB';

    /**
     *
     */
    public const USD = 'USD';

    /**
     *
     */
    public const EUR = 'EUR';

    /**
     *
     */
    public const BYN = 'BYN';

    /**
     *
     */
    public const DEFAULT_EXTRA_CHARGE_PERCENT = 5;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(length=10)
     */
    private $currency;

    /**
     * @var float
     *
     * @ORM\Column(type="float")
     */
    private $value;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $currency
     *
     * @return self
     */
    public function setCurrency($currency): self
    {
        $this->currency = $currency;
        return $this;
    }

    /**
     * @return string
     */
    public function getCurrency(): string
    {
        return $this->currency;
    }

    /**
     * @param float $value
     *
     * @return self
     */
    public function setValue($value): self
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return float
     */
    public function getValue(): float
    {
        return $this->value;
    }

}
