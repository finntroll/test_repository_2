<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Producer
 *
 * @ORM\Table(name="um_store_producer")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProducerRepository")
 */
class Producer implements EntityInterface
{
    /**
     *
     */
    public const STATUS_ACTIVE = 1;

    /**
     *
     */
    public const STATUS_NOT_ACTIVE = 2;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"catalog", "model_catalog"})
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name_short", length=150)
     * @Assert\NotBlank()
     * @Assert\Length(max="150")
     * @Serializer\Exclude()
     */
    private $nameShort;

    /**
     * @var string
     *
     * @ORM\Column(length=250)
     * @Assert\NotBlank()
     * @Assert\Length(max="250")
     * @Serializer\Groups({"catalog"})
     */
    private $name;

    /**
     * @var int
     * @ORM\Column(name="status", type="integer")
     * @Serializer\Exclude()
     */
    private $status = self::STATUS_ACTIVE;

    /**
     * @var int
     * @ORM\Column(name="sort", type="integer")
     * @Serializer\Exclude()
     */
    private $sort = 1;

    /**
     * @var ProductModel|null
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductModel", mappedBy="producer")
     * @Serializer\Exclude()
     */
    private $productModel;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     *
     * @param string $nameShort
     *
     * @return self
     */
    public function setNameShort($nameShort): self
    {
        $this->nameShort = $nameShort;
        return $this;
    }

    /**
     * @return string
     */
    public function getNameShort(): string
    {
        return $this->nameShort;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName($name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return self
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getSort(): int
    {
        return $this->sort;
    }

    /**
     * @param int $sort
     * @return self
     */
    public function setSort(int $sort): self
    {
        $this->sort = $sort;
        return $this;
    }

    /**
     * @return ProductModel|null
     */
    public function getProductModel(): ?ProductModel
    {
        return $this->productModel;
    }

    /**
     * @param ProductModel|null $productModel
     *
     * @return self
     */
    public function setProductModel(?ProductModel $productModel): self
    {
        $this->productModel = $productModel;

        return $this;
    }
}