<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\{
    ArrayCollection, Collection
};
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OrderProduct
 *
 * @ORM\Table(name="um_store_order_product")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderProductRepository")
 */
class OrderProduct implements EntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var UserOrder|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\UserOrder", inversedBy="orderProducts")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @var Product|null
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product", inversedBy="orderProducts")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id")
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="product_name", type="string", length=255)
     */
    private $productName;

    /**
     * @var string
     *
     * @ORM\Column(name="sku", type="string", length=255)
     */
    private $sku;

    /**
     * @var float
     * @ORM\Column(type="decimal", length=19, scale=3)
     * @Assert\Type("float")
     */
    private $price = 0.0;

    /**
     * @var float
     * @ORM\Column(name="base_currency_price", type="decimal", length=19, scale=3)
     * @Assert\Type("float")
     */
    private $baseCurrencyPrice = 0.0;

    /**
     * @var float|null
     * @ORM\Column(name="info_price", type="decimal", nullable=true, length=19, scale=3)
     * @Assert\Type("float")
     */
    private $infoPrice;

    /**
     * @var float|null
     * @ORM\Column(name="info_discount_price", type="decimal", nullable=true, length=19, scale=3)
     * @Assert\Type("float")
     */
    private $infoDiscountPrice;

    /**
     * @var float|null
     * @ORM\Column(name="info_discount", type="decimal", nullable=true, length=19, scale=3)
     * @Assert\Type("float")
     */
    private $infoDiscount;

    /**
     * @var float|null
     * @ORM\Column(name="info_extra_charge", type="decimal", nullable=true, length=10, scale=2)
     */
    private $infoExtraCharge;

    /**
     * @var bool
     * @ORM\Column(name="is_china_product", type="boolean")
     */
    private $isChinaProduct;

    /**
     * @var integer
     * @ORM\Column(name="quantity", type="integer", length=11)
     * @Assert\Type("integer")
     * @Assert\GreaterThanOrEqual(0)
     */
    private $quantity = 1;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return UserOrder|null
     */
    public function getOrder(): ?UserOrder
    {
        return $this->order;
    }

    /**
     * @param UserOrder|null $order
     *
     * @return self
     */
    public function setOrder(?UserOrder $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return Product|null
     */
    public function getProduct(): ?Product
    {
        return $this->product;
    }

    /**
     * @param Product|null $product
     *
     * @return self
     */
    public function setProduct(?Product $product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return self
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return float
     */
    public function getBaseCurrencyPrice(): float
    {
        return $this->baseCurrencyPrice;
    }

    /**
     * @param float $baseCurrencyPrice
     *
     * @return self
     */
    public function setBaseCurrencyPrice(float $baseCurrencyPrice): self
    {
        $this->baseCurrencyPrice = $baseCurrencyPrice;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getInfoPrice(): ?float
    {
        return $this->infoPrice;
    }

    /**
     * @param float|null $infoPrice
     *
     * @return self
     */
    public function setInfoPrice(?float $infoPrice): self
    {
        $this->infoPrice = $infoPrice;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getInfoDiscountPrice(): ?float
    {
        return $this->infoDiscountPrice;
    }

    /**
     * @param float|null $infoDiscountPrice
     *
     * @return self
     */
    public function setInfoDiscountPrice(?float $infoDiscountPrice): self
    {
        $this->infoDiscountPrice = $infoDiscountPrice;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getInfoDiscount(): ?float
    {
        return $this->infoDiscount;
    }

    /**
     * @param float|null $infoDiscount
     *
     * @return self
     */
    public function setInfoDiscount(?float $infoDiscount): self
    {
        $this->infoDiscount = $infoDiscount;

        return $this;
    }

    /**
     * @return float|null
     */
    public function getInfoExtraCharge(): ?float
    {
        return $this->infoExtraCharge;
    }

    /**
     * @param float|null $infoExtraCharge
     *
     * @return self
     */
    public function setInfoExtraCharge(?float $infoExtraCharge): self
    {
        $this->infoExtraCharge = $infoExtraCharge;

        return $this;
    }

    /**
     * @return bool
     */
    public function isChinaProduct(): bool
    {
        return $this->isChinaProduct;
    }

    /**
     * @param bool $isChinaProduct
     *
     * @return self
     */
    public function setIsChinaProduct(bool $isChinaProduct): self
    {
        $this->isChinaProduct = $isChinaProduct;

        return $this;
    }

    /**
     * @return int
     */
    public function getQuantity(): int
    {
        return $this->quantity;
    }

    /**
     * @param int $quantity
     *
     * @return self
     */
    public function setQuantity(int $quantity): self
    {
        $this->quantity = $quantity;

        return $this;
    }

    /**
     * @return string
     */
    public function getProductName(): string
    {
        return $this->productName;
    }

    /**
     * @param string $productName
     *
     * @return self
     */
    public function setProductName(string $productName): self
    {
        $this->productName = $productName;
        return $this;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     *
     * @return self
     */
    public function setSku(string $sku): self
    {
        $this->sku = $sku;
        return $this;
    }
}
