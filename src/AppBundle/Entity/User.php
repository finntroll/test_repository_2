<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\{
    ArrayCollection, Collection
};
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Validator\Constraints as Assert;


/**
 * User
 *
 * @ORM\Table(name="um_user_user")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 *
 * @UniqueEntity("email")
 */
class User implements EntityInterface, UserInterface
{

    private const BASIC_FAKE_UMKAMALL_DOMAIN   = '+fake@pochtamarket.com';
    private const DEFAULT_UMKA_MOBILE_NICKNAME = 'Pochtadroid';
    private const DEFAULT_FIRST_NAME = 'Мобильный клиент';

    public const ACCOUNT_TYPE_REGULAR = 0;
    public const ACCOUNT_TYPE_ANDROID = 1;
    public const ACCOUNT_TYPE_SOCIAL  = 2;

    /**
     *
     */
    public const GENDER_THING = 0;

    /**
     *
     */
    public const GENDER_MALE = 1;

    /**
     *
     */
    public const GENDER_FEMALE = 2;

    /**
     *
     */
    public const STATUS_BLOCK = 0;

    /**
     *
     */
    public const STATUS_ACTIVE = 1;

    /**
     *
     */
    public const STATUS_NOT_ACTIVE = 2;

    /**
     *
     */
    public const STATUS_DELETED = 3;

    /**
     *
     */
    public const EMAIL_CONFIRM_NO = 0;
    /**
     *
     */
    public const EMAIL_CONFIRM_YES = 1;

    /**
     *
     */
    public const ACCESS_LEVEL_USER = 0;
    /**
     *
     */
    public const ACCESS_LEVEL_ADMIN = 1;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="account_type", type="integer", nullable=false)
     */
    protected $accountType = self::ACCOUNT_TYPE_REGULAR;

    /**
     * @var string
     *
     * @ORM\Column(name="device_id", length=64, nullable=true)
     */
    protected $deviceId;

    /**
     * @var string
     *
     * @ORM\Column(name="google_account_name", length=64, nullable=true)
     */
    protected $googleAccountName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="first_name", length=250, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Your name cannot contain a number"
     * )
     * @Assert\NotBlank()
     * @Assert\Length(max="50")
     */
    private $firstName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="middle_name", length=250, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Length(max="50")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Your middle name cannot contain a number"
     * )
     */
    private $middleName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="last_name", length=250, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Length(max="50")
     * @Assert\Regex(
     *     pattern="/\d/",
     *     match=false,
     *     message="Your last name cannot contain a number"
     * )
     */
    private $lastName;

    /**
     * @var string|null
     *
     * @ORM\Column(name="nick_name", length=250, nullable=false)
     *
     * @Assert\Type("string")
     * @Assert\Length(min="2", max="50")
     * @Assert\Regex(pattern="/^[А-Яа-яA-Za-z_-]{2,50}$/iu")
     */
    private $nickName;

    /**
     * @var string
     *
     * @ORM\Column(length=250, unique=true)
     *
     * @Assert\Email()
     * @Assert\NotBlank()
     * @Assert\Length(max="50")
     */
    private $email;

    /**
     * @var int
     *
     * @ORM\Column(name="gender", type="smallint")
     *
     * @Assert\Type("integer")
     * @Assert\Choice({0, 1, 2}, message="Некорректное значения поля 'Пол'")
     */
    private $gender = self::GENDER_THING;

    /**
     * @var null|\DateTime
     *
     * @ORM\Column(name="birth_date", type="date", nullable=true)
     *
     * @Assert\Date()
     * @Serializer\Type("DateTime<'Y-m-d'>")
     */
    private $birthDate;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     */
    private $status = self::STATUS_NOT_ACTIVE;

    /**
     * @var int
     *
     * @ORM\Column(name="access_level", type="integer")
     */
    private $accessLevel = self::ACCESS_LEVEL_USER;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="visit_time", type="datetime", nullable=true)
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $visitTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="update_time", type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updateTime;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_time", type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createTime;

    /**
     * @var string
     *
     * @ORM\Column(length=255)
     * @Serializer\Exclude()
     */
    private $hash;

    /**
     * @var bool
     *
     * @Serializer\Exclude()
     * @ORM\Column(name="email_confirm", type="boolean")
     */
    private $emailConfirm = self::EMAIL_CONFIRM_NO;

    /**
     * @var string|null
     *
     * @ORM\Column(length=30, nullable=true)
     *
     * @Assert\Type("string")
     * @Assert\Regex(pattern="/^((\+?7|\+?375)(-?\d{2,3})-?)?(\d{3})(-?\d{4})$/", message="Некорректный формат телефона.")
     */
    private $phone;

    /**
     * @var
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserToken", mappedBy="user")
     * @Serializer\Exclude()
     */
    private $tokens;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Coupon", mappedBy="user")
     * @Serializer\Exclude()
     */
    private $coupons;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Product")
     * @ORM\JoinTable(name="um_favorite_user_favorite",
     *     joinColumns={@ORM\JoinColumn(name="user_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="product_id", referencedColumnName="id", unique=true)}
     *     )
     *
     * @Serializer\Exclude()
     */
    private $favorites;

    /**
     * @var Collection|CartProduct[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\CartProduct", mappedBy="user")
     * @Serializer\Exclude()
     */
    private $cartProducts;

    /**
     * @var ProductReview[]
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProductReview", mappedBy="user")
     * @Serializer\Exclude()
     */
    private $reviews;

    /**
     * @var Collection|UserOrder[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserOrder", mappedBy="user")
     */
    private $orders;

    /**
     * @var Collection|Dialog[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Dialog", mappedBy="user")
     *
     * @Serializer\Exclude()
     */
    private $dialogs;

    /**
     * @var Collection|UserBonus[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserBonus", mappedBy="user")
     *
     * @Serializer\Exclude()
     */
    private $bonuses;

    /**
     * @var Collection|PromocodeGift[]
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PromocodeGift", mappedBy="user")
     *
     * @Serializer\Exclude()
     */
    private $promocodeGifts;

    /**
     * User constructor.
     */
    public function __construct()
    {
        $this->tokens       = new ArrayCollection();
        $this->favorites    = new ArrayCollection();
        $this->cartProducts = new ArrayCollection();
        $this->reviews      = new ArrayCollection();
        $this->orders       = new ArrayCollection();
        $this->dialogs      = new ArrayCollection();
        $this->bonuses      = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string|null $firstName
     *
     * @return self
     */
    public function setFirstName(?string $firstName = null): self
    {
        $this->firstName = $firstName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getFirstName(): ?string
    {
        return $this->firstName;
    }

    /**
     * @param string|null $middleName
     *
     * @return self
     */
    public function setMiddleName(?string $middleName = null): self
    {
        $this->middleName = $middleName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getMiddleName(): ?string
    {
        return $this->middleName;
    }

    /**
     * @param string|null $lastName
     *
     * @return self
     */
    public function setLastName(?string $lastName = null): self
    {
        $this->lastName = $lastName;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getLastName(): ?string
    {
        return $this->lastName;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param int $gender
     *
     * @return self
     */
    public function setGender($gender): self
    {
        $this->gender = $gender;
        return $this;
    }

    /**
     * @return int
     */
    public function getGender(): int
    {
        return $this->gender;
    }

    /**
     * @param null|\DateTime $birthDate
     *
     * @return self
     */
    public function setBirthDate(?\DateTime $birthDate): self
    {
        $this->birthDate = $birthDate;
        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getBirthDate(): ?\DateTime
    {
        return $this->birthDate;
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $accessLevel
     *
     * @return self
     */
    public function setAccessLevel(int $accessLevel): self
    {
        $this->accessLevel = $accessLevel;
        return $this;
    }

    /**
     * @return int
     */
    public function getAccessLevel(): int
    {
        return $this->accessLevel;
    }

    /**
     * @param \DateTime|null $visitTime
     *
     * @return self
     */
    public function setVisitTime(?\DateTime $visitTime = null): self
    {
        $this->visitTime = $visitTime;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getVisitTime(): ?\DateTime
    {
        return $this->visitTime;
    }

    /**
     * @param \DateTime $updateTime
     *
     * @return self
     */
    public function setUpdateTime(\DateTime $updateTime): self
    {
        $this->updateTime = $updateTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getUpdateTime(): \DateTime
    {
        return $this->updateTime;
    }

    /**
     * @param \DateTime $createTime
     *
     * @return self
     */
    public function setCreateTime(\DateTime $createTime): self
    {
        $this->createTime = $createTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getCreateTime(): \DateTime
    {
        return $this->createTime;
    }

    /**
     * @param string $hash
     *
     * @return self
     */
    public function setHash(string $hash): self
    {
        $this->hash = $hash;
        return $this;
    }

    /**
     * @return string
     */
    public function getHash(): string
    {
        return $this->hash;
    }

    /**
     * @param bool $emailConfirm
     *
     * @return self
     */
    public function setEmailConfirm(bool $emailConfirm): self
    {
        $this->emailConfirm = $emailConfirm;
        return $this;
    }

    /**
     * @return boolean
     */
    public function getEmailConfirm(): bool
    {
        return $this->emailConfirm;
    }

    /**
     * @param string|null $phone
     *
     * @return self
     */
    public function setPhone(?string $phone = null): self
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getPhone(): ?string
    {
        return $this->phone;
    }

    /**
     * @return mixed
     */
    public function getTokens()
    {
        return $this->tokens;
    }

    /**
     * @param UserToken $token
     * @return self
     */
    public function addToken(UserToken $token): self
    {
        $this->tokens[] = $token;
        return $this;
    }

    /**
     * @param UserToken $token
     * @return self
     */
    public function removeToken(UserToken $token): self
    {
        $this->tokens->removeElement($token);
        return $this;
    }


    /**
     * Returns the roles granted to the user.
     *
     * @return array (Role|string)[] The user roles
     */
    public function getRoles(): array
    {
        return ['ROLE_USER'];
    }

    /**
     * Returns the password used to authenticate the user.
     *
     * This should be the encoded password. On authentication, a plain-text
     * password will be salted, encoded, and then compared to this value.
     *
     * @return string The password
     */
    public function getPassword(): string
    {
        return $this->getHash();
        // TODO: Implement getPassword() method.
    }

    /**
     * Returns the salt that was originally used to encode the password.
     *
     * This can return null if the password was not encoded using a salt.
     *
     * @return string|null The salt
     */
    public function getSalt(): ?string
    {
        // TODO: Implement getSalt() method.
    }

    /**
     * Returns the username used to authenticate the user.
     *
     * @return string The username
     */
    public function getUsername(): string
    {
        if ($this->getAccountType() === static::ACCOUNT_TYPE_REGULAR) {
            return $this->getEmail();
        }
        return $this->getDeviceId();
    }

    /**
     * Removes sensitive data from the user.
     *
     * This is important if, at any given point, sensitive information like
     * the plain-text password is stored on this object.
     */
    public function eraseCredentials(): void
    {
        // TODO: Implement eraseCredentials() method.
    }

    /**
     * @return integer
     */
    public function getAccountType(): ?int
    {
        return $this->accountType;
    }

    /**
     * @param integer $accountType
     * @return $this
     */
    public function setAccountType(int $accountType): self
    {
        $this->accountType = $accountType;
        return $this;
    }

    /**
     * @return string
     */
    public function getGoogleAccountName(): ?string
    {
        return $this->googleAccountName;
    }

    /**
     * @param string $googleAccountName
     * @return $this
     */
    public function setGoogleAccountName(string $googleAccountName): self
    {
        $this->googleAccountName = $googleAccountName;
        return $this;
    }

    /**
     * @return string
     */
    public function getDeviceId(): ?string
    {
        return $this->deviceId;
    }

    /**
     * @param string $deviceId
     * @return $this
     */
    public function setDeviceId(string $deviceId)
    {
        $this->deviceId = $deviceId;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getFavorites(): Collection
    {
        return $this->favorites;
    }

    /**
     * @param Product $product
     */
    public function addFavorite(Product $product): void
    {
        if (!$this->favorites->contains($product)) {
            $this->favorites->add($product);
        }
    }

    /**
     * @param Product $product
     */
    public function removeFavorite(Product $product): void
    {
        $this->favorites->removeElement($product);
    }

    /**
     * @return null|string
     */
    public function getNickName(): ?string
    {
        return $this->nickName;
    }

    /**
     * @param null|string $nickName
     * @return $this
     */
    public function setNickName($nickName): self
    {
        $this->nickName = $nickName;
        return $this;
    }

    /**
     * @param string      $deviceId
     * @param null|string $googleAccountName
     * @return User
     */
    public static function createNewAndroidUser(string $deviceId, ?string $googleAccountName = null): User
    {
        $user = new User();
        $user
            ->setAccountType(self::ACCOUNT_TYPE_ANDROID)
            ->setCreateTime(new \DateTime())
            ->setDeviceId($deviceId)
            ->setUpdateTime(new \DateTime())
            //set dump password for db constraint
            ->setHash(md5($deviceId . time()))
            ->setFirstName(self::DEFAULT_FIRST_NAME)
            ->setNickName(self::DEFAULT_UMKA_MOBILE_NICKNAME)
            ->setStatus(self::STATUS_ACTIVE);
        if (null !== $googleAccountName) {
            $user
                ->setGoogleAccountName($googleAccountName)
                ->setEmail($googleAccountName);
        } else {
            //set stub for supporting database constraints
            $user->setEmail(sprintf('%s%s', md5($deviceId), self::BASIC_FAKE_UMKAMALL_DOMAIN));
        }
        return $user;
    }

    /**
     * @param string      $email
     * @param null|string $firstName
     * @param null|string $lastName
     * @return User
     */
    public static function createNewSocialUser(string $email, ?string $firstName, ?string $lastName): User
    {
        $user = new User();
        $user
            ->setAccountType(self::ACCOUNT_TYPE_ANDROID)
            ->setCreateTime(new \DateTime())
            ->setUpdateTime(new \DateTime())
            ->setHash(md5($email . time()))
            ->setNickName($email)
            ->setStatus(self::STATUS_ACTIVE)
            ->setEmail($email)
            ->setFirstName($firstName)
            ->setLastName($lastName);
        return $user;
    }

    /**
     * @return CartProduct[]|Collection
     */
    public function getCartProducts()
    {
        return $this->cartProducts;
    }

    /**
     * @return ProductReview[]
     */
    public function getReviews(): array
    {
        return $this->reviews;
    }

    /**
     * @param ProductReview[] $reviews
     * @return $this
     */
    public function setReviews(array $reviews): self
    {
        $this->reviews = $reviews;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getCoupons(): Collection
    {
        return $this->coupons;
    }

    /**
     * @param Collection $coupons
     *
     * @return self
     */
    public function setCoupons(Collection $coupons): self
    {
        $this->coupons = $coupons;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getOrders(): Collection
    {
        return $this->orders;
    }

    /**
     * @param Collection $orders
     *
     * @return self
     */
    public function setOrders(Collection $orders): self
    {
        $this->orders = $orders;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getDialogs(): Collection
    {
        return $this->dialogs;
    }

    /**
     * @param Collection $dialogs
     * @return self
     */
    public function setDialogs(Collection $dialogs): self
    {
        $this->dialogs = $dialogs;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getBonuses(): Collection
    {
        return $this->bonuses;
    }

    /**
     * @param Collection $bonuses
     * @return User
     */
    public function setBonuses(Collection $bonuses): self
    {
        $this->bonuses = $bonuses;
        return $this;
    }

    /**
     * @return Collection|PromocodeGift[]
     */
    public function getPromocodeGifts(): Collection
    {
        return $this->promocodeGifts;
    }

    /**
     * @param Collection $promocodeGifts
     * @return self
     */
    public function setPromocodeGifts(Collection $promocodeGifts): self
    {
        $this->promocodeGifts = $promocodeGifts;
        return $this;
    }
}
