<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Dialog
 *
 * @ORM\Table(name="um_feedback_dialog")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DialogRepository")
 */
class Dialog
{

    /**
     *
     */
    public const STATUS_NOT_PROCESSED = 0;

    /**
     *
     */
    public const STATUS_PROCESSED = 1;

    /**
     *
     */
    public const STATUS_DEFERRED = 2;

    /**
     *
     */
    public const STATUS_NOT_REQUIRE_ANSWER = 3;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", length=11)
     */
    private $userId;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="dialogs")
     *
     * @var User
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="smallint")
     */
    private $status;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="create_time", type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createTime;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="update_time", type="datetime", nullable=true)
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updateTime;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $status
     *
     * @return self
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param DateTime $createTime
     *
     * @return self
     */
    public function setCreateTime(DateTime $createTime): self
    {
        $this->createTime = $createTime;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreateTime(): DateTime
    {
        return $this->createTime;
    }

    /**
     * @param DateTime|null $updateTime
     *
     * @return self
     */
    public function setUpdateTime(?DateTime $updateTime = null): self
    {
        $this->updateTime = $updateTime;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getUpdateTime(): ?DateTime
    {
        return $this->updateTime;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user

     * @return self
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        $this->userId = $user->getId();

        return $this;
    }
}
