<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\{
    ArrayCollection, Collection
};
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * OrderPayment
 *
 * @ORM\Table(name="um_payment_transaction")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderPaymentRepository")
 */
class OrderPayment implements EntityInterface
{
    /**
     * @inheritdoc
     */
    public const TYPE_ORDER_PAYMENT = 'order_payment';

    /**
     * @inheritdoc
     */
    public const STATUS_NEW = 'new';

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var UserOrder|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\UserOrder", inversedBy="orderPayments")
     * @ORM\JoinColumn(name="order_id", referencedColumnName="id")
     */
    private $order;

    /**
     * @var string
     *
     * @ORM\Column(name="number", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(max="50")
     */
    private $number;

    /**
     * @var string
     *
     * @ORM\Column(name="type", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(max="50")
     */
    private $type;

    /**
     * @var float
     * @ORM\Column(type="decimal", length=19, scale=3)
     * @Assert\Type("float")
     */
    private $sum = 0.0;

    /**
     * @var string
     *
     * @ORM\Column(name="status", length=50)
     * @Assert\NotBlank()
     * @Assert\Length(max="50")
     */
    private $status;

    /**
     * @var string|null
     * @ORM\Column(type="text", nullable=true)
     */
    private $comment;

    /**
     * @var integer|null
     * @ORM\Column(name="payment_method_id", type="integer", length=11, nullable=true)
     * @Assert\Type("integer")
     */
    private $paymentMethodId;

    /**
     * @var DateTime|null
     * @ORM\Column(name="payment_time", type="datetime", nullable=true)
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $paymentTime;

    /**
     * @var string|null
     * @ORM\Column(name="payment_details", type="text", nullable=true)
     */
    private $paymentDetails;

    /**
     * @var DateTime
     * @ORM\Column(name="ts_created", type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $transactionCreated;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return UserOrder|null
     */
    public function getOrder(): ?UserOrder
    {
        return $this->order;
    }

    /**
     * @param UserOrder|null $order
     *
     * @return self
     */
    public function setOrder(?UserOrder $order): self
    {
        $this->order = $order;

        return $this;
    }

    /**
     * @return string
     */
    public function getNumber(): string
    {
        return $this->number;
    }

    /**
     * @param string $number
     *
     * @return self
     */
    public function setNumber(string $number): self
    {
        $this->number = $number;

        return $this;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     *
     * @return self
     */
    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    /**
     * @return float
     */
    public function getSum(): float
    {
        return $this->sum;
    }

    /**
     * @param float $sum
     *
     * @return self
     */
    public function setSum(float $sum): self
    {
        $this->sum = $sum;

        return $this;
    }

    /**
     * @return string
     */
    public function getStatus(): string
    {
        return $this->status;
    }

    /**
     * @param string $status
     *
     * @return self
     */
    public function setStatus(string $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getComment(): ?string
    {
        return $this->comment;
    }

    /**
     * @param null|string $comment
     *
     * @return self
     */
    public function setComment(?string $comment): self
    {
        $this->comment = $comment;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getPaymentMethodId(): ?int
    {
        return $this->paymentMethodId;
    }

    /**
     * @param int|null $paymentMethodId
     *
     * @return self
     */
    public function setPaymentMethodId(?int $paymentMethodId): self
    {
        $this->paymentMethodId = $paymentMethodId;

        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getPaymentTime(): ?DateTime
    {
        return $this->paymentTime;
    }

    /**
     * @param DateTime|null $paymentTime
     *
     * @return self
     */
    public function setPaymentTime(?DateTime $paymentTime): self
    {
        $this->paymentTime = $paymentTime;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getPaymentDetails(): ?string
    {
        return $this->paymentDetails;
    }

    /**
     * @param null|string $paymentDetails
     *
     * @return self
     */
    public function setPaymentDetails(?string $paymentDetails): self
    {
        $this->paymentDetails = $paymentDetails;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getTransactionCreated(): DateTime
    {
        return $this->transactionCreated;
    }

    /**
     * @param DateTime $transactionCreated
     *
     * @return self
     */
    public function setTransactionCreated(DateTime $transactionCreated): self
    {
        $this->transactionCreated = $transactionCreated;

        return $this;
    }
}
