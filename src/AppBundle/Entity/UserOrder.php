<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\{
    ArrayCollection, Collection
};
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * UserOrder
 *
 * @ORM\Table(name="um_store_order")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\OrderRepository")
 */
class UserOrder implements EntityInterface
{
    /**
     * @inheritdoc
     */
    public const STATUS_NEW = 'new';

    /**
     * @inheritdoc
     */
    public const STATUS_DELETED = 'deleted';

    /**
     * @inheritdoc
     */
    public const PAYMENT_METHOD_CARD_ID = 2;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string|null
     *
     * @ORM\Column(name="status", type="string", length=50, nullable=true)
     */
    private $status;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="date", type="datetime", nullable=true)
     *
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     * @Serializer\SerializedName("date_time")
     */
    private $date;

    /**
     * @var int|null
     *
     * @ORM\Column(name="user_id", type="integer", nullable=true)
     *
     * @Serializer\Exclude()
     */
    private $userId;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="orders")
     *
     * @var User
     */
    private $user;

    /**
     * @var float
     *
     * @ORM\Column(name="total_price", type="decimal", length=10, scale=2)
     *
     * @Serializer\SerializedName("cost")
     */
    private $totalPrice = 0.0;

    /**
     * @var int
     *
     * @ORM\Column(name="delivery_id", type="integer", length=11)
     *
     * @Serializer\Exclude()
     */
    private $deliveryId =1;

    /**
     * @var float
     *
     * @ORM\Column(name="delivery_price", type="decimal", length=10, scale=2)
     *
     * @Serializer\Exclude()
     */
    private $deliveryPrice = 0.0;

    /**
     * @var int
     *
     * @ORM\Column(name="paid", type="integer", length=4)
     *
     * @Serializer\Exclude()
     */
    private $paid = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string", length=255)
     *
     * @Serializer\Exclude()
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string", length=255)
     *
     * @Serializer\Exclude()
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string", length=255)
     *
     * @Serializer\Exclude()
     */
    private $phone;

    /**
     * @var string|null
     *
     * @ORM\Column(name="tin", type="string", length=12, nullable=true)
     *
     * @Serializer\Exclude()
     */
    private $tin;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string", length=255)
     *
     * @Serializer\Exclude()
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="url", type="string", length=255)
     */
    private $url;

    /**
     * @var DateTime|null
     *
     * @ORM\Column(name="modified", type="datetime", nullable=true)
     *
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     * @Serializer\Exclude()
     */
    private $modified;

    /**
     * @var string
     *
     * @ORM\Column(name="zipcode", type="string", length=30)
     *
     * @Serializer\Exclude()
     */
    private $zipcode;

    /**
     * @var string
     *
     * @ORM\Column(name="country", type="string", length=150)
     *
     * @Serializer\Exclude()
     */
    private $country;

    /**
     * @var string
     *
     * @ORM\Column(name="region", type="string", length=255)
     *
     * @Serializer\Exclude()
     */
    private $region;

    /**
     * @var string
     *
     * @ORM\Column(name="city", type="string", length=255)
     *
     * @Serializer\Exclude()
     */
    private $city;

    /**
     * @var float
     *
     * @ORM\Column(name="discount", type="integer")
     *
     * @Serializer\Exclude()
     */
    private $discount = 0.0;

    /**
     * @var int
     *
     * @ORM\Column(name="paid_with_bonus", type="integer")
     *
     * @Serializer\Exclude()
     */
    private $paidWithBonus = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="total_price_base", type="decimal", length=10, scale=2)
     *
     * @Serializer\Exclude()
     */
    private $totalPriceBase = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="total_price_user_currency", type="decimal", length=10, scale=2)
     *
     * @Serializer\Exclude()
     */
    private $totalPriceUserCurrency = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="user_currency", type="string", length=3)
     *
     * @Serializer\Exclude()
     */
    private $userCurrency = 0;

    /**
     * @var float
     *
     * @ORM\Column(name="currency_rate", type="decimal", length=10, scale=2)
     *
     * @Serializer\Exclude()
     */
    private $currencyRate = 0;

    /**
     * @var integer
     *
     * @ORM\Column(name="payment_method_id", type="integer", length=3)
     *
     * @Serializer\Exclude()
     */
    private $paymentMethodId = self::PAYMENT_METHOD_CARD_ID;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\OrderProduct", mappedBy="order")
     *
     * @Serializer\Exclude()
     */
    private $orderProducts;

    /**
     * @var ArrayCollection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\OrderPayment", mappedBy="order")
     *
     * @Serializer\Exclude()
     */
    private $orderPayments;

    /**
     * UserOrder constructor.
     */
    public function __construct()
    {
        $this->orderProducts = new ArrayCollection();
        $this->orderPayments = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string|null $status
     *
     * @return self
     */
    public function setStatus(?string $status = null): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getStatus(): ?string
    {
        return $this->status;
    }

    /**
     * @param DateTime|null $date
     *
     * @return self
     */
    public function setDate(?DateTime $date = null): self
    {
        $this->date = $date;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getDate(): ?DateTime
    {
        return $this->date;
    }

    /**
     * @return int|null
     */
    public function getUserId(): ?int
    {
        return $this->userId;
    }

    /**
     * @return float
     */
    public function getTotalPrice(): float
    {
        return $this->totalPrice;
    }

    /**
     * @param float $totalPrice
     *
     * @return self
     */
    public function setTotalPrice(float $totalPrice): self
    {
        $this->totalPrice = $totalPrice;
        return $this;
    }

    /**
     * @return float
     */
    public function getDiscount(): float
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     *
     * @return self
     */
    public function setDiscount(float $discount): self
    {
        $this->discount = $discount;
        return $this;
    }

    /**
     * @return int
     */
    public function getPaidWithBonus(): int
    {
        return $this->paidWithBonus;
    }

    /**
     * @param int $paidWithBonus
     *
     * @return self
     */
    public function setPaidWithBonus(int $paidWithBonus): self
    {
        $this->paidWithBonus = $paidWithBonus;
        return $this;
    }

    /**
     * @return Collection
     */
    public function getOrderProducts(): Collection
    {
        return $this->orderProducts;
    }

    /**
     * @param OrderProduct $orderProduct
     *
     * @return self
     */
    public function addOrderProduct(OrderProduct $orderProduct): self
    {
        $this->orderProducts[] = $orderProduct;

        return $this;
    }

    /**
     * @param OrderProduct $orderProduct
     */
    public function removeOrderProduct(OrderProduct $orderProduct): void
    {
        $this->orderProducts->removeElement($orderProduct);
    }

    /**
     * @param Collection $orderProducts
     *
     * @return self
     */
    public function setOrderProducts(Collection $orderProducts): self
    {
        $this->orderProducts = $orderProducts;

        return $this;
    }

    /**
     * @return Collection
     */
    public function getOrderPayments(): Collection
    {
        return $this->orderPayments;
    }

    /**
     * @param OrderPayment $orderPayment
     *
     * @return self
     */
    public function addOrderPayment(OrderPayment $orderPayment): self
    {
        $this->orderPayments[] = $orderPayment;

        return $this;
    }

    /**
     * @param OrderPayment $orderPayment
     */
    public function removeOrderPayment(OrderPayment $orderPayment): void
    {
        $this->orderPayments->removeElement($orderPayment);
    }

    /**
     * @param Collection $orderPayments
     *
     * @return self
     */
    public function setOrderPayments(Collection $orderPayments): self
    {
        $this->orderPayments = $orderPayments;

        return $this;
    }

    /**
     * @return int
     */
    public function getDeliveryId(): int
    {
        return $this->deliveryId;
    }

    /**
     * @param int $deliveryId
     *
     * @return self
     */
    public function setDeliveryId(int $deliveryId): self
    {
        $this->deliveryId = $deliveryId;
        return $this;
    }

    /**
     * @return float
     */
    public function getDeliveryPrice(): float
    {
        return $this->deliveryPrice;
    }

    /**
     * @param float $deliveryPrice
     *
     * @return self
     */
    public function setDeliveryPrice(float $deliveryPrice): self
    {
        $this->deliveryPrice = $deliveryPrice;
        return $this;
    }

    /**
     * @return int
     */
    public function getPaid(): int
    {
        return $this->paid;
    }

    /**
     * @param int $paid
     *
     * @return self
     */
    public function setPaid(int $paid): self
    {
        $this->paid = $paid;
        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;
        return $this;
    }

    /**
     * @return string
     */
    public function getAddress(): string
    {
        return $this->address;
    }

    /**
     * @param string $address
     *
     * @return self
     */
    public function setAddress(string $address): self
    {
        $this->address = $address;

        return $this;
    }

    /**
     * @return string
     */
    public function getPhone(): string
    {
        return $this->phone;
    }

    /**
     * @param string $phone
     *
     * @return self
     */
    public function setPhone(string $phone): self
    {
        $this->phone = $phone;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     *
     * @return self
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return string
     */
    public function getUrl(): string
    {
        return $this->url;
    }

    /**
     * @param string $url
     *
     * @return self
     */
    public function setUrl(string $url): self
    {
        $this->url = $url;
        return $this;
    }

    /**
     * @return string
     */
    public function getZipcode(): string
    {
        return $this->zipcode;
    }

    /**
     * @param string $zipcode
     *
     * @return self
     */
    public function setZipcode(string $zipcode): self
    {
        $this->zipcode = $zipcode;
        return $this;
    }

    /**
     * @return string
     */
    public function getCountry(): string
    {
        return $this->country;
    }

    /**
     * @param string $country
     *
     * @return self
     */
    public function setCountry(string $country): self
    {
        $this->country = $country;
        return $this;
    }

    /**
     * @return string
     */
    public function getRegion(): string
    {
        return $this->region;
    }

    /**
     * @param string $region
     *
     * @return self
     */
    public function setRegion(string $region): self
    {
        $this->region = $region;
        return $this;
    }

    /**
     * @return string
     */
    public function getCity(): string
    {
        return $this->city;
    }

    /**
     * @param string $city
     *
     * @return self
     */
    public function setCity(string $city): self
    {
        $this->city = $city;
        return $this;
    }

    /**
     * @return DateTime|null
     */
    public function getModified(): ?DateTime
    {
        return $this->modified;
    }

    /**
     * @param DateTime|null $modified
     *
     * @return self
     */
    public function setModified(?DateTime $modified): self
    {
        $this->modified = $modified;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPriceBase(): float
    {
        return $this->totalPriceBase;
    }

    /**
     * @param float $totalPriceBase
     *
     * @return self
     */
    public function setTotalPriceBase(float $totalPriceBase): self
    {
        $this->totalPriceBase = $totalPriceBase;
        return $this;
    }

    /**
     * @return float
     */
    public function getTotalPriceUserCurrency(): float
    {
        return $this->totalPriceUserCurrency;
    }

    /**
     * @param float $totalPriceUserCurrency
     *
     * @return self
     */
    public function setTotalPriceUserCurrency(float $totalPriceUserCurrency): self
    {
        $this->totalPriceUserCurrency = $totalPriceUserCurrency;
        return $this;
    }

    /**
     * @return string
     */
    public function getUserCurrency(): string
    {
        return $this->userCurrency;
    }

    /**
     * @param string $userCurrency
     *
     * @return self
     */
    public function setUserCurrency(string $userCurrency): self
    {
        $this->userCurrency = $userCurrency;
        return $this;
    }

    /**
     * @return float
     */
    public function getCurrencyRate(): float
    {
        return $this->currencyRate;
    }

    /**
     * @param float $currencyRate
     *
     * @return self
     */
    public function setCurrencyRate(float $currencyRate): self
    {
        $this->currencyRate = $currencyRate;
        return $this;
    }

    /**
     * @return int
     */
    public function getPaymentMethodId(): int
    {
        return $this->paymentMethodId;
    }

    /**
     * @param int $paymentMethodId
     *
     * @return self
     */
    public function setPaymentMethodId(int $paymentMethodId): self
    {
        $this->paymentMethodId = $paymentMethodId;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getTin(): ?string
    {
        return $this->tin;
    }

    /**
     * @param string|null $tin
     *
     * @return self
     */
    public function setTin(?string $tin): self
    {
        $this->tin = $tin;
        return $this;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user

     * @return self
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        $this->userId = $user->getId();

        return $this;
    }
}
