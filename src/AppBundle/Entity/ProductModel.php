<?php

namespace AppBundle\Entity;

use DateTime;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductModel
 *
 * @ORM\Table(name="um_product_model")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductModelRepository")
 */
class ProductModel
{
    /**
     * @inheritdoc
     */
    public const STATUS_NOT_ACTIVE = 0;

    /**
     * @inheritdoc
     */
    public const STATUS_ACTIVE = 1;

    /**
     * @inheritdoc
     */
    public const MODERATION_CONFIRMED = 1;

    /**
     * @inheritdoc
     */
    public const PRODUCT_IN_STOCK = 1;

    /**
     * @inheritdoc
     */
    public const PRODUCT_NOT_IN_STOCK = 0;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Groups({"model_catalog"})
     */
    private $id;

    /**
     * @var Category|null
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Category", inversedBy="productModel")
     * @ORM\JoinColumn(name="category_id", referencedColumnName="id")
     */
    private $category;

    /**
     * @var Producer|null
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Producer", inversedBy="productModel")
     * @ORM\JoinColumn(name="producer_id", referencedColumnName="id")
     */
    private $producer;

    /**
     * @var ArrayCollection|Product
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Product", mappedBy="productModel")
     */
    private $product;

    /**
     * @var string
     *
     * @ORM\Column(name="name", length=512, nullable=false)
     * @Assert\Length(max="512")
     * @Serializer\Groups({"model_catalog"})
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="image", length=255, nullable=true)
     * @Assert\Length(max="255")
     * @Serializer\Exclude()
     */
    private $image;

    /**
     * @var bool
     * @ORM\Column(name="is_china_product", type="boolean")
     * @Serializer\Groups({"model_catalog"})
     */
    private $isChinaProduct;

    /**
     * @var string
     *
     * @ORM\Column(name="model_name", length=255, nullable=false)
     * @Assert\Length(max="255")
     */
    private $modelName;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="text", nullable=true)
     * @Serializer\Exclude()
     */
    private $description;

    /**
     * @var float
     *
     * @ORM\Column(name="min_price", type="decimal", length=19, scale=3)
     * @Assert\Type("float")
     * @Serializer\Exclude()
     */
    private $minPrice;

    /**
     * @var float
     *
     * @ORM\Column(name="price", type="decimal", length=19, scale=3, nullable=true)
     * @Assert\Type("float")
     * @Serializer\Exclude()
     */
    private $price;

    /**
     * @var float
     *
     * @ORM\Column(name="discount_price", type="decimal", length=19, scale=3, nullable=true)
     * @Assert\Type("float")
     * @Serializer\Groups({"model_catalog"})
     */
    private $discountPrice;

    /**
     * @var float
     *
     * @ORM\Column(name="discount", type="decimal", length=19, scale=3, nullable=true)
     * @Assert\Type("float")
     * @Serializer\Exclude()
     */
    private $discount;

    /**
     * @var array|null
     *
     * @ORM\Column(name="product_attributes", type="json", nullable=true)
     * @Serializer\Exclude()
     */
    private $attributes;

    /**
     * @var array|null
     *
     * @ORM\Column(name="active_product_ids", type="json", nullable=true)
     * @Serializer\SerializedName("products")
     * @Serializer\Groups({"model_catalog"})
     */
    private $activeProductIds;

    /**
     * @var bool $inStock
     * @ORM\Column(name="in_stock", type="boolean")
     * @Serializer\Exclude()
     */
    private $inStock;

    /**
     * @var bool $active
     * @ORM\Column(name="active", type="boolean")
     * @Serializer\Exclude()
     */
    private $active;

    /**
     * @var bool $status
     * @ORM\Column(name="status", type="boolean")
     * @Serializer\Exclude()
     */
    private $status;

    /**
     * @var bool $moderation
     * @ORM\Column(name="moderation", type="boolean")
     * @Serializer\Exclude()
     */
    private $moderation;

    /**
     * @var DateTime
     *
     * @ORM\Column(name="update_time", type="datetime")
     *
     * @Serializer\Exclude()
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $updateTime;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return Category|null
     */
    public function getCategory(): ?Category
    {
        return $this->category;
    }

    /**
     * @param Category|null $category
     *
     * @return self
     */
    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }

    /**
     * @return Producer|null
     */
    public function getProducer(): ?Producer
    {
        return $this->producer;
    }

    /**
     * @param Producer|null $producer
     *
     * @return self
     */
    public function setProducer(?Producer $producer): self
    {
        $this->producer = $producer;

        return $this;
    }

    /**
     * @return Product|ArrayCollection
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @param Product|ArrayCollection $product
     *
     * @return self
     */
    public function setProduct($product): self
    {
        $this->product = $product;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     *
     * @return self
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getImage(): ?string
    {
        return $this->image;
    }

    /**
     * @param string $image
     *
     * @return self
     */
    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return string
     */
    public function getModelName(): string
    {
        return $this->modelName;
    }

    /**
     * @param string $modelName
     *
     * @return self
     */
    public function setModelName(string $modelName): self
    {
        $this->modelName = $modelName;

        return $this;
    }

    /**
     * @return float
     */
    public function getMinPrice(): float
    {
        return $this->minPrice;
    }

    /**
     * @param float $minPrice
     *
     * @return self
     */
    public function setMinPrice(float $minPrice): self
    {
        $this->minPrice = $minPrice;

        return $this;
    }

    /**
     * @return array
     */
    public function getAttributes(): ?array
    {
        return $this->attributes;
    }

    /**
     * @param array $attributes
     *
     * @return self
     */
    public function setAttributes(array $attributes): self
    {
        $this->attributes = $attributes;

        return $this;
    }

    /**
     * @return bool
     */
    public function isChinaProduct(): bool
    {
        return $this->isChinaProduct;
    }

    /**
     * @param bool $isChinaProduct
     *
     * @return self
     */
    public function setIsChinaProduct(bool $isChinaProduct): self
    {
        $this->isChinaProduct = $isChinaProduct;
        return $this;
    }

    /**
     * @return float
     */
    public function getPrice(): ?float
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return self
     */
    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    /**
     * @return float
     */
    public function getDiscountPrice(): ?float
    {
        return $this->discountPrice;
    }

    /**
     * @param float $discountPrice
     *
     * @return self
     */
    public function setDiscountPrice(float $discountPrice): self
    {
        $this->discountPrice = $discountPrice;

        return $this;
    }

    /**
     * @return float
     */
    public function getDiscount(): ?float
    {
        return $this->discount;
    }

    /**
     * @param float $discount
     *
     * @return self
     */
    public function setDiscount(float $discount): self
    {
        $this->discount = $discount;

        return $this;
    }

    /**
     * @return null|string
     */
    public function getDescription(): ?string
    {
        return $this->description;
    }

    /**
     * @param null|string $description
     *
     * @return self
     */
    public function setDescription(?string $description): self
    {
        $this->description = $description;

        return $this;
    }

    /**
     * @return array|null
     */
    public function getActiveProductIds(): ?array
    {
        return $this->activeProductIds;
    }

    /**
     * @param array|null $activeProductIds
     *
     * @return self
     */
    public function setActiveProductIds(?array $activeProductIds): self
    {
        $this->activeProductIds = $activeProductIds;

        return $this;
    }

    /**
     * @return bool
     */
    public function getInStock(): bool
    {
        return $this->inStock;
    }

    /**
     * @param bool $inStock
     *
     * @return self
     */
    public function setInStock(bool $inStock): self
    {
        $this->inStock = $inStock;

        return $this;
    }

    /**
     * @return bool
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * @param bool $active
     *
     * @return self
     */
    public function setActive(bool $active): self
    {
        $this->active = $active;

        return $this;
    }

    /**
     * @return bool
     */
    public function getStatus(): bool
    {
        return $this->status;
    }

    /**
     * @param bool $status
     *
     * @return self
     */
    public function setStatus(bool $status): self
    {
        $this->status = $status;

        return $this;
    }

    /**
     * @return bool
     */
    public function getModeration(): bool
    {
        return $this->moderation;
    }

    /**
     * @param bool $moderation
     *
     * @return self
     */
    public function setModeration(bool $moderation): self
    {
        $this->moderation = $moderation;

        return $this;
    }

    /**
     * @return DateTime
     */
    public function getUpdateTime(): DateTime
    {
        return $this->updateTime;
    }

    /**
     * @param DateTime $updateTime
     *
     * @return self
     */
    public function setUpdateTime(DateTime $updateTime): self
    {
        $this->updateTime = $updateTime;
        
        return $this;
    }
}