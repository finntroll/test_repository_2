<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * Class CommentImage
 *
 * @package AppBundle\Entity
 *
 * @ORM\Table(name="um_comment_image")
 * @ORM\Entity()
 */
class CommentImage implements EntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id",type="integer")
     * @ORM\Id()
     * @ORM\GeneratedValue(strategy="IDENTITY")
     * @Serializer\Exclude()
     */
    private $id;

    /**
     * @var ProductReview
     *
     * @ORM\ManyToOne(targetEntity="ProductReview", inversedBy="images")
     * @ORM\JoinColumn(name="comment_id", referencedColumnName="id")
     */
    private $review;

    /**
     * @var string
     *
     * @ORM\Column(name="image", type="string")
     */
    private $filename;

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @return ProductReview
     */
    public function getReview(): ProductReview
    {
        return $this->review;
    }

    /**
     * @param ProductReview $review
     * @return CommentImage
     */
    public function setReview(ProductReview $review): self
    {
        $this->review = $review;

        return $this;
    }

    /**
     * @return string
     */
    public function getFilename(): string
    {
        return $this->filename;
    }

    /**
     * @param string $filename
     * @return CommentImage
     */
    public function setFilename(string $filename): self
    {
        $this->filename = $filename;

        return $this;
    }
}
