<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * UserToken
 *
 * @ORM\Table(name="um_user_tokens")
 * @ORM\HasLifecycleCallbacks()
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserTokenRepository")
 */
class UserToken implements EntityInterface
{

    /**
     * Token types:
     *
     * activate        - account activation
     * change_password - change password request
     * email_verify    - e-mail verifying
     * cookie_auth     - auth through cookie
     */
    public const TYPE_ACTIVATE = 1;
    /**
     *
     */
    public const TYPE_CHANGE_PASSWORD = 2;
    /**
     *
     */
    public const TYPE_EMAIL_VERIFY = 3;
    /**
     *
     */
    public const TYPE_COOKIE_AUTH = 4;


    /**
     * Token statuses:
     *
     * null     - default
     * activate - activated
     * fail     - expired/compromised
     */
    public const STATUS_NEW = 0;
    /**
     *
     */
    public const STATUS_ACTIVATE = 1;
    /**
     *
     */
    public const STATUS_FAIL = 2;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @Serializer\Exclude()
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer")
     * @Serializer\Exclude()
     */
    private $userId;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     * @Serializer\SerializedName("cookie_token")
     */
    private $token;

    /**
     * @var string|null
     *
     * @ORM\Column(name="api_token", nullable=true)
     */
    private $apiToken;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @Serializer\Exclude()
     */
    private $type;

    /**
     * @var int|null
     *
     * @ORM\Column(type="integer", nullable=true)
     * @Serializer\Exclude()
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="create_time", type="datetime")
     * @Serializer\Exclude()
     */
    private $createTime;

    /**
     * @var \DateTime|null
     *
     * @ORM\Column(name="update_time", type="datetime", nullable=true)
     * @Serializer\Exclude()
     */
    private $updateTime;

    /**
     * @var string|null
     *
     * @ORM\Column(nullable=true)
     * @Serializer\Exclude()
     */
    private $ip;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="expire_time", type="datetime")
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $expireTime;

    /**
     * @var
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="tokens")
     * @Serializer\Exclude()
     */
    private $user;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_mobile", type="boolean", nullable=false)
     */
    private $isMobile = true;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string|null $token
     *
     * @return self
     */
    public function setToken(?string $token = null): self
    {
        $this->token = $token;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getToken(): ?string
    {
        return $this->token;
    }

    /**
     * @param string|null $apiToken
     *
     * @return self
     */
    public function setApiToken(?string $apiToken = null): self
    {
        $this->apiToken = $apiToken;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getApiToken(): ?string
    {
        return $this->apiToken;
    }

    /**
     * @param int $type
     *
     * @return self
     */
    public function setType(int $type): self
    {
        $this->type = $type;
        return $this;
    }

    /**
     * @return int
     */
    public function getType(): int
    {
        return $this->type;
    }

    /**
     * @param int|null $status
     *
     * @return self
     */
    public function setStatus(?int $status = null): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getStatus(): ?int
    {
        return $this->status;
    }

    /**
     * @param \DateTime $createTime
     *
     * @return self
     */
    public function setCreateTime(\DateTime $createTime): self
    {
        $this->createTime = $createTime;
        return $this;
    }

    /**
     * @return null|\DateTime
     */
    public function getCreateTime(): ?\DateTime
    {
        return $this->createTime;
    }

    /**
     * @param \DateTime|null $updateTime
     *
     * @return self
     */
    public function setUpdateTime(?\DateTime $updateTime = null): self
    {
        $this->updateTime = $updateTime;
        return $this;
    }

    /**
     * @return \DateTime|null
     */
    public function getUpdateTime(): ?\DateTime
    {
        return $this->updateTime;
    }

    /**
     * @param string|null $ip
     *
     * @return self
     */
    public function setIp(?string $ip = null): self
    {
        $this->ip = $ip;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getIp(): ?string
    {
        return $this->ip;
    }

    /**
     * @param \DateTime $expireTime
     *
     * @return self
     */
    public function setExpireTime(\DateTime $expireTime): self
    {
        $this->expireTime = $expireTime;
        return $this;
    }

    /**
     * @return \DateTime
     */
    public function getExpireTime(): \DateTime
    {
        return $this->expireTime;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     * @return self
     */
    public function setUser(User $user): self
    {
        $this->user = $user;
        $this->setUserId($user->getId());

        return $this;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $userId
     * @return self
     */
    public function setUserId(int $userId): self
    {
        $this->userId = $userId;
        return $this;
    }

    /**
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function updateTimestamps(): void
    {
        $this->setUpdateTime(new \DateTime());

        if ($this->getCreateTime() === null) {
            $this->setCreateTime(new \DateTime());
        }
    }

    /**
     * @return bool
     */
    public function isExpired(): bool
    {
        return $this->getExpireTime() < new \DateTime();
    }

    /**
     * @return bool
     */
    public function isMobile(): bool
    {
        return $this->isMobile;
    }

    /**
     * @param bool $isMobile
     * @return UserToken
     */
    public function setIsMobile(bool $isMobile): UserToken
    {
        $this->isMobile = $isMobile;
        return $this;
    }
}
