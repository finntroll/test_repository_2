<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * PromocodeGift
 *
 * @ORM\Table(name="um_promo_promocode_gift")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PromocodeGiftRepository")
 */
class PromocodeGift
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="promo_id", type="integer", length=11)
     */
    private $promoId;

    /**
     * @var int
     *
     * @ORM\Column(name="user_id", type="integer", length=11)
     */
    private $userId;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="promocodeGifts")
     *
     * @var User
     */
    private $user;

    /**
     * @var int
     *
     * @ORM\Column(name="order_id", type="integer", length=11)
     */
    private $orderId;

    /**
     * @var int|null
     *
     * @ORM\Column(name="promocode_id", type="integer", nullable=true, length=11)
     */
    private $promocodeId;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $promoId
     *
     * @return self
     */
    public function setPromoId(int $promoId): self
    {
        $this->promoId = $promoId;
        return $this;
    }

    /**
     * @return int
     */
    public function getPromoId(): int
    {
        return $this->promoId;
    }

    /**
     * @return int
     */
    public function getUserId(): int
    {
        return $this->userId;
    }

    /**
     * @param int $orderId
     *
     * @return self
     */
    public function setOrderId(int $orderId): self
    {
        $this->orderId = $orderId;
        return $this;
    }

    /**
     * @return int
     */
    public function getOrderId(): int
    {
        return $this->orderId;
    }

    /**
     * @param int|null $promocodeId
     *
     * @return self
     */
    public function setPromocodeId(?int $promocodeId = null): self
    {
        $this->promocodeId = $promocodeId;
        return $this;
    }

    /**
     * @return int|null
     */
    public function getPromocodeId(): ?int
    {
        return $this->promocodeId;
    }

    /**
     * @return User
     */
    public function getUser(): User
    {
        return $this->user;
    }

    /**
     * @param User $user
     *
     * @return self
     */
    public function setUser(User $user): self
    {
        $this->user   = $user;
        $this->userId = $user->getId();
        return $this;
    }

}
