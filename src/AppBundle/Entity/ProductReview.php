<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\ORM\Mapping as ORM;
use Doctrine\ORM\PersistentCollection;
use JMS\Serializer\Annotation as Serializer;
use \DateTime;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * ProductReview
 *
 * @ORM\Table(name="um_comment_comment")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductReviewRepository")
 */
class ProductReview
{

    public const STATUS_ON_REVIEW = 0;
    public const STATUS_APPROVED  = 1;
    public const STATUS_SPAM      = 2;
    public const STATUS_DELETED   = 3;

    public const MIN_RATING = 0;
    public const MAX_RATING = 5;

    /**
     * If rating less this number - don't show it
     */
    public const MIN_SHOW_RATING = 4;

    /**
     * @var int
     *
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User|null
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="reviews")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     * @Serializer\Exclude()
     */
    private $user;

    /**
     * @var Product
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Product", inversedBy="reviews")
     * @ORM\JoinColumn(name="model_id", referencedColumnName="id")
     * @Serializer\Exclude()
     */
    private $product;

    /**
     * @ORM\Column(name="model_id", type="integer")
     * @var int
     */
    private $productId;

    /**
     * @ORM\Column(name="model", type="string")
     * @Serializer\Exclude()
     * @var string
     */
    private $model = 'Product';

    /**
     *
     * @ORM\Column(name="name", type="string")
     * @var string
     */
    private $userName;

    /**
     * @ORM\Column(name="email", type="string")
     * @Serializer\Exclude()
     * @var string
     */
    private $email = '';

    /**
     *
     * @ORM\Column(name="text", type="text")
     * @var string
     */
    private $text = '';

    /**
     * @ORM\Column(name="create_time", type="datetime")
     * @var DateTime;
     * @Serializer\Type("DateTime<'Y-m-d H:i:s'>")
     */
    private $createTime;

    /**
     * @ORM\Column(name="status", type="integer")
     * @var int
     */
    private $status = self::STATUS_ON_REVIEW;

    /**
     * @ORM\Column(name="product_rating", type="integer")
     * @Serializer\SerializedName("rating")
     *
     * @Assert\Range(min="0", max="5")
     * @var int
     */
    private $productRating = 0;

    /**
     * @var string
     *
     * @ORM\Column(name="ip", type="string", nullable=true)
     * @Serializer\Exclude()
     */
    private $ip;

    /**
     * @var ArrayCollection|PersistentCollection
     *
     * @ORM\OneToMany(targetEntity="CommentImage", mappedBy="review", cascade={"persist"})
     */
    private $images;

    /**
     * ProductReview constructor
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;
        return $this;
    }

    /**
     * @return User|null
     */
    public function getUser(): ?User
    {
        return $this->user;
    }

    /**
     * @param User|null $user
     * @return $this
     */
    public function setUser(?User $user): self
    {
        $this->user = $user;
        return $this;
    }

    /**
     * @return Product
     */
    public function getProduct(): Product
    {
        return $this->product;
    }

    /**
     * @param Product $product
     * @return $this
     */
    public function setProduct(Product $product): self
    {
        $this->product   = $product;
        $this->productId = $product->getId();
        return $this;
    }

    /**
     * @return string
     */
    public function getUserName(): string
    {
        return $this->userName;
    }

    /**
     * @param string $userName
     * @return $this
     */
    public function setUserName(string $userName): self
    {
        $this->userName = $userName;
        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @param string $text
     * @return $this
     */
    public function setText(string $text): self
    {
        $this->text = $text;
        return $this;
    }

    /**
     * @return int
     */
    public function getStatus(): int
    {
        return $this->status;
    }

    /**
     * @param int $status
     * @return $this
     */
    public function setStatus(int $status): self
    {
        $this->status = $status;
        return $this;
    }

    /**
     * @return DateTime
     */
    public function getCreateTime(): DateTime
    {
        return $this->createTime;
    }

    /**
     * @param DateTime $createTime
     *
     * @return self
     */
    public function setCreateTime(DateTime $createTime): self
    {
        $this->createTime = $createTime;

        return $this;
    }

    /**
     * @return int
     */
    public function getProductRating(): int
    {
        return $this->productRating;
    }

    /**
     * @param int $productRating
     * @return self
     */
    public function setProductRating(int $productRating): self
    {
        $this->productRating = $productRating;
        return $this;
    }

    /**
     * @return string
     */
    public function getEmail(): string
    {
        return $this->email;
    }

    /**
     * @param string $email
     * @return self
     */
    public function setEmail(string $email): self
    {
        $this->email = $email;
        return $this;
    }

    /**
     * @return int
     */
    public function getProductId(): int
    {
        return $this->productId;
    }

    /**
     * @return bool
     */
    public function getIsApproved(): bool
    {
        return $this->status === self::STATUS_APPROVED;
    }

    /**
     * @return bool
     */
    public function getIsDeleted(): bool
    {
        return $this->status === self::STATUS_SPAM || $this->status === self::STATUS_DELETED;
    }

    /**
     * @return PersistentCollection|ArrayCollection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * @param CommentImage[] $images
     *
     * @return ProductReview
     */
    public function setImages(array $images): self
    {
        $this->images = new ArrayCollection($images);

        return $this;
    }

    /**
     * @param CommentImage $image
     * @return ProductReview
     */
    public function removeImage(CommentImage $image): self
    {
        if ($this->images->contains($image)) {
            $this->images->removeElement($image);
        }

        return $this;
    }

    /**
     * @param CommentImage $image
     * @return ProductReview
     */
    public function addImage(CommentImage $image): self
    {
        if (!$this->images->contains($image)) {
            $this->images->add($image);
        }

        return $this;
    }
}
