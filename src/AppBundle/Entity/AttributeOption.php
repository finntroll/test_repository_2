<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

/**
 * AttributeOption
 *
 * @ORM\Table(name="um_attribute_option")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AttributeOptionRepository")
 */
class AttributeOption implements EntityInterface
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var Attribute
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Attribute", inversedBy="options")
     * @Serializer\Exclude()
     */
    private $attribute;

    /**
     * @var int
     *
     * @ORM\Column(name="attribute_id", type="integer")
     */
    private $attributeId;

    /**
     * @var string|null
     *
     * @ORM\Column(name="value", type="string", length=150, nullable=true)
     */
    private $value;


    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string|null $value
     *
     * @return self
     */
    public function setValue(?string $value = null): self
    {
        $this->value = $value;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getValue(): ?string
    {
        return $this->value;

    }

    /**
     * @return Attribute
     */
    public function getAttribute(): Attribute
    {
        return $this->attribute;
    }

    /**
     * @param Attribute $attribute
     *
     * @return self
     */
    public function setAttribute(Attribute $attribute): self
    {
        $this->attribute = $attribute;
        return $this;
    }

}
