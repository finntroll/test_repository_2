<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use AppBundle\Entity\UserToken;
use AppBundle\Manager\LegacyYiiPasswordManagerAbstract;
use AppBundle\Manager\PasswordManager;
use AppBundle\Repository\UserRepository;
use AppBundle\Service\TokenStorage;

use Doctrine\ORM\{
    EntityManager, EntityManagerInterface, NonUniqueResultException, ORMException
};
use Symfony\Component\HttpKernel\Exception\{
    NotFoundHttpException, UnauthorizedHttpException
};

/**
 * Class AuthService
 *
 * @package AppBundle\Service
 */
class AuthService
{

    public const  MAGIC_YII_STATE_KEY_PREFIX  = '8358fab38c5760c284ec78c65c716522';
    private const YII_DEFAULT_COOKIE_LIFETIME = 604800;

    /**
     * @var PasswordManager
     */
    private $passwordManager;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var EntityManager
     */
    private $em;

    /**
     * UserManager constructor.
     *
     * @param TokenStorage           $tokenStorage
     * @param EntityManagerInterface $em
     * @param PasswordManager        $passwordManager
     */
    public function __construct(
        TokenStorage $tokenStorage,
        EntityManagerInterface $em,
        PasswordManager $passwordManager
    )
    {
        $this->tokenStorage    = $tokenStorage;
        $this->em              = $em;
        $this->passwordManager = $passwordManager;
    }


    /**
     * @param User   $user
     * @param string $siteToken
     * @return string
     */
    public function getCookieValueByToken(User $user, string $siteToken): string
    {
        $data = [
            sprintf('%d', $user->getId()),
            $user->getNickName() ?? '',
            static::YII_DEFAULT_COOKIE_LIFETIME,
            [
                'at' => $siteToken
            ]
        ];

        $value = $this->hashData(serialize($data));
        $value = $this->hashData(serialize($value));
        return self::MAGIC_YII_STATE_KEY_PREFIX . ':' . urlencode($value);
    }

    //TODO по-хорошему бы пассманагера держать тут, и унести из юзер сервиса. Но не сегодня.
    //private $passwordManager;

    /**
     * @param      $data
     * @param null $key
     * @return string
     */
    private function hashData($data, $key = null): string
    {
        try {
            return $this->passwordManager->computeHMAC($data, $key) . $data;
        } catch (\Exception $exception) {
            return base64_encode('legacy_error_from_manager');
        }
    }

    /**
     * @param User $user
     * @return UserToken
     * @throws \RuntimeException
     */
    public function authenticate(User $user): UserToken
    {
        $authToken = $this->tokenStorage->createAuthToken($user);
        $authToken->setToken($this->getCookieValueByToken($user, $authToken->getToken()));
        return $authToken;
    }

}