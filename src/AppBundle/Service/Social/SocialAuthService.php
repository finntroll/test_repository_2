<?php

namespace AppBundle\Service\Social;


use AppBundle\Entity\UserSocial;
use AppBundle\Manager\UserManager;
use AppBundle\Service\Social\Provider\FacebookService;
use AppBundle\Service\Social\Provider\GoogleService;
use AppBundle\Service\Social\Provider\VkService;
use Symfony\Component\HttpKernel\Exception\HttpException;

class SocialAuthService
{
    /** @var SocialProviderInterface[] */
    protected $availableServices = [];
    /**
     * @var UserManager
     */
    private $userManager;

    /**
     * SocialAuthService constructor.
     *
     * @param FacebookService $facebookService
     * @param GoogleService   $googleService
     * @param VkService       $vkService
     * @param UserManager     $userManager
     */
    public function __construct(FacebookService $facebookService, GoogleService $googleService, VkService $vkService, UserManager $userManager)
    {
        $this->availableServices = [
            SocialProviderInterface::FACEBOOK_LOGIN => $facebookService,
            SocialProviderInterface::GOOGLE_LOGIN   => $googleService,
            SocialProviderInterface::VK_LOGIN       => $vkService,
        ];
        $this->userManager       = $userManager;
    }

    /**
     * @param string $providerKey
     * @param string $code
     * @param string|null
     * @return UserSocial|null
     * @throws \Exception
     */
    public function authenticate(string $providerKey, string $code, ?string $email = null): ?UserSocial
    {
        $service = $this->availableServices[$providerKey] ?? null;
        if (null === $service) {
            throw new \Exception('Invalid service name exception');
        }

        $userData = $service->auth($code, $email);
        if (null == $userData) {
            throw new \Exception('Can not get data from provider');
        }

        /** @var UserSocial|null $user */
        $user = null;

        try {
            $user = $this->userManager->findOrCreateBySocialAccount($userData);
        } catch (\Exception $ex) {
            if ($ex instanceof HttpException) {
                throw $ex;
            }
            return null;
        }
        return $user;
    }
}