<?php

namespace AppBundle\Service\Social\DTO;

class UserData
{
    /**
     * UserData constructor.
     *
     * @param $provider
     */
    public function __construct($provider)
    {
        $this->provider = $provider;
    }

    /**
     * @var string
     */
    public $provider;
    /** @var string */
    public $uid;
    /** @var string */
    public $email;
    /** @var string */
    public $firstName;
    /** @var string */
    public $lastName;
}