<?php

namespace AppBundle\Service\Social\Provider;

use AppBundle\Service\Social\DTO\UserData;
use AppBundle\Service\Social\SocialAuthService;
use AppBundle\Service\Social\SocialProviderInterface;
use League\OAuth2\Client\Provider\Facebook;
use League\OAuth2\Client\Provider\FacebookUser;
use League\OAuth2\Client\Token\AccessToken;

/**
 * Class FacebookService
 * https://github.com/thephpleague/oauth2-facebook
 *
 * @package AppBundle\Service\Social\Provider
 */
class FacebookService implements SocialProviderInterface
{
    /** @var Facebook */
    protected $provider;

    /**
     * FacebookService constructor.
     *
     * @param $appId
     * @param $clientSecret
     * @throws \InvalidArgumentException
     */
    public function __construct($appId, $clientSecret)
    {
        $this->provider = new Facebook(
            [
                'clientId'        => $appId,
                'clientSecret'    => $clientSecret,
                'redirectUri'     => 'https://umkamall.ru/social/login/service/facebook',
                'graphApiVersion' => 'v2.10', //expires after 7 NOV 2019
            ]
        );
    }

    /**
     * @param string $token
     * @param null $email
     *
     * @return UserData|null
     * @throws \InvalidArgumentException
     */
    public function auth($token, $email = null): ?UserData
    {

        $accessToken = new AccessToken(['access_token' => $token]);

        try {
            /** @var FacebookUser $user */
            $user = $this->provider->getResourceOwner($accessToken);
            if (null === $user->getEmail() || null === $user->getId()) {
                return null;
            }
            $userData            = new UserData(SocialProviderInterface::FACEBOOK_LOGIN);
            $userData->uid       = $user->getId();
            $userData->email     = $user->getEmail();
            $userData->firstName = $user->getFirstName();
            $userData->lastName  = $user->getLastName();

            return $userData;

        } catch (\Exception $e) {
            return null;
        }
    }
}