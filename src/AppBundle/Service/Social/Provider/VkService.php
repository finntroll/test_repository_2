<?php

namespace AppBundle\Service\Social\Provider;

use AppBundle\Service\Social\DTO\UserData;
use AppBundle\Service\Social\Oauth2Provider\VkontakteDataProvider;
use AppBundle\Service\Social\Oauth2Provider\VkUser;
use AppBundle\Service\Social\SocialProviderInterface;
use League\OAuth2\Client\Token\AccessToken;

/**
 * Class VkService
 *
 * https://github.com/j4k/oauth2-vkontakte честно скопировано отсюда
 *
 * @package AppBundle\Service\Social\Provider
 */
class VkService implements SocialProviderInterface
{
    /** @var VkontakteDataProvider */
    protected $provider;
    /**
     * @var string
     */

    /**
     * VkService constructor.
     *
     * @param string $appId
     * @param string $clientSecret
     */
    public function __construct($appId, $clientSecret)
    {
        $this->provider = new VkontakteDataProvider(
            [
                'clientId'     => $appId,
                'clientSecret' => $clientSecret,
                'redirectUri'  => 'https://umkamall.ru/social/login/service/vkontakte',
                'hostedDomain' => 'https://umkamall.ru',
            ]
        );
    }

    /**
     * @param $token
     * @param string|null $email
     *
     * @return UserData|null
     *
     * @throws \InvalidArgumentException
     */
    public function auth($token, $email): ?UserData
    {
        $accessToken = new AccessToken(['access_token' => $token]);

        try {
            /** @var VkUser $user */
            $user = $this->provider->getResourceOwner($accessToken);
            $user->setEmail($email);

            if (null === $user->getEmail() || null === $user->getId()) {
                return null;
            }

            $userData            = new UserData(SocialProviderInterface::VK_LOGIN);
            $userData->uid       = $user->getId();
            $userData->email     = $user->getEmail();
            $userData->firstName = $user->getFirstName();
            $userData->lastName  = $user->getLastName();

            return $userData;

        } catch (\Exception $e) {
            return null;
        }
    }
}