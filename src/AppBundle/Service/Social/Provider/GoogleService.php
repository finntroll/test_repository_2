<?php

namespace AppBundle\Service\Social\Provider;

use AppBundle\Service\Social\DTO\UserData;
use AppBundle\Service\Social\SocialProviderInterface;
use League\OAuth2\Client\Provider\Google;
use League\OAuth2\Client\Provider\GoogleUser;
use League\OAuth2\Client\Token\AccessToken;

/**
 * Class GoogleService
 *
 * https://github.com/thephpleague/oauth2-google
 * @package AppBundle\Service\Social\Provider
 */
class GoogleService implements SocialProviderInterface
{
    /** @var Google */
    protected $provider;

    /**
     * GoogleService constructor.
     *
     * @param string $appId
     * @param string $clientSecret
     */
    public function __construct($appId, $clientSecret)
    {
        $this->provider = new Google(
            [
                'clientId'     => $appId,
                'clientSecret' => $clientSecret,
            ]
        );

    }

    /**
     * @param string $token
     * @param null   $email
     *
     * @return UserData|null
     * @throws \InvalidArgumentException
     */
    public function auth($token, $email = null): ?UserData
    {
        $accessToken = new AccessToken(['access_token' => $token]);

        try {
            /** @var GoogleUser $user */
            $user = $this->provider->getResourceOwner($accessToken);
            if (null === $user->getEmail() || null === $user->getId()) {
                return null;
            }
            $userData            = new UserData(SocialProviderInterface::GOOGLE_LOGIN);
            $userData->uid       = $user->getId();
            $userData->email     = $user->getEmail();
            $userData->firstName = $user->getFirstName();
            $userData->lastName  = $user->getLastName();

            return $userData;

        } catch (\Exception $e) {
            return null;
        }
    }
}