<?php

namespace AppBundle\Service\Social\Oauth2Provider;

use League\OAuth2\Client\Provider\ResourceOwnerInterface;

/**
 * Class VkUser
 *
 * @package AppBundle\Service\Social\Oauth2Provider
 */
class VkUser implements ResourceOwnerInterface
{
    /**
     * @var array
     */
    protected $response;

    /**
     * @param array $response
     */
    public function __construct(array $response)
    {
        $this->response = $response;
    }

    /**
     * @return mixed
     */
    public function getId()
    {
        return $this->response['id'];
    }

    /**
     * Get preferred first name.
     *
     * @return string
     */
    public function getFirstName(): string
    {
        return $this->response['first_name'];
    }

    /**
     * Get preferred last name.
     *
     * @return string
     */
    public function getLastName(): string
    {
        return $this->response['last_name'];
    }

    /**
     * Get screen name.
     *
     * @return string
     */
    public function getScreenName(): string
    {
        return $this->response['screen_name'];
    }

    /**
     * Get screen name.
     *
     * @return string
     */
    public function getEmail(): ?string
    {
        return $this->response['email'] ?? null;
    }

    /**
     * @param null|string $email
     *
     * @return self
     */
    public function setEmail(?string $email): self
    {
        $this->response['email'] = $email;
        return $this;
    }

    /**
     * Get avatar image URL.
     *
     * @return string|null
     */
    public function getPhotoMedium(): ?string
    {
        if (!empty($this->response['photo_medium'])) {
            return $this->response['photo_medium'];
        }

        return null;
    }

    /**
     * Get user data as an array.
     *
     * @return array
     */
    public function toArray(): array
    {
        return $this->response;
    }

}