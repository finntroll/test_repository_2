<?php

namespace AppBundle\Service\Social;

use AppBundle\Service\Social\DTO\UserData;

/**
 * Interface SocialProviderInterface
 *
 * @package AppBundle\Service\Social
 */
interface SocialProviderInterface
{
    public const FACEBOOK_LOGIN = 'facebook';
    public const GOOGLE_LOGIN   = 'google';
    public const VK_LOGIN       = 'vkontakte';
    /**
     * @param $code
     * @param $email
     *
     * @return UserData|null
     */
    public function auth($code, $email): ?UserData;

}