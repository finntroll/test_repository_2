<?php

namespace AppBundle\Service;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductModel;
use AppBundle\Manager\ImageManager;
use Solarium\Core\Client\Client;
use Solarium\QueryType\Select\Result\Result;

/**
 * Class SolrManager
 *
 * @package AppBundle\Service
 */
class SolrManager
{
    /**
     * @inheritdoc
     */
    private const SORT_START_NUMBER = 1;

    /**
     * @inheritdoc
     */
    public const PRODUCT_DATA = 1;

    /**
     * @inheritdoc
     */
    public const MODEL_DATA = 2;

    /**
     * @var Client $solrClient
     */
    private $solrClient;

    /**
     * @var ImageManager $imageManager
     */
    private $imageManager;

    /**
     * @var int
     */
    private $wordBoost;

    /**
     * @var float
     */
    private $fuzzy;

    /**
     * @param Client            $solrClient
     * @param ImageManager|null $imageManager
     * @param int|null          $wordBoost
     * @param float|null        $fuzzy
     */
    public function __construct(Client $solrClient, ImageManager $imageManager = null, int $wordBoost = null, float $fuzzy = null)
    {
        $this->solrClient   = $solrClient;
        $this->imageManager = $imageManager;
        $this->wordBoost    = $wordBoost;
        $this->fuzzy        = $fuzzy;
    }

    /**
     * @param array $requestBodyData
     * @param int   $limit
     * @param int   $offset
     *
     * @return Result
     */
    public function getCatalogData($requestBodyData, $limit, $offset): Result
    {
        $query = $this->solrClient->createSelect();

        $query->setStart($offset)->setRows($limit);

        foreach ($requestBodyData as $key => $value) {
            switch ($key) {
                case 'sorting':
                    if (isset($value['direction']) && isset($value['param'])) {
                        $solrKey = $this->getSolrKey($value['param']);
                        $query->addSort($solrKey, $value['direction']);
                    } else {
                        foreach ($value as $sortingValue) {
                            $solrKey = $this->getSolrKey($sortingValue['param']);
                            $query->addSort($solrKey, $sortingValue['direction']);
                        }
                    }
                    break;
                case 'attributes':
                    foreach ($value as $params) {
                        $key = $params['id'];
                        switch ($params['type']) {
                            case 'checkbox':
                                $value      = $params['checkbox'];
                                $falseArray = ['false', 'no', 0, '0', 'n', false];

                                if (is_string($value)) {
                                    $value = strtolower($value);
                                }

                                $value = in_array($value, $falseArray, true) ? false : true;

                                $query->createFilterQuery($key)->setQuery('attr_' . $key . ':' . $value);
                                break;
                            case 'between':
                                $value = $params['between'];

                                if (isset($value['from'])) {
                                    $from = (int)$value['from'];
                                } else {
                                    $from = '*';
                                }

                                if (isset($value['to'])) {
                                    $to = (int)$value['to'];
                                } else {
                                    $to = '*';
                                }

                                if ($from !== '*' || $to !== '*') {
                                    $query->createFilterQuery($key)->setQuery('attr_' . $key . ':' . '[' . $from . ' TO ' . $to . ']');
                                }
                                break;
                            case 'option':
                                $value = $params['option'];
                                $query->createFilterQuery($key)->setQuery('attr_' . $key . ':' . '(' . quotemeta($value) . ')');
                                break;
                            case 'options':
                                $value = $params['options'];
                                $query->createFilterQuery($key)->setQuery('attr_' . $key . ':' . '(' . implode(' || ', $value) . ')');
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case 'priceFrom':
                    $solrKey = $this->getSolrKey($key);
                    $query->createFilterQuery($key)->setQuery($solrKey . ':[' . $value . ' TO *]');
                    break;
                case 'priceTo':
                    $solrKey = $this->getSolrKey($key);
                    $query->createFilterQuery($key)->setQuery($solrKey . ':[* TO ' . $value . ']');
                    break;
                case 'inStock':
                    $solrKey   = 'in_stock';
                    $solrValue = (bool)$this->getSolrValue($value);
                    if ($solrValue) {
                        $query->createFilterQuery($key)->setQuery($solrKey . ':' . $solrValue);
                    }
                    break;
                case 'query':
                    $words            = preg_split('#\s+#', trim($value));
                    $solrSearchString = '';

                    foreach ($words as $word) {
                        if (\mb_strlen($word) === 1) {
                            continue;
                        }

                        $solrSearchString .= "{$word}^{$this->wordBoost} {$word}~{$this->fuzzy}^{$this->wordBoost} ";
                    }

                    $query->getEDisMax()->setQueryFields('full_name^1 description^1 producer_name^15  category_name^15 category_chain^5');
                    $query->setQuery(trim($solrSearchString));
                    break;
                case 'isChinaProduct':
                    $solrKey   = 'is_china_product';
                    $solrValue = ((bool)$this->getSolrValue($value)) ? 'true' : 'false';
                    $query->createFilterQuery($key)->setQuery($solrKey . ':' . $solrValue);
                    break;
                default:
                    $solrKey   = $this->getSolrKey($key);
                    $solrValue = $this->getSolrValue($value);
                    $query->createFilterQuery($key)->setQuery($solrKey . ':' . $solrValue);
                    break;
            }
        }
        $query->createFilterQuery('in_stock')->setQuery('in_stock:true');
        $query->createFilterQuery('in_draft_category')->setQuery('in_draft_category:false');
        $query->createFilterQuery('status')->setQuery('status:' . Product::STATUS_ACTIVE);
        $query->createFilterQuery('moderation')->setQuery('promotion_status:' . Product::NOT_DISABLED_ON_PROMOTION);
        $query->createFilterQuery('disabled')->setQuery('moderation_status:' . Product::MODERATION_CONFIRMED);

        return $this->solrClient->select($query);
    }

    /**
     * @param array $requestData
     * @param int   $limit
     * @param int   $offset
     *
     * @return Result
     */
    public function getModelData($requestData, $limit, $offset): Result
    {
        $query = $this->solrClient->createSelect();

        $query->setStart($offset)->setRows($limit);

        foreach ($requestData as $key => $value) {
            switch ($key) {
                case 'sorting':
                    if (isset($value['direction']) && isset($value['param'])) {
                        $solrKey = $this->getSolrKey($value['param']);
                        $query->addSort($solrKey, $value['direction']);
                    } else {
                        foreach ($value as $sortingValue) {
                            $solrKey = $this->getSolrKey($sortingValue['param']);
                            $query->addSort($solrKey, $sortingValue['direction']);
                        }
                    }
                    break;
                case 'attributes':
                    foreach ($value as $params) {
                        $key = $params['id'];
                        switch ($params['type']) {
                            case 'checkbox':
                                $value      = $params['checkbox'];
                                $falseArray = ['false', 'no', 0, '0', 'n', false];

                                if (is_string($value)) {
                                    $value = strtolower($value);
                                }

                                $value = in_array($value, $falseArray, true) ? false : true;

                                $query->createFilterQuery($key)->setQuery('attr_' . $key . ':' . $value);
                                break;
                            case 'between':
                                $value = $params['between'];

                                if (isset($value['from'])) {
                                    $from = (int)$value['from'];
                                } else {
                                    $from = '*';
                                }

                                if (isset($value['to'])) {
                                    $to = (int)$value['to'];
                                } else {
                                    $to = '*';
                                }

                                if ($from !== '*' || $to !== '*') {
                                    $query->createFilterQuery($key)->setQuery('attr_' . $key . ':' . '[' . $from . ' TO ' . $to . ']');
                                }
                                break;
                            case 'option':
                                $value = $params['option'];
                                $query->createFilterQuery($key)->setQuery('attr_' . $key . ':' . '(' . quotemeta($value) . ')');
                                break;
                            case 'options':
                                $value = $params['options'];
                                $query->createFilterQuery($key)->setQuery('attr_' . $key . ':' . '(' . implode(' || ', $value) . ')');
                                break;
                            default:
                                break;
                        }
                    }
                    break;
                case 'priceFrom':
                    $solrKey = $this->getSolrKey($key);
                    $query->createFilterQuery($key)->setQuery($solrKey . ':[' . $value . ' TO *]');
                    break;
                case 'priceTo':
                    $solrKey = $this->getSolrKey($key);
                    $query->createFilterQuery($key)->setQuery($solrKey . ':[* TO ' . $value . ']');
                    break;
                case 'name':
                    $words            = preg_split('#\s+#', $value);
                    $solrSearchString = '';

                    foreach ($words as $word) {
                        if (\mb_strlen($word) === 1) {
                            continue;
                        }

                        $solrSearchString .= "{$word}^{$this->wordBoost} {$word}~{$this->fuzzy}^{$this->wordBoost} ";
                    }

                    $query->getEDisMax()->setQueryFields('name^1 description^1 producer_name^15  category_name^15 category_chain^5');
                    $query->setQuery(trim($solrSearchString));
                    break;
                case 'inStock':
                    $solrKey   = 'in_stock';
                    $solrValue = (bool)$this->getSolrValue($value);
                    if ($solrValue) {
                        $query->createFilterQuery($key)->setQuery($solrKey . ':' . $solrValue);
                    }

                    break;
                case 'isChinaProduct':
                    $solrKey   = 'is_china_product';
                    $solrValue = ((bool)$this->getSolrValue($value)) ? 'true' : 'false';
                    $query->createFilterQuery($key)->setQuery($solrKey . ':' . $solrValue);

                    break;
                default:
                    $solrKey   = $this->getSolrKey($key);
                    $solrValue = $this->getSolrValue($value);
                    $query->createFilterQuery($key)->setQuery($solrKey . ':' . $solrValue);
                    break;
            }
        }

        $query->createFilterQuery('in_draft_category')->setQuery('in_draft_category:false');
        $query->createFilterQuery('status')->setQuery('status:' . ProductModel::STATUS_ACTIVE);
        $query->createFilterQuery('in_stock')->setQuery('in_stock:true');
        $query->createFilterQuery('moderation_status')->setQuery('moderation_status:' . ProductModel::MODERATION_CONFIRMED);

        return $this->solrClient->select($query);
    }

    /**
     * @param string $key
     * @return string
     */
    private function getSolrKey(string $key): string
    {
        switch ($key) {
            case 'category':
                $queryKey = 'category_id_chain';
                break;
            case 'producers':
                $queryKey = 'producer_id';
                break;
            case 'priceFrom':
            case 'priceTo':
            case 'price':
                $queryKey = 'price_total';
                break;
            case 'isChinaProduct':
                $queryKey = 'is_china_product';
                break;
            default:
                $queryKey = $key;
                break;
        }

        return $queryKey;
    }

    /**
     * @param mixed $value
     * @return string
     */
    private function getSolrValue($value): string
    {
        if (\is_array($value)) {
            if ((isset($value['from']) || isset($value['to'])) && \count($value) <= 2) {
                $from = $value['from'] ?? '*';
                $to   = $value['to'] ?? '*';
                return '[' . $from . ' TO ' . $to . ']';
            }
            return '(' . implode(' || ', $value) . ')';
        }
        return $value;
    }

    /**
     * @param Result $solariumData
     * @param int    $dataType
     * @return array
     */
    public function getFormattedProductData($solariumData, $dataType = self::PRODUCT_DATA): array
    {
        $resultData   = [];
        $solariumData = $solariumData->getData()['response']['docs'] ?? [];

        foreach ($solariumData as $elementId => $data) {
            foreach ($data as $dataKey => $dataValue) {
                switch ($dataKey) {
                    case 'id':
                    case 'price':
                    case 'discount_price':
                    case 'is_china_product':
                    case 'products':
                        $resultData[$elementId][$dataKey] = $dataValue;
                        break;
                    case 'full_name':
                        if (self::PRODUCT_DATA == $dataType) {
                            $resultData[$elementId]['name'] = $dataValue[0];
                        }
                        break;
                    case 'name':
                        if (self::MODEL_DATA == $dataType) {
                            $resultData[$elementId]['name'] = $dataValue[0];
                        }
                        break;
                    case 'in_stock':
                        $resultData[$elementId]['in_stock']        = $dataValue;
                        $resultData[$elementId]['is_out_of_stock'] = !(bool)$dataValue;
                        break;
                    case 'image':
                        if (self::MODEL_DATA == $dataType) {
                            $resultData[$elementId]['image_filename'] = $dataValue;
                        } else {
                            if ($this->imageManager) {
                                $imageDir      = $this->imageManager->getCommonDir();
                                $imageThumbDir = $this->imageManager->getCommonThumbDir();
                                $largeSize     = $this->imageManager->getLargeSize();
                                $previewSize   = $this->imageManager->getPreviewSize();

                                if (\array_key_exists('external_id', $data) && $data['is_china_product']) {
                                    $directory = $this->imageManager->getChinaProductDir() . DIRECTORY_SEPARATOR . $data['external_id'] . DIRECTORY_SEPARATOR;
                                } else {
                                    $directory = $this->imageManager->getProductDir() . DIRECTORY_SEPARATOR;
                                }

                                $resultData[$elementId]['images'][] = [
                                    'preview' => $imageThumbDir . $directory . $previewSize . '_' . $dataValue,
                                    'large'   => $imageThumbDir . $directory . $largeSize . '_' . $dataValue,
                                    'full'    => $imageDir . $directory . $dataValue,
                                    'sort'    => self::SORT_START_NUMBER
                                ];
                            } else {
                                $resultData[$elementId]['images'] = [];
                            }
                        }
                        break;
                    default:
                        break;
                }
            }
        }

        return $resultData;
    }

}
