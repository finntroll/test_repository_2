<?php

namespace AppBundle\Service;

use AppBundle\Entity\{
    User, UserToken
};
use AppBundle\Manager\PasswordManager;
use AppBundle\Repository\UserTokenRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class TokenStorage
 * @package AppBundle\Service
 */
class TokenStorage
{

    public const DEFAULT_SESSION_LIFETIME = 604800;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var UserTokenRepository
     */
    private $tokenRepository;

    /**
     * @var PasswordManager
     */
    private $passwordManager;

    /**
     * @var RequestStack
     */
    private $requestStack;

    /**
     * TokenStorage constructor.
     * @param EntityManagerInterface $em
     * @param PasswordManager $passwordManager
     * @param RequestStack $requestStack
     */
    public function __construct(EntityManagerInterface $em, PasswordManager $passwordManager, RequestStack $requestStack)
    {
        $this->em = $em;
        $this->tokenRepository = $this->em->getRepository(UserToken::class);
        $this->passwordManager = $passwordManager;
        $this->requestStack = $requestStack;
    }

    /**
     * @param User $user
     * @param int $expire
     *
     * @return UserToken
     * @throws \RuntimeException
     */
    public function createAuthToken(User $user, $expire = self::DEFAULT_SESSION_LIFETIME): UserToken
    {
        $this->tokenRepository->deleteByUserAndType($user, UserToken::TYPE_COOKIE_AUTH);

        return $this->create($user, $expire, UserToken::TYPE_COOKIE_AUTH);
    }


    /**
     * @param User $user
     * @param int $expire
     * @param int $type
     *
     * @return UserToken
     */
    public function create(User $user, int $expire, int $type): UserToken
    {
        $expireDate = new \DateTime();
        $expireDate->modify('+' . $expire . ' seconds');

        $ip = null;

        if ($request = $this->requestStack->getCurrentRequest()) {
            $ip = $request->getClientIp();
        }

        $token = new UserToken();
        $token->setUser($user)
            ->setType($type)
            ->setToken($this->passwordManager->generateRandomToken())
            ->setApiToken($this->passwordManager->generateRandomToken())
            ->setIp($ip)
            ->setStatus(UserToken::STATUS_NEW)
            ->setExpireTime($expireDate);

        $this->em->persist($token);
        $this->em->flush();

        return $token;
    }

    public function getUsernameByApiToken($accessToken): ?string {
        $qb = $this->tokenRepository->createQueryBuilder('t');
        /** @var UserToken $token */
        $token = $this->tokenRepository->findOneBy(['apiToken' => $accessToken]);
        if (null === $token){
            return null;
        }
        if ($token->isExpired()){
            return null;
        }
        return $token->getUser()->getEmail();
    }

}