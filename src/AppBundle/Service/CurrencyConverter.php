<?php

namespace AppBundle\Service;

use AppBundle\Entity\Rate;
use AppBundle\Parameters\RedisParameter;
use AppBundle\Repository\RateRepository;
use Predis\Client as Predis;
use Symfony\Component\Cache\Simple\RedisCache;

/**
 * Class CurrencyConverter
 *
 * @package AppBundle\Service
 */
class CurrencyConverter
{


    /** @var RedisCache */
    private $cache;

    /** @var RateRepository */
    private $repository;

    /**
     * CurrencyConverter constructor.
     *
     * @param RateRepository $repository
     * @param Predis         $redis
     */
    public function __construct(RateRepository $repository, Predis $redis)
    {
        $this->cache      = new RedisCache($redis);
        $this->repository = $repository;
    }

    /**
     * @param float $value
     *
     * @return float
     */
    public function usdToRub(float $value): float
    {
        return $value * $this->getRate(Rate::RUB);
    }

    /**
     * @param float $value
     * @return float
     */
    public function rubToUsd(float $value): float
    {
        return $value / $this->getRate(Rate::RUB);
    }

    /**
     * @param string $currencyName
     *
     * @return float
     */
    public function getRate(string $currencyName = Rate::RUB): float
    {
        $rate = $this->cache->get(RedisParameter::RATE_PREFIX . $currencyName);

        if ($rate === null) {
            $rateEntity = $this->repository->findOneBy(['currency' => $currencyName]);

            if ($rateEntity === null) {
                throw new \RuntimeException('Wrong rate code.');
            }

            $rate = $rateEntity->getValue();

            $this->cache->set(RedisParameter::RATE_PREFIX . $currencyName, $rate, RedisParameter::RATE_CACHE_TIME_SECONDS);
        }

        return $rate;
    }

}