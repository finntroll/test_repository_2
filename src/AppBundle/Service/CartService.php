<?php

namespace AppBundle\Service;

use AppBundle\Entity\{
    CartProduct, Product, User
};
use AppBundle\Repository\CartProductRepository;
use Doctrine\DBAL\DBALException;

/**
 * Class CartService
 *
 * @package AppBundle\Service
 */
class CartService
{

    /**
     * @var CartProductRepository
     */
    private $repository;

    /**
     * CartService constructor.
     *
     * @param CartProductRepository $repository
     */
    public function __construct(CartProductRepository $repository)
    {
        $this->repository = $repository;
    }

    /**
     * @param User $user
     *
     * @return CartProduct[]
     */
    public function getProductList(User $user): array
    {
        return $this->repository->getCartProductsByUser($user);
    }

    /**
     * @param User    $user
     * @param Product $product
     * @param int     $count
     *
     * @return CartProduct[]
     *
     */
    public function addProduct(User $user, Product $product, int $count = 1): array
    {
        return $this->repository->addProductQuery($user, $product, $count);
    }

    /**
     * @param User    $user
     * @param Product $product
     * @param int     $count
     *
     * @return CartProduct[]
     */
    public function updateProduct(User $user, Product $product, int $count = 1): array
    {
        return $this->repository->updateProductCount($user, $product, $count);
    }

    /**
     * @param User    $user
     * @param Product $productId
     *
     * @return CartProduct[]
     */
    public function deleteProduct(User $user, Product $productId): array
    {
        return $this->repository->deleteByUserAndProductId($user, $productId);
    }

    /**
     * @param User $user
     */
    public function clearUserCart(User $user): void
    {
        $this->repository->deleteByUser($user);
    }

    /**
     * @param CartProduct[] $products
     *
     * @return array
     */
    public function getFormattedCart(array $products)
    {
        return ['items' => $products];
    }
}