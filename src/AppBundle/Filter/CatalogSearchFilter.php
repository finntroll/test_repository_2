<?php

namespace AppBundle\Filter;


class CatalogSearchFilter extends AbstractFilter
{
    /**
     * Build filter by using add() method
     */
    public function buildFilter()
    {
        $this->add('in_stock', 'filterByIsInStock','toBooleanTransformer')
            ->add('price', 'filterByPrice')
            ->add('producers','filterByProducers', 'toInArrayTransformer')
            ->add('attributes','filterByAttributes')
            ->add('sorting', 'filterBySorting');
    }
}