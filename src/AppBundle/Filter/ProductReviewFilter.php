<?php

namespace AppBundle\Filter;

class ProductReviewFilter extends AbstractFilter
{

    /**
     * Build filter by using add() method
     */
    public function buildFilter()
    {
        $this->add('rating', 'filterByRating', 'toIntegerTransformer');
    }
}