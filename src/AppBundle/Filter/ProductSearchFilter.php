<?php

namespace AppBundle\Filter;

/**
 * Class ProductSearchFilter
 *
 * @package AppBundle\Filter
 */
class ProductSearchFilter extends AbstractFilter
{
    /**
     * Build filter by using add() method
     */
    public function buildFilter()
    {
        $this->add('category', 'filterByCategoryId')
            ->add('isChinaProduct', 'filterByIsChinaProduct', 'toBooleanTransformer')
            ->add('inStock', 'filterByIsInStock','toBooleanTransformer')
            ->add('priceFrom','filterByFromPrice')
            ->add('priceTo','filterByToPrice')
            ->add('producers','filterByProducers', 'toInArrayTransformer')
            ->add('attributes','filterByAttributes')
            ->add('name', 'filterByName')
            ->add('query', 'filterByQuery')
            ->add('sorting', 'filterBySorting');
    }
}
