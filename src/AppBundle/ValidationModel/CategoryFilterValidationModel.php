<?php

namespace AppBundle\ValidationModel;

use AppBundle\ValidationModel\Base\AbstractValidationModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class CategoryFilterValidationModel
 *
 * @package AppBundle\Requests
 */
class CategoryFilterValidationModel extends AbstractValidationModel
{
    /**
     * @var bool
     * @Assert\Type(type="bool")
     */
    private $in_stock;

    /**
     * @var
     * @Assert\Collection(fields={
     *     "from" = {
     *          @Assert\Type(type="numeric"),
     *          @Assert\GreaterThanOrEqual(0)
     *     },
     *     "to" = {
     *          @Assert\Type(type="numeric"),
     *          @Assert\GreaterThanOrEqual(0)
     *     }
     * })
     */
    private $price;

    /**
     * @var array
     * @Assert\Type(type="array")
     */
    private $producers;

    /**
     * @var string
     * @Assert\Collection(fields={
     *          "direction" = {
     *              @Assert\Choice(choices={"asc", "desc"})
     *          },
     *          "param" = {
     *              @Assert\Choice(choices={"name", "price", "discount", "id"})
     *          }
     *     })
     */
    private $sorting;

    /**
     * @param Request $request
     * @return CategoryFilterValidationModel
     */
    public static function makeInstance(Request $request): self
    {
        $me   = new static();
        $data = json_decode($request->getContent(), true);
        foreach ($data as $key => $value) {
            $me->$key = $value;
        }

        return $me;
    }
}
