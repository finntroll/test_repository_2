<?php

namespace AppBundle\ValidationModel\Base;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\ConstraintViolationList;

/**
 * Class AbstractValidationModel
 * Базовый класс для всех моделей, используемых для валидации запросов
 *
 * @package AppBundle\Requests\Base
 */
abstract class AbstractValidationModel
{
    /**
     * AbstractRequest constructor.
     *
     * @see AbstractValidationModel::makeInstance()
     */
    protected function __construct()
    {
    }

    /**
     * @param Request $request
     * @return static
     */
    abstract public static function makeInstance(Request $request);

    /**
     * @param ConstraintViolationList $violations
     * @return string
     */
    public function makeErrorString(ConstraintViolationList $violations): string
    {
        $message = '';
        foreach ($violations as $violation) {
            $message .= 'Property "' . $violation->getPropertyPath() . '" has violations. ' . $violation->getMessage() . PHP_EOL;
        }

        return $message;
    }
}
