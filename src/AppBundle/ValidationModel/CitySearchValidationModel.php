<?php

namespace AppBundle\ValidationModel;


use AppBundle\ValidationModel\Base\AbstractValidationModel;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Validator\Constraints as Assert;

class CitySearchValidationModel extends AbstractValidationModel
{
    /**
     * @Assert\Type(type="string")
     */
    private $filter_name;

    /**
     * @Assert\Type(type="integer")
     */
    private $region_id;

    /**
     * @Assert\Type(type="integer")
     */
    private $limit;

    /**
     * @inheritdoc
     */
    public static function makeInstance(Request $request)
    {
        $me = new self();
        $data = json_decode($request->getContent(), true);

        foreach ($data as $key => $value) {
            $me->$key = $value;
        }

        return $me;
    }

}
