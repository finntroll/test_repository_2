<?php

namespace AppBundle\Manager;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductReview;
use AppBundle\Entity\User;
use AppBundle\Repository\ProductReviewRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ReviewManager
 *
 * @package AppBundle\Manager
 */
class ReviewManager extends AbstractManager
{

    /**
     * ReviewManager constructor.
     *
     * @param ProductReviewRepository $repository
     * @param EntityManagerInterface  $em
     */
    public function __construct(ProductReviewRepository $repository, EntityManagerInterface $em)
    {
        parent::__construct($em);

        $this->repository = $repository;
    }

    /**
     * @param Product $product
     * @param User    $user
     *
     * @return ProductReview
     */
    public function createReview(Product $product, User $user): ProductReview
    {
        $review = new ProductReview();
        $review->setProduct($product)
            ->setUser($user)
            ->setUserName($user->getFirstName())
            ->setEmail($user->getEmail())
            ->setCreateTime(new \DateTime());

        return $review;
    }

    /**
     * @param ProductReview $review
     */
    public function deleteReview(ProductReview $review): void
    {
        $review->setStatus($review::STATUS_DELETED);
        $this->em->persist($review);
        $this->em->flush();
    }

}
