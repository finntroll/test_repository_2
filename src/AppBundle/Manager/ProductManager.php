<?php

namespace AppBundle\Manager;

use AppBundle\Entity\{
    Category, Product, ProductCategory, ProductReview, Rate, User
};
use AppBundle\Filter\FilterInterface;
use AppBundle\Repository\ProductCategoryRepository;
use AppBundle\Repository\ProductRepository;
use AppBundle\Service\CurrencyConverter;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\QueryBuilder;

/**
 * Class ProductManager
 *
 * @package AppBundle\Manager
 */
class ProductManager extends AbstractManager
{
    /**
     * @var CurrencyConverter
     */
    private $currencyConverter;

    /**
     * @var ProductCategoryRepository
     */
    private $productCategoryRepository;

    /**
     * @var bool
     */
    private $extraChargeEnabled;

    /**
     * @var float
     */
    private $extraChargePercent;

    /**
     * ProductManager constructor.
     *
     * @param ProductRepository         $repository
     * @param CurrencyConverter         $currencyConverter
     * @param EntityManagerInterface    $em
     * @param ProductCategoryRepository $productCategoryRepository
     * @param bool                      $extraChargeEnabled
     * @param float                     $extraChargePercent
     */
    public function __construct(
        ProductRepository $repository,
        CurrencyConverter $currencyConverter,
        EntityManagerInterface $em,
        ProductCategoryRepository $productCategoryRepository,
        bool $extraChargeEnabled = true,
        float $extraChargePercent = Rate::DEFAULT_EXTRA_CHARGE_PERCENT
    )
    {
        parent::__construct($em);
        $this->repository                = $repository;
        $this->currencyConverter         = $currencyConverter;
        $this->productCategoryRepository = $productCategoryRepository;
        $this->extraChargeEnabled        = $extraChargeEnabled;
        $this->extraChargePercent        = $extraChargePercent;
    }

    /**
     * @param Product $product
     *
     * @return float
     */
    public function getPrice(Product $product): float
    {
        if ($product->getDiscountPrice()) {
            $value = $this->getChargedDiscountPrice($product);
        } else {
            $extraCharge = $this->getExtraChargeByProduct($product);
            $value       = $product->getPrice() * (1 - ($product->getDiscount() ?: 0) / 100) * (1 + $extraCharge / 100);
        }

        return round($value, 2);
    }

    /**
     * @param Product $product
     *
     * @return float
     */
    public function getPriceRub(Product $product): float
    {
        $value = $this->getPrice($product);
        $value = $this->currencyConverter->usdToRub($value);

        return round($value, -1) ?: 10;
    }

    /**
     * @param Product $product
     *
     * @return float|null
     */
    public function getDiscountPriceRub(Product $product): ?float
    {
        if (!$product->getDiscountPrice()) {
            return null;
        }

        $value = $this->currencyConverter->usdToRub($this->getChargedDiscountPrice($product));

        return round($value, -1);
    }

    /**
     * @param Product $product
     * @return float|null
     */
    public function getChargedDiscountPrice(Product $product): ?float
    {
        if (!$product->getDiscountPrice()) {
            return null;
        }

        $extraCharge = $this->getExtraChargeByProduct($product);
        return $product->getDiscountPrice() * (1 + $extraCharge / 100);
    }

    /**
     * @param Product $product
     *
     * @return float
     */
    public function getBasePriceRub(Product $product): float
    {
        $value = $this->currencyConverter->usdToRub($this->getChargedBasePrice($product));

        return round($value, -1) ?: 10;
    }

    /**
     * @param Product $product
     * @return float
     */
    public function getChargedBasePrice(Product $product): float
    {
        $extraCharge = $this->getExtraChargeByProduct($product);

        return $product->getPrice() * (1 + $extraCharge / 100);
    }

    /**
     * @param Product $product
     * @return float
     */
    public function getExtraChargeByProduct(Product $product): float
    {
        if (!$this->extraChargeEnabled) {
            return 0.0;
        }

        return $product->getExtraCharge() ?? $this->extraChargePercent;
    }

    /**
     * @param FilterInterface|null $filter
     *
     * @return QueryBuilder
     */
    public function queryApprovedByFilter(FilterInterface $filter = null): QueryBuilder
    {
        return $this->repository->queryApprovedByFilter($filter);
    }

    /**
     * @param Category             $category
     * @param FilterInterface|null $filter
     *
     * @return QueryBuilder
     */
    public function getProductQueryByCategoryWithAdditional(Category $category, FilterInterface $filter = null): QueryBuilder
    {
        $productCategoryInAdditionalCategory = $this->productCategoryRepository->getProductsByCategory($category);

        $products = array_map(function ($productCategory) {
            /** @var ProductCategory $productCategory */
            return $productCategory->getProduct();
        }, $productCategoryInAdditionalCategory);

        return $this->repository->getProductQueryByCategoryWithAdditional($category, $products, $filter);
    }
}
