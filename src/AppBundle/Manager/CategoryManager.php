<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Category;
use AppBundle\Parameters\RedisParameter;
use AppBundle\Repository\{
    AttributeRepository, CategoryRepository, ProducerRepository, ProductRepository
};
use Doctrine\ORM\EntityManagerInterface;
use Predis\Client as Predis;
use Symfony\Component\Cache\Simple\RedisCache;

/**
 * Class CategoryManager
 *
 * @package AppBundle\Manager
 */
class CategoryManager extends AbstractManager
{

    /** @var ProductRepository */
    private $productRepository;

    /** @var AttributeRepository */
    private $attributeRepository;

    /** @var RedisCache */
    private $cache;

    /**
     * CategoryManager constructor.
     *
     * @param CategoryRepository     $repository
     * @param ProductRepository      $productRepository
     * @param AttributeRepository    $attributeRepository
     * @param EntityManagerInterface $em
     * @param Predis                 $redis
     */
    public function __construct(
        CategoryRepository $repository,
        ProductRepository $productRepository,
        AttributeRepository $attributeRepository,
        EntityManagerInterface $em,
        Predis $redis
    )
    {
        parent::__construct($em);

        $this->repository          = $repository;
        $this->productRepository   = $productRepository;
        $this->attributeRepository = $attributeRepository;
        $this->cache = new RedisCache($redis);
    }

    /**
     * Getting categories list with producer ids and attribute ids
     *
     * @return array
     */
    public function getCategoriesIdsWithAddFields(): array
    {
        $data = [];
        $categoryList = $this->repository->getList();

        foreach ($categoryList as $category) {
            $data[$category->getId()] = $this->getAddFieldsByCategory($category);
        }

        return $data;
    }

    /**
     * @return array
     */
    public function getCategoriesWithAddFieldsRedis(): array
    {
        return $this->cache->get(RedisParameter::CACHE_CATEGORIES_WITH_ADD_NAME, []);
    }

    /**
     * Getting producers and attributes for one category
     *
     * @param Category $category
     * @return array
     */
    private function getAddFieldsByCategory(Category $category): array
    {
        $productIds = $this->productRepository->getAllProductIdByCategoryId($category->getId());

        $producerIds = array_map(function ($item) {
            return (int)$item['producerId'];
        }, $this->productRepository->getProducerIdsByProductIds($productIds));

        $attributeIds = array_map(function ($item) {
            return (int)$item['attributeId'];
        }, $this->attributeRepository->getIdsByProductIds($productIds));

        return [
            'attributes'  => $attributeIds,
            'producers' => $producerIds
        ];
    }


}