<?php

namespace AppBundle\Manager;

use AppBundle\Entity\OrderPayment;
use AppBundle\Entity\UserOrder;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PaymentTransactionManager
 *
 * @package AppBundle\Manager
 */
class PaymentTransactionManager extends AbstractManager
{
    /**
     * PaymentTransactionManager constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        parent::__construct($em);
    }

    /**
     * @param UserOrder $userOrder
     * @param array     $price
     * @return OrderPayment
     */
    public function createPaymentTransaction($userOrder, $price): OrderPayment
    {
        $time = new \DateTime();

        $paymentTransaction = new OrderPayment();

        $paymentTransaction->setNumber($userOrder->getId() . '-1');
        $paymentTransaction->setType(OrderPayment::TYPE_ORDER_PAYMENT);
        $paymentTransaction->setSum($price['total']);
        $paymentTransaction->setStatus(OrderPayment::STATUS_NEW);
        $paymentTransaction->setOrder($userOrder);
        $paymentTransaction->setTransactionCreated($time);

        $this->em->persist($paymentTransaction);

        return $paymentTransaction;
    }
}