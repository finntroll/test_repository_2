<?php

namespace AppBundle\Manager;

use AppBundle\Entity\OrderProduct;
use AppBundle\Entity\Product;
use AppBundle\Entity\Rate;
use AppBundle\Entity\UserOrder;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class OrderProductManager
 *
 * @package AppBundle\Manager
 */
class OrderProductManager extends AbstractManager
{
    /**
     * @var ProductManager
     */
    private $productManager;

    /**
     * OrderProductManager constructor.
     *
     * @param EntityManagerInterface $em
     * @param ProductManager         $productManager
     */
    public function __construct(ProductManager $productManager, EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->productManager = $productManager;
    }

    /**
     * @param array $products
     * @param UserOrder $userOrder
     * @return array
     */
    public function createOrderProductsReturnPrice(array $products, UserOrder $userOrder): array
    {
        $totalPrice     = 0;
        $totalPriceBase = 0;

        foreach ($products as $productArray) {
            /**
             * @var Product $product
             */
            $product = $productArray['product'];

            $quantity          = (int)$productArray['count'];
            $infoDiscountPrice = $product->getDiscountPrice() ?: null;
            $infoDiscount      = $product->getDiscount() ?: null;
            $infoPrice         = $product->getPrice();
            $extraCharge       = $this->productManager->getExtraChargeByProduct($product);

            $baseCurrencyPrice = $this->productManager->getPrice($product);
            $price             = $this->productManager->getPriceRub($product);

            $orderProduct = new OrderProduct();

            $orderProduct->setQuantity($quantity);
            $orderProduct->setOrder($userOrder);
            $orderProduct->setProduct($product);
            $orderProduct->setProductName($product->getName());
            $orderProduct->setSku($product->getSku());
            $orderProduct->setPrice($price);
            $orderProduct->setBaseCurrencyPrice($baseCurrencyPrice);
            $orderProduct->setInfoPrice($infoPrice);
            $orderProduct->setInfoDiscountPrice($infoDiscountPrice);
            $orderProduct->setInfoDiscount($infoDiscount);
            $orderProduct->setInfoExtraCharge($extraCharge);
            $orderProduct->setIsChinaProduct($product->isChinaProduct());
            $userOrder->addOrderProduct($orderProduct);

            if ((int)$product->getQuantity() === (int)$quantity) {
                $product->setInStock(false);
            }
            $product->setQuantity($product->getQuantity() - $quantity);

            if ($quantity !== 1) {
                $baseCurrencyPrice = $baseCurrencyPrice * $quantity;
                $price             = $price * $quantity;
            }

            $totalPriceBase += $baseCurrencyPrice;
            $totalPrice     += $price;

            $this->em->persist($product);
            $this->em->persist($orderProduct);
        }

        return ['total' => $totalPrice, 'base' => $totalPriceBase];
    }
}