<?php

namespace AppBundle\Manager;

use AppBundle\Entity\User;

/**
 * Class PasswordManager
 *
 * @package AppBundle\Service
 */
class PasswordManager extends LegacyYiiPasswordManagerAbstract
{
    /**
     * @param User   $user
     * @param string $password
     * @return bool
     */
    public function isPassCorrectForUser(User $user, string $password): bool
    {
        return $this->checkPassword($password, $user->getHash());
    }
}