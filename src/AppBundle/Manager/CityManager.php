<?php

namespace AppBundle\Manager;

use AppBundle\Repository\CityRepository;

/**
 * Class CityManager
 *
 * @package AppBundle\Manager
 */
class CityManager extends AbstractManager
{

    /**
     * CityManager constructor.
     * @param CityRepository $repository
     */
    public function __construct(CityRepository $repository)
    {
        $this->repository = $repository;
    }

}