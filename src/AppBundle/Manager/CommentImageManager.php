<?php

namespace AppBundle\Manager;

use Gumlet\ImageResize;
use Psr\Log\LoggerInterface;
use Gumlet\ImageResizeException;
use Symfony\Bridge\Monolog\Logger;
use AppBundle\Document\CommentImage;
use Doctrine\ODM\MongoDB\DocumentManager;
use MongoDB\GridFS\Exception\CorruptFileException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class CommentImageManager
 *
 * @package AppBundle\Manager
 */
class CommentImageManager
{
    private const DEFAULT_MAX_IMAGE_SIDE_LENGTH = 800;

    private const MAX_LENGTH_PARAM = 'max_comment_image_side';

    private const UPLOAD_FAILED_MESSAGE = 'Image upload failed';

    /**
     * @var DocumentManager
     */
    private $documentManager;

    /**
     * @var int
     */
    private $maxImageSizeLength;

    /**
     * @var \MongoDB\GridFS\Bucket
     */
    private $bucket;

    /**
     * @var Logger
     */
    private $logger;

    /**
     * CommentImageManager constructor.
     *
     * @param DocumentManager    $documentManager
     * @param ContainerInterface $container
     * @param LoggerInterface    $logger
     */
    public function __construct(DocumentManager $documentManager, ContainerInterface $container, LoggerInterface $logger)
    {
        $this->documentManager    = $documentManager;
        $this->maxImageSizeLength = $container->hasParameter(self::MAX_LENGTH_PARAM)
            ? $container->getParameter(self::MAX_LENGTH_PARAM)
            : self::DEFAULT_MAX_IMAGE_SIDE_LENGTH;

        $this->logger             = $logger;
        $this->bucket             = $this->documentManager
            ->getDocumentDatabase(CommentImage::class)
            ->getMongoDB()
            ->getDb()
            ->selectGridFSBucket();
    }

    /**
     * Uploads image to MongoDB and returns new file name
     *
     * @param UploadedFile $file
     * @param bool         $resize
     *
     * @return string
     * @throws \Gumlet\ImageResizeException
     * @throws \MongoResultException
     * @throws \InvalidArgumentException
     */
    public function uploadImage(UploadedFile $file, bool $resize): string
    {
        $upload = new CommentImage();
        $upload->setMimeType($file->getClientMimeType());
        $filename = $file->getPathname();

        if (!$upload->isValidImage()) {
            throw new \InvalidArgumentException(
                sprintf('Invalid file type "%s". Only JPEG or PNG allowed', $upload->getMimeType())
            );
        }

        if ($resize) {
            $filename = $this->resizeImage($file);
        }

        try {
            $upload->setFile($filename)->generateName();
        } catch (\InvalidArgumentException $e) {
            $this->logger->error(sprintf('Upload failed: %s', $e->getMessage()));

            throw new \MongoResultException(self::UPLOAD_FAILED_MESSAGE);
        }

        if ($this->findImage($upload) === null) {
            $this->saveInDatabase($upload);
        }

        return $upload->getFilename();
    }

    /**
     * @param CommentImage $upload
     * @return null|CommentImage
     */
    private function findImage(CommentImage $upload): ?CommentImage
    {
        /** @var CommentImage $mongoRecord */
        $mongoRecord = $this->documentManager
            ->getRepository(CommentImage::class)
            ->findOneBy([
                            'filename' => $upload->getFilename()
                        ]);

        if (!$mongoRecord) {
            return null;
        }

        try {
            $this->bucket->openDownloadStreamByName($upload->getFilename());
        } catch (CorruptFileException $exception) {
            return null;
        }

        return $mongoRecord;
    }

    /**
     * @param UploadedFile $file
     *
     * @return string
     *
     * @throws ImageResizeException
     */
    private function resizeImage(UploadedFile $file): string
    {
        try {
            $resized = $file->getPathname() . '_resized';
            $img     = new ImageResize($file->getPathname());
            $img->resizeToLongSide($this->maxImageSizeLength)->save($resized);
        } catch (ImageResizeException $e) {
            $this->logger->error(
                sprintf('Image resize failed: %s', $e->getMessage())
            );

            throw new ImageResizeException(self::UPLOAD_FAILED_MESSAGE);
        }

        return $resized;
    }

    /**
     * @param CommentImage $upload
     * @throws \MongoResultException
     */
    private function saveInDatabase(CommentImage $upload): void
    {
        $this->documentManager->persist($upload);
        $this->documentManager->flush();

        //При успешном сохранении в модель пишется ID
        if ($upload->getId() === null) {
            $this->logger->error('Image saving failed (MongoDB)');
            throw new \MongoResultException(self::UPLOAD_FAILED_MESSAGE);
        }

        try {
            //Если что-то пошло не так - будет что-то типа
            //Expected chunk to have size "261120" but found "34819"
            $this->bucket->openDownloadStreamByName($upload->getFilename());
        } catch (CorruptFileException $exception) {
            $this->logger->error(sprintf('Image saving failed (GridFS): %s', $exception->getMessage()));

            throw new \MongoResultException(self::UPLOAD_FAILED_MESSAGE);
        }
    }

    /**
     * @param string $filename
     * @return bool
     */
    public function imageExists(string $filename): bool
    {
        $document = $this->documentManager
            ->getRepository(CommentImage::class)
            ->findOneBy(['filename' => str_replace(['.jpg', '.png', '.jpeg'], '', $filename)]);

        return $document !== null;
    }
}
