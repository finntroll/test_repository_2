<?php

namespace AppBundle\Manager;

use RuntimeException;

/**
 * Кусок мерзкого легаси из старого проекта
 * Class PasswordManager
 *
 * @package AppBundle\Service
 */
class LegacyYiiPasswordManagerAbstract
{
    
    /**
     * @var string
     */
    protected $yiiValidationSecret;

    /**
     * @var bool
     */
    protected $mbstring;


    /**
     * PasswordManager constructor.
     *
     * @param string $yiiValidationSecret
     */
    public function __construct(string $yiiValidationSecret = '')
    {
        $this->yiiValidationSecret = $yiiValidationSecret;
        $this->mbstring            = \extension_loaded('mbstring');
    }

    /**
     * @param int  $length
     * @param bool $cryptographicallyStrong
     *
     * @return bool|string
     * @throws \RuntimeException
     */
    public function generateRandomToken($length = 32, $cryptographicallyStrong = true)
    {
        if (($randomBytes = $this->generateRandomBytes($length + 2, $cryptographicallyStrong)) !== false) {
            return strtr($this->substr(base64_encode($randomBytes), 0, $length), ['+' => '_', '/' => '~']);
        }
        return false;
    }

    /**
     * Verify a password against a hash.
     *
     * @param string $password The password to verify.
     * @param string $hash     he hash to verify the password against.
     * @return bool
     */
    public function checkPassword(string $password, string $hash): bool
    {
        if (!\is_string($password) || $password === '') {
            return false;
        }

        if (!$password
            || !preg_match('{^\$2[axy]\$(\d\d)\$[\./0-9A-Za-z]{22}}', $hash, $matches)
            || $matches[1] < 4 || $matches[1] > 31) {
            return false;
        }

        $test = crypt($password, $hash);
        if (!\is_string($test) || \strlen($test) < 32) {
            return false;
        }


        return $this->same($test, $hash);
    }

    /**
     *
     *  Check for sameness of two strings using an algorithm with timing
     * independent of the string values if the subject strings are of equal length.
     *
     * The function can be useful to prevent timing attacks. For example, if $a and $b
     * are both hash values from the same algorithm, then the timing of this function
     * does not reveal whether or not there is a match.
     *
     * NOTE: timing is affected if $a and $b are different lengths or either is not a
     * string. For the purpose of checking password hash this does not reveal information
     * useful to an attacker.
     *
     * @param string $a First subject string to compare.
     * @param string $b Second subject string to compare.
     *
     * @return bool
     */
    public function same($a, $b): ?bool
    {
        if (!\is_string($a) || !\is_string($b)) {
            return false;
        }

        $mb     = \function_exists('mb_strlen');
        $length = $mb ? \mb_strlen($a, '8bit') : \strlen($a);
        if ($length !== ($mb ? \mb_strlen($b, '8bit') : \strlen($b))) {
            return false;
        }

        $check = 0;

        for ($i = 0; $i < $length; ++$i) {
            $check |= (\ord($a[$i]) ^ \ord($b[$i]));
        }

        return $check === 0;
    }

    /**
     * Generates a string of random bytes.
     *
     * @param integer $length                  number of random bytes to be generated.
     * @param boolean $cryptographicallyStrong whether to fail if a cryptographically strong
     *                                         result cannot be generated. The method attempts to read from a cryptographically strong
     *                                         pseudorandom number generator (CS-PRNG), see
     *                                         {@link https://en.wikipedia.org/wiki/Cryptographically_secure_pseudorandom_number_generator#Requirements Wikipedia}.
     *                                         However, in some runtime environments, PHP has no access to a CS-PRNG, in which case
     *                                         the method returns false if $cryptographicallyStrong is true. When $cryptographicallyStrong is false,
     *                                         the method always returns a pseudorandom result but may fall back to using {@link generatePseudoRandomBlock}.
     *                                         This method does not guarantee that entropy, from sources external to the CS-PRNG, was mixed into
     *                                         the CS-PRNG state between each successive call. The caller can therefore expect non-blocking
     *                                         behavior, unlike, for example, reading from /dev/random on Linux, see
     *                                         {@link http://eprint.iacr.org/2006/086.pdf Gutterman et al 2006}.
     * @return boolean|string generated random binary string or false on failure.
     * @throws \RuntimeException
     */
    public function generateRandomBytes($length, $cryptographicallyStrong = true)
    {
        $bytes = '';
        if (\function_exists('openssl_random_pseudo_bytes')) {
            $bytes = openssl_random_pseudo_bytes($length, $strong);
            if ($bytes === false) {
                throw new RuntimeException('Crypt generation failed');
            }
            if ($this->strlen($bytes) >= $length && ($strong || !$cryptographicallyStrong)) {
                return $this->substr($bytes, 0, $length);
            }
        }

        if (\function_exists('mcrypt_create_iv') &&
            ($bytes = @mcrypt_create_iv($length, MCRYPT_DEV_URANDOM)) !== false &&
            $this->strlen($bytes) >= $length) {
            return $this->substr($bytes, 0, $length);
        }

        if (($file = @fopen('/dev/urandom', 'rb')) !== false &&
            ($bytes = @fread($file, $length)) !== false &&
            (fclose($file) || true) &&
            $this->strlen($bytes) >= $length) {
            return $this->substr($bytes, 0, $length);
        }

        $i = 0;
        while ($this->strlen($bytes) < $length &&
            ($byte = $this->generateSessionRandomBlock()) !== false &&
            ++$i < 3) {
            $bytes .= $byte;
        }
        if ($this->strlen($bytes) >= $length) {
            return $this->substr($bytes, 0, $length);
        }

        if ($cryptographicallyStrong) {
            return false;
        }

        while ($this->strlen($bytes) < $length) {
            $bytes .= $this->generatePseudoRandomBlock();
        }

        return $this->substr($bytes, 0, $length);
    }

    /**
     * Get random bytes from the system entropy source via PHP session manager.
     *
     * @return boolean|string 20-byte random binary string or false on error.
     */
    public function generateSessionRandomBlock()
    {
        ini_set('session.entropy_length', 20);
        if (ini_get('session.entropy_length') !== 20) {
            return false;
        }

        // These calls are (supposed to be, according to PHP manual) safe even if
        // there is already an active session for the calling script.
        @session_start();
        @session_regenerate_id();

        $bytes = session_id();
        if (!$bytes) {
            return false;
        }

        // $bytes has 20 bytes of entropy but the session manager converts the binary
        // random bytes into something readable. We have to convert that back.
        // SHA-1 should do it without losing entropy.
        return sha1($bytes, true);
    }

    /**
     * Generate a pseudo random block of data using several sources. On some systems this may be a bit
     * better than PHP's {@link mt_rand} built-in function, which is not really random.
     *
     * @return string of 64 pseudo random bytes.
     */
    public function generatePseudoRandomBlock(): string
    {
        $bytes = '';

        if (\function_exists('openssl_random_pseudo_bytes')
            && ($bytes = openssl_random_pseudo_bytes(512)) !== false
            && $this->strlen($bytes) >= 512) {
            return $this->substr($bytes, 0, 512);
        }

        for ($i = 0; $i < 32; ++$i) {
            $bytes .= pack('S', mt_rand(0, 0xffff));
        }

        // On UNIX and UNIX-like operating systems the numerical values in `ps`, `uptime` and `iostat`
        // ought to be fairly unpredictable. Gather the non-zero digits from those.
        foreach (['ps', 'uptime', 'iostat'] as $command) {
            @exec($command, $commandResult, $retVal);
            if (\is_array($commandResult) && !empty($commandResult) && $retVal === 0) {
                $bytes .= preg_replace('/[^1-9]/', '', implode('', $commandResult));
            }
        }

        // Gather the current time's microsecond part. Note: this is only a source of entropy on
        // the first call! If multiple calls are made, the entropy is only as much as the
        // randomness in the time between calls.
        $bytes .= $this->substr(microtime(), 2, 6);

        // Concatenate everything gathered, mix it with sha512. hash() is part of PHP core and
        // enabled by default but it can be disabled at compile time but we ignore that possibility here.
        return hash('sha512', $bytes, true);
    }

    /**
     *
     * @throws \RuntimeException
     */
    public function checkBlowfish(): void
    {
        if (!\function_exists('crypt')) {
            throw new RuntimeException(__CLASS__ . ' requires the PHP crypt() function. This system doesnt\'t have it.');
        }

        if (!\defined('CRYPT_BLOWFISH') || !CRYPT_BLOWFISH) {
            throw new RuntimeException(__CLASS__ . ' requires the Blowfish option of the PHP crypt() function. This system doesn\'t have it');
        }
    }

    /**
     * Returns the portion of string specified by the start and length parameters.
     * If available uses the multibyte string function mb_substr
     *
     * @param string  $string the input string. Must be one character or longer.
     * @param integer $start  the starting position
     * @param integer $length the desired portion length
     * @return string the extracted part of string, or FALSE on failure or an empty string.
     */
    private function substr($string, $start, $length): string
    {
        return $this->mbstring ? mb_substr($string, $start, $length, '8bit') : substr($string, $start, $length);
    }

    /**
     * Returns the length of the given string.
     * If available uses the multibyte string function mb_strlen.
     *
     * @param string $string the string being measured for length
     * @return integer the length of the string
     */
    private function strlen($string): int
    {
        return $this->mbstring ? mb_strlen($string, '8bit') : \strlen($string);
    }


    /**
     * Sorry, guys. Taken from Yii project as is.
     *
     * @param      $data
     * @param null $key
     * @param null $hashAlgorithm
     * @return string
     * @throws \Exception
     */
    public function computeHMAC($data, $key = null, $hashAlgorithm = null): string
    {
        if (null === $key) {
            $key = $this->yiiValidationSecret;
        }
        if ($hashAlgorithm === null) {
            $hashAlgorithm = 'sha1';
        }

        if (function_exists('hash_hmac'))
            return hash_hmac($hashAlgorithm, $data, $key);

        if (0 === strcasecmp($hashAlgorithm, 'sha1')) {
            $pack = 'H40';
            $func = 'sha1';
        } elseif (0 === strcasecmp($hashAlgorithm, 'md5')) {
            $pack = 'H32';
            $func = 'md5';
        } else {
            throw new \RuntimeException('Only SHA1 and MD5 hashing algorithms are supported when using PHP 5.1.1 or below.');
        }
        if ($this->strlen($key) > 64) {
            $key = pack($pack, $func($key));
        }
        if ($this->strlen($key) < 64) {
            $key = str_pad($key, 64, chr(0));
        }
        $key = $this->substr($key, 0, 64);
        return $func((str_repeat(chr(0x5C), 64) ^ $key) . pack($pack, $func((str_repeat(chr(0x36), 64) ^ $key) . $data)));
    }
}