<?php

namespace AppBundle\Manager;

use AppBundle\Repository\ProductRepository;
use AppBundle\Repository\ShowcaseRepository;
use Doctrine\ORM\Query;

/**
 * Class ShowcaseManager
 * @package AppBundle\Manager
 */
class ShowcaseManager
{

    /**
     * @var ShowcaseRepository
     */
    private $showcaseRepository;

    /**
     * @var ProductRepository
     */
    private $productRepository;

    /**
     * ShowcaseManager constructor.
     * @param ShowcaseRepository $showcaseRepository
     * @param ProductRepository $productRepository
     */
    public function __construct(ShowcaseRepository $showcaseRepository, ProductRepository $productRepository)
    {
        $this->showcaseRepository = $showcaseRepository;
        $this->productRepository  = $productRepository;
    }

    /**
     * @param int $id
     * @return Query
     */
    public function getProductsQueryById(int $id): Query
    {
        $showcases = $this->showcaseRepository->getShowcasesById($id);

        $productIds = array_column($showcases, 'productId');

        return $this->productRepository->getProductQueryByIdList($productIds);
    }


}