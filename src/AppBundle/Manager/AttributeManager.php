<?php

namespace AppBundle\Manager;

use AppBundle\Repository\AttributeRepository;

/**
 * Class AttributeManager
 * @package AppBundle\Manager
 */
class AttributeManager extends AbstractManager
{

    /**
     * AttributeManager constructor.
     * @param AttributeRepository $repository
     */
    public function __construct(AttributeRepository $repository)
    {
        $this->repository = $repository;
    }

}