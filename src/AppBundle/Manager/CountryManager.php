<?php

namespace AppBundle\Manager;

use AppBundle\Repository\CountryRepository;

/**
 * Class CountryManager
 * @package AppBundle\Manager
 */
class CountryManager extends AbstractManager
{

    /**
     * CountryManager constructor.
     * @param CountryRepository $repository
     */
    public function __construct(CountryRepository $repository)
    {
        $this->repository = $repository;
    }

}