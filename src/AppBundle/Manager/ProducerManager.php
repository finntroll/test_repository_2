<?php

namespace AppBundle\Manager;

use AppBundle\Repository\ProducerRepository;

/**
 * Class ProducerManager
 * @package AppBundle\Manager
 */
class ProducerManager extends AbstractManager
{
    /**
     * ProducerManager constructor.
     * @param ProducerRepository $repository
     */
    public function __construct(ProducerRepository $repository)
    {
        $this->repository = $repository;
    }

}