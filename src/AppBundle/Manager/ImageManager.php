<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Category;
use AppBundle\Entity\ProductImage;

/**
 * Class ImageManager
 *
 * @package AppBundle\Service
 */
class ImageManager
{
    /**
     *
     */
    private const SIZE_CATEGORY_PREVIEW = '50x50';

    /**
     *
     */
    private const SIZE_PREVIEW = '60x60';

    /**
     *
     */
    private const SIZE_LARGE = '400x400';

    /**
     *
     */
    private const SIZE_CATALOG_PRODUCT = '300x560';

    /**
     *
     */
    private const DIR_STORE = 'store';

    /**
     *
     */
    private const DIR_CATEGORY = 'category';

    /**
     *
     */
    private const DIR_PRODUCT = 'product';

    /**
     *
     */
    private const DIR_CHINA_PRODUCT = 'china_product';

    /**
     * @var string
     */
    private $host;

    /**
     * @var string
     */
    private $uploadDir;

    /**
     * @var string
     */
    private $thumbDir;

    /**
     * ImageManager constructor.
     *
     * @param $host
     * @param $uploadDir
     * @param $thumbDir
     */
    public function __construct($host, $uploadDir, $thumbDir)
    {
        $this->host = $host;
        $this->uploadDir = $uploadDir;
        $this->thumbDir = $thumbDir;
    }

    /**
     * @param ProductImage $productImage
     * @return string
     */
    public function getPreview(ProductImage $productImage): string
    {
        return $this->getThumb(
            $productImage,
            self::SIZE_PREVIEW
        );
    }

    /**
     * @param ProductImage $productImage
     * @return string
     */
    public function getLarge(ProductImage $productImage): string
    {
        return $this->getThumb(
            $productImage,
            self::SIZE_LARGE
        );
    }

    /**
     * @param ProductImage $productImage
     * @return string
     */
    public function getCatalogProductPreview(ProductImage $productImage): string
    {
        return $this->getThumb(
            $productImage,
            self::SIZE_CATALOG_PRODUCT
        );
    }

    /**
     * @param Category $category
     * @return string
     */
    public function getCategoryPreview(Category $category): string
    {
        return $this->getCommonThumbDir()
            . self::DIR_CATEGORY . DIRECTORY_SEPARATOR
            . self::SIZE_CATEGORY_PREVIEW . '_' . $category->getImage();
    }

    /**
     * @param ProductImage $productImage
     * @return string
     */
    public function getFull(ProductImage $productImage): string
    {
        $dir = self::DIR_PRODUCT;

        if ($productImage->getProduct()->isChinaProduct() && $productImage->getProduct()->getExternalId()) {
            $dir = self::DIR_CHINA_PRODUCT . DIRECTORY_SEPARATOR
                . $productImage->getProduct()->getExternalId();
        }

        return $this->getCommonDir()
            . $dir . DIRECTORY_SEPARATOR
            . $productImage->getName();
    }

    /**
     * @param ProductImage $productImage
     * @param string       $size
     * @return string
     */
    private function getThumb(ProductImage $productImage, string $size): string
    {
        $dir = self::DIR_PRODUCT;

        if ($productImage->getProduct()->isChinaProduct() && $productImage->getProduct()->getExternalId()) {
            $dir = self::DIR_CHINA_PRODUCT . DIRECTORY_SEPARATOR
                . $productImage->getProduct()->getExternalId();
        }

        return $this->getCommonThumbDir()
            . $dir . DIRECTORY_SEPARATOR
            . $size . '_' . $productImage->getName();
    }

    /**
     * @return string
     */
    public function getCommonDir(): string
    {
        return $this->host . DIRECTORY_SEPARATOR
            . $this->uploadDir . DIRECTORY_SEPARATOR
            . self::DIR_STORE . DIRECTORY_SEPARATOR;
    }

    /**
     * @return string
     */
    public function getCommonThumbDir(): string
    {
        return $this->host . DIRECTORY_SEPARATOR
            . $this->uploadDir . DIRECTORY_SEPARATOR
            . $this->thumbDir . DIRECTORY_SEPARATOR
            . self::DIR_STORE . DIRECTORY_SEPARATOR;
    }

    /**
     * @return string
     */
    public function getHost(): string
    {
        return $this->host;
    }

    /**
     * @param string $host
     * @return self
     */
    public function setHost(string $host): self
    {
        $this->host = $host;
        return $this;
    }

    /**
     * @return string
     */
    public function getUploadDir(): string
    {
        return $this->uploadDir;
    }

    /**
     * @param string $uploadDir
     * @return self
     */
    public function setUploadDir(string $uploadDir): self
    {
        $this->uploadDir = $uploadDir;
        return $this;
    }

    /**
     * @return string
     */
    public function getThumbDir(): string
    {
        return $this->thumbDir;
    }

    /**
     * @param string $thumbDir
     * @return self
     */
    public function setThumbDir(string $thumbDir): self
    {
        $this->thumbDir = $thumbDir;
        return $this;
    }

    /**
     * @return string
     */
    public function getProductDir(): string
    {
        return self::DIR_PRODUCT;
    }

    /**
     * @return string
     */
    public function getChinaProductDir(): string
    {
        return self::DIR_CHINA_PRODUCT;
    }

    /**
     * @return string
     */
    public function getLargeSize(): string
    {
        return self::SIZE_LARGE;
    }

    /**
     * @return string
     */
    public function getPreviewSize(): string
    {
        return self::SIZE_PREVIEW;
    }
}