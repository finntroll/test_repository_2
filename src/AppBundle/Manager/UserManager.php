<?php

namespace AppBundle\Manager;

use AppBundle\Entity\CartProduct;
use AppBundle\Entity\User;
use AppBundle\Entity\UserSocial;
use AppBundle\Exception\UmkaUserFriendlyException;
use AppBundle\Exception\User\HttpDuplicateUserException;
use AppBundle\Repository\UserRepository;
use AppBundle\Repository\UserSocialRepository;
use AppBundle\Service\Social\DTO\UserData;
use Doctrine\DBAL\ConnectionException;
use Doctrine\DBAL\Exception\UniqueConstraintViolationException;
use Doctrine\ORM\{
    EntityManager, EntityManagerInterface
};
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\DependencyInjection\Exception\ServiceCircularReferenceException;
use Symfony\Component\DependencyInjection\Exception\ServiceNotFoundException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\{
    NotFoundHttpException, UnauthorizedHttpException
};

/**
 * Class Auth
 *
 * @package AppBundle\Service
 */
class UserManager
{

    /**
     * @var PasswordManager
     */
    private $passwordManager;

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var EntityManager
     */
    private $em;
    /**
     * @var UserSocialRepository
     */
    private $userSocialRepository;

    private $container;

    /**
     * UserManager constructor.
     *
     * @param PasswordManager        $passwordManager
     * @param UserRepository         $userRepository
     * @param EntityManagerInterface $em
     * @param UserSocialRepository   $userSocialRepository
     * @param ContainerInterface     $container
     */
    public function __construct(
        PasswordManager $passwordManager,
        UserRepository $userRepository,
        EntityManagerInterface $em,
        UserSocialRepository $userSocialRepository,
        ContainerInterface $container
    )
    {
        $this->passwordManager      = $passwordManager;
        $this->userRepository       = $userRepository;
        $this->em                   = $em;
        $this->userSocialRepository = $userSocialRepository;
        $this->container            = $container;
    }

    /**
     * @param string $email
     * @param string $password
     *
     * @return User
     *
     * @throws \RuntimeException
     * @throws UnauthorizedHttpException
     * @throws NotFoundHttpException
     */
    public function getUserByEmailAndPassword(string $email, string $password): User
    {
        $user = $this->userRepository->findActiveByEmail($email);

        if (!$user) {
            throw new NotFoundHttpException('User not found');
        }

        if (!$this->passwordManager->isPassCorrectForUser($user, $password)) {
            throw new UnauthorizedHttpException('', 'Wrong credentials');
        }

        return $this->refreshLoggedUser($user);
    }

    /**
     * @param string      $deviceId
     * @param null|string $googleAccountName
     * @return User
     * @throws HttpDuplicateUserException
     */
    public function getOrCreateAndroidUser(string $deviceId, ?string $googleAccountName = null): User
    {
        if ($googleAccountName !== null) {
            $googleName = $this->prepareGoogleAccountName($googleAccountName);
        } else {
            $googleName = null;
        }

        $user = $this->userRepository->findAndroidUserByDeviceAndAccount($deviceId, $googleName);
        if (null === $user) {
            $user = User::createNewAndroidUser($deviceId, $googleName);
            $this->em->persist($user);
            try {
                $this->em->flush();
            } catch (UniqueConstraintViolationException $ex) {
                throw new HttpDuplicateUserException(Response::HTTP_CONFLICT, 'User already exists');
            }
        }

        return $this->refreshLoggedUser($user);
    }

    /**
     * @param string $deviceId
     * @param string $newDeviceId
     * @return User
     * @throws HttpDuplicateUserException
     */
    public function updateMobileUser(string $deviceId, string $newDeviceId): User
    {
        $user = $this->userRepository->findAndroidUserByDeviceAndAccount($deviceId);
        if (null === $user) {
            $user = User::createNewAndroidUser($newDeviceId);
            $this->em->persist($user);
            try {
                $this->em->flush();
            } catch (UniqueConstraintViolationException $ex) {
                throw new HttpDuplicateUserException(Response::HTTP_CONFLICT, 'User already exists');
            }

            return $this->refreshLoggedUser($user);
        }

        return $this->refreshDeviceUser($user, $newDeviceId);
    }

    /**
     * /**
     * @param null|string $accountName
     * @return null|string
     */
    protected function prepareGoogleAccountName(?string $accountName): ?string
    {
        if (\is_string(filter_var($accountName, FILTER_VALIDATE_EMAIL))) {
            return filter_var($accountName, FILTER_VALIDATE_EMAIL);
        }

        if (\is_string(filter_var($accountName . '@gmail.com', FILTER_VALIDATE_EMAIL))) {
            return filter_var($accountName . '@gmail.com', FILTER_VALIDATE_EMAIL);
        }

        return null;
    }

    /**
     * @param User   $user
     * @param string $newDeviceId
     * @return User
     */
    protected function refreshDeviceUser(User $user, $newDeviceId): User
    {
        $user->setDeviceId($newDeviceId);
        $user->setVisitTime(new \DateTime());
        $this->em->flush($user);
        return $user;
    }

    /**
     * @param User $user
     * @return User
     */
    protected function refreshLoggedUser(User $user): User
    {
        $user->setVisitTime(new \DateTime());
        $this->em->flush($user);
        return $user;
    }

    /**
     * @param UserData $userData
     * @return UserSocial|null
     * @throws HttpDuplicateUserException
     */
    public function findOrCreateBySocialAccount(UserData $userData): ?UserSocial
    {
        $socialAccount = $this->userSocialRepository->getSocialAccountByUidAndProvider($userData->uid, $userData->provider);
        if (null !== $socialAccount) {
            return $socialAccount;
        }

        $socialAccount = new UserSocial();
        $socialAccount
            ->setUid($userData->uid)
            ->setProvider($userData->provider);

        if (null !== $userData->email) {
            $user = $this->userRepository->findActiveByEmail($userData->email);
            if ($user instanceof User) {
                throw new HttpDuplicateUserException(Response::HTTP_CONFLICT, 'User with email already registered.');
            }
            $user = User::createNewSocialUser($userData->email, $userData->firstName, $userData->lastName);
            $socialAccount->setUser($user);
        }

        $this->em->persist($socialAccount);
        $this->em->flush();
        return $socialAccount;
    }

    /**
     * Merge dialogs, orders, etc from one account to another account
     *
     * @param User $userFrom
     * @param User $userTo
     *
     * @throws ServiceNotFoundException
     * @throws ServiceCircularReferenceException
     * @throws ConnectionException
     */
    public function mergeUsers(User $userFrom, User $userTo): void
    {
        $doctrine = $this->container->get('doctrine');

        $orderRepository         = $doctrine->getRepository('AppBundle:UserOrder');
        $cartRepository          = $doctrine->getRepository('AppBundle:CartProduct');
        $dialogRepository        = $doctrine->getRepository('AppBundle:Dialog');
        $userBonusRepository     = $doctrine->getRepository('AppBundle:UserBonus');
        $promocodeGiftRepository = $doctrine->getRepository('AppBundle:PromocodeGift');

        $this->em->getConnection()->beginTransaction();

        try {
            $orderRepository->transferBetweenUsers($userFrom, $userTo);
            $cartRepository->transferBetweenUsers($userFrom, $userTo);
            $dialogRepository->transferBetweenUsers($userFrom, $userTo);
            $userBonusRepository->transferBetweenUsers($userFrom, $userTo);
            $promocodeGiftRepository->transferBetweenUsers($userFrom, $userTo);

            $favoriteList     = $userFrom->getFavorites();
            /** @var CartProduct[] $cartProductsFrom */
            $cartProductsFrom = $userFrom->getCartProducts()->toArray();
            $cartProductsTo   = $userTo->getCartProducts();
            $cartRepository->deleteByUser($userFrom);

            foreach ($favoriteList as $favorite) {
                $userFrom->removeFavorite($favorite);
                $userTo->addFavorite($favorite);
            }

            $productsToTransfer = [];
            foreach ($cartProductsFrom as $cartProduct) {
                $productsToTransfer[$cartProduct->getProduct()->getId()] = $cartProduct->getCount();
            }

            foreach ($cartProductsTo as $cartProduct) {
                if (\array_key_exists($cartProduct->getProduct()->getId(), $productsToTransfer)) {
                    $cartProduct->setCount(
                        $productsToTransfer[$cartProduct->getProduct()->getId()] + $cartProduct->getCount()
                    );

                    $this->em->persist($cartProduct);
                }
            }

            $userTo->setDeviceId($userFrom->getDeviceId());

            $this->em->persist($userFrom);
            $this->em->persist($userTo);
            $this->em->flush();

            $this->em->getConnection()->commit();
        } catch (\Exception $exception) {

            $this->container->get('logger')->error('User merge error: ' . $exception->getTraceAsString());
            $this->em->getConnection()->rollBack();

            throw new UmkaUserFriendlyException();
        }
    }

}
