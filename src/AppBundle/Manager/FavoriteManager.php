<?php

namespace AppBundle\Manager;

use AppBundle\Entity\{
    Product, User
};
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class FavoriteManager
 *
 * @package AppBundle\Manager
 */
class FavoriteManager
{

    /**
     * @var EntityManagerInterface
     */
    private $entityManager;

    /**
     * FavoriteManager constructor.
     *
     * @param EntityManagerInterface $entityManager
     */
    public function __construct(EntityManagerInterface $entityManager)
    {
        $this->entityManager = $entityManager;
    }

    /**
     * @param User    $user
     * @param Product $product
     *
     * @return void
     */
    public function addFavorite(User $user, Product $product): void
    {
        $user->addFavorite($product);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

    /**
     * @param User    $user
     * @param Product $product
     *
     * @return void
     */
    public function removeFavorite(User $user, Product $product): void
    {
        $user->removeFavorite($product);

        $this->entityManager->persist($user);
        $this->entityManager->flush();
    }

}