<?php

namespace AppBundle\Manager;

use AppBundle\Filter\FilterInterface;
use AppBundle\Repository\ProductModelRepository;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class ProductModelManager
 *
 * @package AppBundle\Manager
 */
class ProductModelManager extends AbstractManager
{
    /**
     * ProductManager constructor.
     *
     * @param ProductModelRepository      $repository
     * @param EntityManagerInterface $em
     */
    public function __construct(ProductModelRepository $repository, EntityManagerInterface $em)
    {
        parent::__construct($em);
        $this->repository        = $repository;
    }

    /**
     * @param FilterInterface|null $filter
     * @return \Doctrine\ORM\QueryBuilder
     */
    public function queryApprovedByFilter(FilterInterface $filter = null)
    {
        return $this->repository->queryApprovedByFilter($filter);
    }
}