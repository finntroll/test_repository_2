<?php

namespace AppBundle\Manager;

use AppBundle\Entity\Rate;
use AppBundle\Entity\User;
use AppBundle\Entity\UserOrder;
use AppBundle\Repository\OrderRepository;
use Doctrine\ORM\EntityManagerInterface;
use AppBundle\Service\CurrencyConverter;
use Doctrine\ORM\QueryBuilder;

/**
 * Class OrderManager
 *
 * @package AppBundle\Manager
 */
class OrderManager extends AbstractManager
{
    /**
     * @var CurrencyConverter
     */
    private $currencyConverter;

    /**
     * @var OrderProductManager
     */
    private $orderProductManager;

    /**
     * @var PaymentTransactionManager
     */
    private $paymentTransactionManager;

    /**
     * OrderManager constructor.
     *
     * @param OrderRepository           $repository
     * @param CurrencyConverter         $currencyConverter
     * @param OrderProductManager       $orderProductManager
     * @param PaymentTransactionManager $paymentTransactionManager
     * @param EntityManagerInterface    $em
     */
    public function __construct(
        OrderRepository $repository,
        CurrencyConverter $currencyConverter,
        OrderProductManager $orderProductManager,
        PaymentTransactionManager $paymentTransactionManager,
        EntityManagerInterface $em
    )
    {
        parent::__construct($em);
        $this->repository                = $repository;
        $this->currencyConverter         = $currencyConverter;
        $this->orderProductManager       = $orderProductManager;
        $this->paymentTransactionManager = $paymentTransactionManager;
    }

    /**
     * @param UserOrder $order
     *
     * @return float
     */
    public function getTotalCost(UserOrder $order): float
    {
        return $order->getTotalPrice() - $order->getDiscount() - $order->getPaidWithBonus();
    }

    /**
     * @param User  $user
     * @param array $formattedParams
     * @param array $products
     *
     * @return UserOrder
     */
    public function createNewOrder(User $user, array $formattedParams, array $products): UserOrder
    {
        // TODO работа со скидками и купонами + подумать как бы это разнести, а то много кода
        $contact = $formattedParams['contact'];
        $address = $formattedParams['address'];
        $rate    = $this->currencyConverter->getRate(Rate::RUB);
        $time    = new \DateTime();

        $userOrder = new UserOrder();

        $userOrder->setStatus($userOrder::STATUS_NEW);
        $userOrder->setDate($time);
        $userOrder->setUser($user);
        $userOrder->setCountry(htmlspecialchars($address['country']));
        $userOrder->setRegion(htmlspecialchars($address['region']));
        $userOrder->setCity(htmlspecialchars($address['city']));
        $userOrder->setAddress(htmlspecialchars($address['full_address']));
        $userOrder->setZipcode(htmlspecialchars($address['zip_code']));
        $userOrder->setEmail(htmlspecialchars($contact['email']));
        $userOrder->setName(htmlspecialchars($contact['full_name']));
        $userOrder->setPhone($contact['phone']);
        $userOrder->setTin(htmlspecialchars($contact['tin'] ?? null));
        $userOrder->setUrl(md5(uniqid(time(), true)));
        $userOrder->setModified($time);
        $userOrder->setUserCurrency(Rate::RUB);
        $userOrder->setCurrencyRate($rate);
        $userOrder->setPaymentMethodId(UserOrder::PAYMENT_METHOD_CARD_ID);

        $this->em->persist($userOrder);
        $this->em->flush();

        $price = $this->orderProductManager->createOrderProductsReturnPrice($products, $userOrder);

        $userOrder->setTotalPriceBase($price['base']);
        $userOrder->setTotalPrice($price['total']);
        $userOrder->setTotalPriceUserCurrency($price['total']);

        $paymentTransaction = $this->paymentTransactionManager->createPaymentTransaction($userOrder, $price);

        $userOrder->addOrderPayment($paymentTransaction);

        $this->em->flush();

        return $userOrder;
    }

    /**
     * @param int $userId
     *
     * @return QueryBuilder
     */
    public function findByUserId(int $userId): QueryBuilder
    {
        return $this->repository->findByUserId($userId);
    }

    /**
     * @param int $userId
     * @param int $orderId
     *
     * @return mixed
     */
    public function findByOrderId(int $userId, int $orderId)
    {
        return $this->repository->findByUserIdAndOrderId($userId, $orderId)->getQuery()->getOneOrNullResult();
    }

    /**
     * @param int    $userId
     * @param int    $orderId
     * @param string $status
     *
     * @return mixed
     */
    public function changeOrderStatus(int $userId, int $orderId, string $status)
    {
        $userOrder = $this->findByOrderId($userId, $orderId);

        if (!empty($userOrder)) {
            /**
             * @var UserOrder $userOrder
             */
            $userOrder->setStatus($status);

            $this->em->persist($userOrder);

            $this->em->flush();
        }

        return $userOrder;
    }
}