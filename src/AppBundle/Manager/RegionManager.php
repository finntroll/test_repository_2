<?php

namespace AppBundle\Manager;

use AppBundle\Repository\RegionRepository;

class RegionManager extends AbstractManager
{

    /**
     * CountryManager constructor.
     * @param RegionRepository $repository
     */
    public function __construct(RegionRepository $repository)
    {
        $this->repository = $repository;
    }

}