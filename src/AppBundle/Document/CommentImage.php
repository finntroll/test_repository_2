<?php

namespace AppBundle\Document;

use Doctrine\MongoDB\GridFSFile;
use Doctrine\ODM\MongoDB\Mapping\Annotations as MongoDB;

/**
 * Class CommentImage
 *
 * @package AppBundle\Document
 * @MongoDB\Document(collection="fs")
 */
class CommentImage
{
    private const MIME_TYPE_JPG = 'image/jpeg';

    private const MIME_TYPE_PNG = 'image/png';

    /**
     * @var string
     *
     * @MongoDB\Id
     */
    private $id;

    /**
     * @var GridFSFile|string
     *
     * @MongoDB\File
     */
    private $file;

    /**
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    private $filename;

    /**
     * @var string
     */
    private $mimeType;

    /**
     * @var \DateTime|null
     *
     * @MongoDB\Field(type="date")
     */
    private $uploadDate;

    /**
     * @var int
     *
     * @MongoDB\Field(type="integer")
     */
    private $length;

    /**
     * @var int
     *
     * @MongoDB\Field(type="integer")
     */
    private $chunkSize;

    /**
     * @var string
     *
     * @MongoDB\Field(type="string")
     */
    private $md5;

    public function getId(): string
    {
        return $this->id;
    }

    public function getFile()
    {
        return $this->file;
    }

    public function setFile($file): self
    {
        $this->file = $file;

        return $this;
    }

    public function getFilename(): string
    {
        return $this->filename;
    }

    public function getMimeType(): string
    {
        return $this->mimeType;
    }

    public function setMimeType($mimeType): self
    {
        $this->mimeType = $mimeType;

        return $this;
    }

    public function getChunkSize(): int
    {
        return $this->chunkSize;
    }

    public function getLength(): int
    {
        return $this->length;
    }

    public function getMd5(): string
    {
        return $this->md5;
    }

    public function getUploadDate(): ?\DateTime
    {
        return $this->uploadDate;
    }

    public function isValidImage(): bool
    {
        return \in_array($this->mimeType, [self::MIME_TYPE_JPG, self::MIME_TYPE_PNG], true);
    }

    public function generateName(): self
    {
        if (!(bool)$this->file) {
            throw new \InvalidArgumentException('File is not set');
        }

        if ($this->file instanceof GridFSFile) {
            throw new \InvalidArgumentException('Renaming of existed file is not available');
        }

        $this->filename = sha1(file_get_contents($this->file));

        return $this;
    }
}
