<?php

namespace AppBundle\EventListener;

use Symfony\Component\{
    HttpFoundation\JsonResponse, HttpKernel\Event\GetResponseForExceptionEvent, HttpFoundation\Response,
    HttpKernel\Exception\HttpExceptionInterface, Security\Core\Exception\InsufficientAuthenticationException, Validator\Exception\ValidatorException
};

class ExceptionListener
{
    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onKernelException(GetResponseForExceptionEvent $event): void
    {
        $exception = $event->getException();
        $message   = $exception->getMessage();
        $status    = Response::HTTP_INTERNAL_SERVER_ERROR;

        switch ($exception) {
            case $exception instanceof HttpExceptionInterface:
                $status = $exception->getStatusCode();
                break;
            case $exception instanceof InsufficientAuthenticationException:
                $status = Response::HTTP_FORBIDDEN;
                break;
            default:
                if (($code = $exception->getCode()) !== 0) {
                    $status = $code;
                }
                break;
        }

        $response = new JsonResponse(
            [
                'status'  => $status,
                'message' => $message
            ],
            $status
        );

        $event->allowCustomResponseCode();
        $event->setResponse($response);
    }

}
