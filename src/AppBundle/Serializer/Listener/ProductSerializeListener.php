<?php

namespace AppBundle\Serializer\Listener;

use AppBundle\Manager\{
    ImageManager, ProductManager
};
use AppBundle\Serializer\SerializationGroups;
use JMS\Serializer\EventDispatcher\{
    EventSubscriberInterface, ObjectEvent
};
use AppBundle\Entity\{
    ProductAttributeValue, Product, User
};
use JMS\Serializer\JsonSerializationVisitor;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ProductSerializeListener
 */
class ProductSerializeListener implements EventSubscriberInterface
{

    /**
     *
     */
    private const SORT_START_NUMBER = 1;

    /** @var ImageManager */
    private $imageManager;

    /** @var ProductManager */
    private $productManager;

    /** * @var null|User */
    private $user;

    /**
     * ProductSerializeListener constructor.
     *
     * @param ImageManager   $imageManager
     * @param ProductManager $productManager
     */
    public function __construct(ImageManager $imageManager, ProductManager $productManager, TokenStorageInterface $tokenStorage)
    {
        $this->imageManager   = $imageManager;
        $this->productManager = $productManager;

        if (\is_object($user = $tokenStorage->getToken()->getUser())) {
            $this->user = $user;
        }
    }

    /**
     * @inheritdoc
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            [
                'event'  => 'serializer.post_serialize',
                'method' => 'onProductPostSerialize',
                'class'  => Product::class,
                'format' => 'json'
            ]
        ];
    }

    /**
     * @param ObjectEvent $event
     */
    public function onProductPostSerialize(ObjectEvent $event): void
    {
        /** @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();

        /** @var Product $product */
        $product = $event->getObject();

        $context = $event->getContext();

        $visitor->setData('in_favorites', false);
        $visitor->setData('is_out_of_stock', !$product->isInStock());
        $visitor->setData('price', $this->productManager->getBasePriceRub($product));
        $visitor->setData('description', $product->getDescription());
        if ($discountPrice = $this->productManager->getDiscountPriceRub($product)) {
            $visitor->setData('discount_price', $discountPrice);
        }

        $groups = [];
        if ($context->attributes->containsKey('groups')) {
            $groups = $context->attributes->get('groups')->get();
        }

        if ($this->user !== null && $this->user->getFavorites()->contains($product)) {
            $visitor->setData('in_favorites', true);
        }

        if (\in_array(SerializationGroups::PRODUCT_PREVIEW, $groups, true)) {
            $visitor->setData('images', $this->getMainImagePath($product));

            return;
        }

        $visitor->setData('images', $this->getImagesPath($product));
        $visitor->setData('attributes', $this->getAttributes($product));

        if ($product->getProductModel() && !empty($product->getProductModel()->getAttributes())) {
            $visitor->setData('similar', $product->getProductModel()->getAttributes());
        }
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    private function getMainImagePath(Product $product): array
    {
        $image = $product->getMainImage();

        if (!$image) {
            return [
                [
                    'preview' => null,
                    'large'   => null,
                    'full'    => null,
                    'sort'    => self::SORT_START_NUMBER
                ]
            ];
        }

        return [
            [
                'preview' => $this->imageManager->getPreview($image),
                'large'   => $this->imageManager->getLarge($image),
                'full'    => $this->imageManager->getFull($image),
                'sort'    => self::SORT_START_NUMBER
            ]
        ];

    }

    /**
     * @param Product $product
     *
     * @return array
     */
    private function getImagesPath(Product $product): array
    {
        $handledImages = [];
        $imageList     = $product->getImages()->getValues();
        $sort          = self::SORT_START_NUMBER;

        if ($mainImage = $product->getMainImage()) {
            array_unshift($imageList, $mainImage);
        }

        foreach ($imageList as $image) {
            $handledImages [] = [
                'preview' => $this->imageManager->getPreview($image),
                'large'   => $this->imageManager->getLarge($image),
                'full'    => $this->imageManager->getFull($image),
                'sort'    => $sort
            ];

            $sort++;
        }

        return $handledImages;
    }

    /**
     * @param Product $product
     *
     * @return array
     */
    private function getAttributes(Product $product): array
    {
        $handledAttributes = [];

        /** @var ProductAttributeValue $attribute */
        foreach ($product->getAttributes() as $attribute) {
            $elem = [
                'id' => $attribute->getAttributeId(),
                'value' => $attribute->getValue()
            ];

            $handledAttributes[] = $elem;
        }
        return $handledAttributes;
    }

}
