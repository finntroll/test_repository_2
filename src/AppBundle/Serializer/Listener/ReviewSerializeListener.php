<?php

namespace AppBundle\Serializer\Listener;


use AppBundle\Entity\ProductReview;
use AppBundle\Entity\User;
use AppBundle\Repository\UserRepository;
use JMS\Serializer\EventDispatcher\{
    EventSubscriberInterface, ObjectEvent
};
use JMS\Serializer\JsonSerializationVisitor;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage;

class ReviewSerializeListener implements EventSubscriberInterface
{

    /**
     * @var User|null
     */
    private $user;


    /**
     * ReviewSerializeListener constructor.
     *
     * @param TokenStorage $tokenStorage
     */
    public function __construct(TokenStorage $tokenStorage)
    {
        if ($token = $tokenStorage->getToken()) {
            if (\is_object($token->getUser())) {
                $this->user = $token->getUser();
            }
        }
    }

    /**
     * @inheritdoc
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            [
                'event'  => 'serializer.post_serialize',
                'method' => 'onReviewPostSerialize',
                'class'  => ProductReview::class,
                'format' => 'json'
            ]
        ];
    }

    /**
     * @param ObjectEvent $event
     */
    public function onReviewPostSerialize(ObjectEvent $event): void
    {
        /** @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();

        /** @var ProductReview $review */
        $review = $event->getObject();

        $owner = $this->reviewOwner($review->getUser());

        if (!$owner && $review->getProductRating() < ProductReview::MIN_SHOW_RATING) {
            $visitor->setData('rating', ProductReview::MIN_RATING);
        }

        $visitor->setData('user_is_owner', $owner);
    }

    /**
     * @param User|null $reviewUser
     * @return bool
     */
    private function reviewOwner(?User $reviewUser): bool
    {
        if ($this->user === null || $reviewUser === null) {
            return false;
        }

        return $this->user->getId() === $reviewUser->getId();
    }

}
