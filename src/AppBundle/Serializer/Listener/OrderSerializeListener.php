<?php

namespace AppBundle\Serializer\Listener;

use AppBundle\Manager\OrderManager;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use AppBundle\Entity\UserOrder;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;

/**
 * Class OrderSerializeListener
 *
 * @package AppBundle\Serializer\Listener
 */
class OrderSerializeListener implements EventSubscriberInterface
{

    /**
     * @var OrderManager
     */
    private $orderManager;

    /**
     * @param OrderManager $orderManager
     */
    public function setOrderManager(OrderManager $orderManager): void
    {
        $this->orderManager = $orderManager;
    }

    /**
     * @inheritdoc
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            [
                'event'  => 'serializer.post_serialize',
                'method' => 'onOrderPostSerialize',
                'class'  => UserOrder::class,
                'format' => 'json'
            ]
        ];
    }

    /**
     * @param ObjectEvent $event
     */
    public function onOrderPostSerialize(ObjectEvent $event): void
    {
        /** @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();

        /** @var UserOrder $order */
        $order = $event->getObject();

        $address  = [];
        $contacts = [];

        $address['city']         = $order->getCity();
        $address['country']      = $order->getCountry();
        $address['full_address'] = $order->getAddress();
        $address['region']       = $order->getRegion();
        $address['zip_code']     = $order->getZipcode();

        $contacts['email']     = $order->getEmail();
        $contacts['full_name'] = $order->getName();
        $contacts['phone']     = $order->getPhone();

        $tin = $order->getTin() ?: null;
        if (!empty($tin)) {
            $contacts['tin'] = $tin;
        }

        $visitor->setData('address', $address);
        $visitor->setData('contact', $contacts);
    }

}