<?php

namespace AppBundle\Serializer\Listener;

use AppBundle\Manager\{
    ImageManager, ProductModelManager
};
use AppBundle\Serializer\SerializationGroups;
use JMS\Serializer\EventDispatcher\{
    EventSubscriberInterface, ObjectEvent
};
use AppBundle\Entity\{
    ProductAttributeValue, ProductModel, User
};
use JMS\Serializer\JsonSerializationVisitor;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

/**
 * Class ProductModelSerializeListener
 */
class ProductModelSerializeListener implements EventSubscriberInterface
{

    /**
     *
     */
    private const SORT_START_NUMBER = 1;

    /** @var ImageManager */
    private $imageManager;

    /** @var ProductModelManager */
    private $productModelManager;

    /**
     * ProductModelSerializeListener constructor.
     *
     * @param ImageManager   $imageManager
     * @param ProductModelManager $productModelManager
     */
    public function __construct(ImageManager $imageManager, ProductModelManager $productModelManager, TokenStorageInterface $tokenStorage)
    {
        $this->imageManager   = $imageManager;
        $this->productModelManager = $productModelManager;

        if (\is_object($user = $tokenStorage->getToken()->getUser())) {
            $this->user = $user;
        }
    }

    /**
     * @inheritdoc
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            [
                'event'  => 'serializer.post_serialize',
                'method' => 'onProductModelPostSerialize',
                'class'  => ProductModel::class,
                'format' => 'json'
            ]
        ];
    }

    /**
     * @param ObjectEvent $event
     */
    public function onProductModelPostSerialize(ObjectEvent $event): void
    {
        /** @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();

        /** @var ProductModel $productModel */
        $productModel = $event->getObject();

        $visitor->setData('price', $productModel->getPrice()??$productModel->getMinPrice());
        if (!empty($productModel->getDiscountPrice())) {
            $visitor->setData('discount_price', $productModel->getDiscountPrice());
        }
        $visitor->setData('image_filename', $productModel->getImage());
        $visitor->setData('in_stock', $productModel->getInStock());
        $visitor->setData('is_out_of_stock', !$productModel->getInStock());
    }
}
