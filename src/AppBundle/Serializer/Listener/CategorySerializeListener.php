<?php
namespace AppBundle\Serializer\Listener;

use AppBundle\Entity\Category;
use AppBundle\Manager\ImageManager;
use JMS\Serializer\EventDispatcher\{
    EventSubscriberInterface, PreSerializeEvent
};

class CategorySerializeListener implements EventSubscriberInterface
{

    /**
     * @inheritdoc
     * @return array
     */
    public static function getSubscribedEvents(): array
    {
        return [
            [
                'event' => 'serializer.pre_serialize',
                'method' => 'onCategoryPreSerialize',
                'class' => Category::class,
                'format' => 'json',
            ],
        ];
    }

    /**
     * @param PreSerializeEvent $event
     */
    public function onCategoryPreSerialize(PreSerializeEvent $event): void
    {
        $this->fixParent($event->getObject());
    }

    /**
     * @param Category $category
     */
    private function fixParent(Category $category): void
    {
        $parent = $category->getParent();
        if (!$parent) {
            return;
        }

        if ($parent->isGroup()) {
            $category->setParent($parent->getParent());
        }
    }

}