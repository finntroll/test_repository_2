<?php

namespace AppBundle\Serializer;

/**
 * Class SerializationGroups
 */
abstract class SerializationGroups
{

    /**
     *
     */
    public const PRODUCT_PREVIEW = 'product_preview';

    /**
     * @inheritdoc
     */
    public const MODEL_CATALOG = 'model_catalog';

    /**
     *
     */
    public const CATALOG = 'catalog';

    /**
     *
     */
    public const REVIEW_LIST = 'review_list';

}