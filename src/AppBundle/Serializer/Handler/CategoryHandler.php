<?php

namespace AppBundle\Serializer\Handler;

use AppBundle\Entity\Category;
use AppBundle\Manager\CategoryManager;
use AppBundle\Manager\ImageManager;
use JMS\Serializer\{
    Context, GraphNavigator, Handler\SubscribingHandlerInterface, JsonSerializationVisitor
};

/**
 * Class CategoryHandler
 *
 * @package AppBundle\Serializer\Handler
 */
class CategoryHandler implements SubscribingHandlerInterface
{

    /** @var ImageManager */
    private $imageManager;

    /** @var array */
    private $categoryInfo;

    /**
     * CategoryHandler constructor.
     *
     * @param ImageManager       $imageManager
     * @param CategoryManager    $categoryManager
     */
    public function __construct(
        ImageManager $imageManager,
        CategoryManager $categoryManager
    )
    {
        $this->imageManager        = $imageManager;
        $this->categoryInfo        = $categoryManager->getCategoriesWithAddFieldsRedis();
    }

    /**
     * @return array
     */
    public static function getSubscribingMethods(): array
    {
        return [
            [
                'direction' => GraphNavigator::DIRECTION_SERIALIZATION,
                'format'    => 'json',
                'type'      => 'AppBundle:Category',
                'method'    => 'serializeCategoryToJson'
            ]
        ];
    }

    /**
     * @param JsonSerializationVisitor $visitor
     * @param Category                 $category
     * @param array                    $type
     * @param Context                  $context
     *
     * @return array
     */
    public function serializeCategoryToJson(JsonSerializationVisitor $visitor, Category $category, array $type, Context $context): array
    {
        $array = [
            'id'         => $category->getId(),
            'title'      => $category->getTitle(),
            'image'      => $this->imageManager->getCategoryPreview($category),
            'sort'       => $category->getSort(),
            'producers'  => []
        ];

        if ($category->getParent()) {
            $array['parent_id'] = $category->getParent()->getId();
        }

        if (isset($this->categoryInfo[$category->getId()])) {
            $categoryInfo = $this->categoryInfo[$category->getId()];
            $array['producers']  = $categoryInfo['producers'];
            $array['attributes'] = $categoryInfo['attributes'];
        }

        return array_filter($array, function ($value) {
            return !empty($value);
        });
    }

}