<?php

namespace AppBundle\Parameters;

/**
 * Class RedisParameter
 *
 * @package AppBundle\Parameters
 */
abstract class RedisParameter
{
    /**
     *
     */
    public const RATE_PREFIX = 'rate_';

    /**
     *
     */
    public const RATE_CACHE_TIME_SECONDS = 86400;

    /**
     *
     */
    public const CACHE_CATEGORIES_WITH_ADD_NAME = 'categories.producers.attributes.list';

    /**
     *
     */
    public const CACHE_CATEGORIES_WITH_ADD_TIME_SECONDS = 86400;

}