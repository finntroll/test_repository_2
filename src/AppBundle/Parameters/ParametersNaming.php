<?php

namespace AppBundle\Parameters;

/**
 * Class ParametersNaming
 *
 * @package AppBundle\Parameters
 */
abstract class ParametersNaming
{
    /**
     *
     */
    public const PAGE = 'page';

    /**
     *
     */
    public const PER_PAGE = 'perPage';

    /**
     *
     */
    public const COUNT = 'count';
}