<?php

namespace AppBundle\Parameters;

/**
 * Class DeliveryDates
 *
 * @package AppBundle\Parameters
 */
abstract class DeliveryDates
{

    /**
     * Доставка внутри страны
     */
    public const FAST_DAYS_COUNT_FROM = 5;

    /**
     * Доставка внутри страны
     */
    public const FAST_DAYS_COUNT_TO = 21;

    /**
     * Доставка из Китая
     */
    public const CROSSBORDER_DAYS_COUNT_FROM = 15;

    /**
     * Доставка из Китая
     */
    public const CROSSBORDER_DAYS_COUNT_TO = 60;

}