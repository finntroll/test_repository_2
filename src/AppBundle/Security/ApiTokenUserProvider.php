<?php

// src/AppBundle/Security/ApiKeyUserProvider.php
namespace AppBundle\Security;

use AppBundle\Repository\UserRepository;
use AppBundle\Service\TokenStorage;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Core\User\User;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;

class ApiTokenUserProvider implements UserProviderInterface
{

    /**
     * @var UserRepository
     */
    private $userRepository;

    /**
     * @var TokenStorage
     */
    private $tokenStorage;

    /**
     * @var EntityManager
     */
    private $em;
    /** @noinspection PhpDocSignatureInspection */
    /**
     * ApiTokenUserProvider constructor.
     * @param UserRepository $userRepository
     * @param TokenStorage $tokenStorage
     * @param EntityManagerInterface $em
     */
    public function __construct( UserRepository $userRepository,
                                 TokenStorage $tokenStorage,
                                 EntityManagerInterface $entityManager)
    {
        $this->userRepository = $userRepository;
        $this->tokenStorage = $tokenStorage;
        $this->em = $entityManager;
    }


    public function getUsernameForApiToken($accessToken)
    {
        $username = $this->tokenStorage->getUsernameByApiToken($accessToken);
        // Look up the username based on the token in the database, via
        // an API call, or do something entirely different
        //$username = ...;

        return $username;
    }

    /**
     * @param string $username
     * @return \AppBundle\Entity\User|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function loadUserByUsername($username): ?\AppBundle\Entity\User
    {
        return $this->userRepository->findActiveByEmail($username);
    }

    /**
     * @param UserInterface $user
     * @return \AppBundle\Entity\User|null
     * @throws \Doctrine\ORM\NonUniqueResultException
     */
    public function refreshUser(UserInterface $user): ?\AppBundle\Entity\User
    {
        // this is used for storing authentication in the session
        // but in this example, the token is sent in each request,
        // so authentication can be stateless. Throwing this exception
        // is proper to make things stateless
        throw new UnsupportedUserException();
    }

    /**
     * @param string $class
     * @return bool
     */
    public function supportsClass($class): bool
    {
        return User::class === $class;
    }
}