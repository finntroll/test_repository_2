<?php

namespace AppBundle\Form\User;

use AppBundle\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\{
    ChoiceType, DateType, EmailType
};
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\{
    Exception\AccessException, OptionsResolver
};


/**
 * Class ProfileForm
 *
 * @package AppBundle\Form
 */
class ProfileForm extends AbstractType
{

    /**
     * {@inheritdoc}
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->setMethod('PUT')
            ->add('first_name', null, ['property_path' => 'firstName'])
            ->add('middle_name', null, ['property_path' => 'middleName'])
            ->add('last_name', null, ['property_path' => 'lastName'])
            ->add('nick_name', null, ['property_path' => 'nickName'])
            ->add('email', EmailType::class)
            ->add('gender', ChoiceType::class, [
                'choices' => [
                    'Не указано' => User::GENDER_THING,
                    'Мужчина'    => User::GENDER_MALE,
                    'Женщина'    => User::GENDER_FEMALE
                ]
            ])
            ->add('phone')
            ->add('birth_date', DateType::class, ['property_path' => 'birthDate',
                                                  'widget'        => 'choice',
                                                  'years'         => range(
                                                      (new \DateTime())->format('Y'),
                                                      (new \DateTime())->sub(new \DateInterval('P100Y'))->format('Y')
                                                  ),
            ]);
    }

    /**
     * {@inheritdoc}
     * @param OptionsResolver $resolver
     * @throws AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                                   'data_class'      => User::class,
                                   'csrf_protection' => false
                               ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return '';
    }

}
