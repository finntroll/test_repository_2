<?php

namespace AppBundle\Form\ProductReview;

use AppBundle\Entity\ProductReview;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\{
    Core\Type\IntegerType, Core\Type\TextType
};
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\Exception\AccessException;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ReviewForm
 *
 * @package AppBundle\Form\ProductReview
 */
class ReviewForm extends AbstractType
{

    /**
     * {@inheritdoc}
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder->setMethod('PATCH')
            ->add('product_id', IntegerType::class, ['mapped' => false])
            ->add('rating', IntegerType::class, ['property_path' => 'productRating'])
            ->add('text', TextType::class);
    }

    /**
     * {@inheritdoc}
     * @param OptionsResolver $resolver
     * @throws AccessException
     */
    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
                                   'data_class'         => ProductReview::class,
                                   'csrf_protection'    => false,
                                   'allow_extra_fields' => true,
                               ]);
    }

    /**
     * @return string
     */
    public function getBlockPrefix(): string
    {
        return '';
    }

}
