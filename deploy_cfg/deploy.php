<?php
namespace Deployer;

require 'recipe/symfony3.php';

//@TODO remove m drom api
const STAGE_DEPLOY_PATH = '/var/www/api.in.umka.co';
const PROD_DEPLOY_PATH = '/var/www/zeta.umkamall.ru';
set('ssh_multiplexing', true);
// Project name
set('application', 'mobile-api');

set('keep_releases', 5);
set('cleanup_use_sudo', true);
set('writable_dirs', ['var/cache', 'var/logs', 'var/sessions']);
add('shared_files', ['web/layout']);

// Project repository
set('repository', 'git@gitlab.in.umka.co:backend/api-mobile.git');

// [Optional] Allocate tty for git clone. Default value is false.
set('git_tty', false); 

// Shared files/dirs between deploys 
//add('shared_files', []);
///add('shared_dirs', ['app/config']);

// Writable dirs by web server 
set('allow_anonymous_stats', false);

// Hosts

host('stage.in.umka.co')
    ->stage('stage')
    ->set('branch','master')
    ->user('deployer')
    ->identityFile('~/.ssh/ci_id_rsa')
    ->forwardAgent(true)
    ->multiplexing(true)
    ->set('deploy_path', STAGE_DEPLOY_PATH);

host('back0371.in.umka.co')
    ->stage('prod')
    ->set('branch','{RELEASE_TAG_PLACEHOLDER}')
    ->user('deployer')
    ->identityFile('~/.ssh/ci_id_rsa')
    ->forwardAgent(true)
    ->multiplexing(true)
    ->set('deploy_path', PROD_DEPLOY_PATH);


host('back0361.in.umka.co')
    ->stage('prod')
    ->set('branch','{RELEASE_TAG_PLACEHOLDER}')
    ->user('deployer')
    ->identityFile('~/.ssh/ci_id_rsa')
    ->forwardAgent(true)
    ->multiplexing(true)
    ->set('deploy_path', PROD_DEPLOY_PATH);


host('back0362.in.umka.co')
    ->stage('prod')
    ->set('branch','{RELEASE_TAG_PLACEHOLDER}')
    ->user('deployer')
    ->identityFile('~/.ssh/ci_id_rsa')
    ->forwardAgent(true)
    ->multiplexing(true)
    ->set('deploy_path', PROD_DEPLOY_PATH);

// Tasks

task('build', function () {
    run('cd {{release_path}} && build');
});

// [Optional] if deploy fails automatically unlock.
after('deploy:failed', 'deploy:unlock');

// Migrate database before symlink new release.

before('deploy:symlink', 'database:migrate');

task('reload:php-fpm', function () {
    run('cd {{release_path}} && rm -rf ./deploy_cfg');
    run('sudo service php7.1-fpm reload');
});

after('deploy:symlink', 'reload:php-fpm');
after('deploy:symlink', 'category:update-fields');

/**
 * Main task
 */
task('database:migrate', function () {
    //bundle not available
    //run('{{bin/php}} {{bin/console}} doctrine:migrations:migrate {{console_options}} --allow-no-migration');
})->desc('Migrate database');

/**
 * Redis filling
 */
task('category:update-fields', function () {
    run('cd {{release_path}} && php bin/console category:update-add-fields --env prod');
})->desc('Filling redis with categories info: producers, attributes');

task('deploy:writable', function () {
    $dirs = join(' ', get('writable_dirs'));
    $mode = get('writable_mode');
    $sudo = get('writable_use_sudo') ? 'sudo' : '';
    $httpUser = get('http_user', false);
    $runOpts = [];
    if ($sudo) {
        $runOpts['tty'] = get('writable_tty', false);
    }

    if (empty($dirs)) {
        return;
    }

    if ($httpUser === false && $mode !== 'chmod') {
        // Detect http user in process list.
        $httpUser = run("ps axo user,comm | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\\  -f1");

        if (empty($httpUser)) {
            throw new \RuntimeException(
                "Can't detect http user name.\n" .
                "Please setup `http_user` config parameter."
            );
        }
    }

    try {
        cd('{{release_path}}');

        // Create directories if they don't exist
        run("mkdir -p $dirs");
        run("sudo chown -RL $httpUser $dirs", $runOpts);
    } catch (\RuntimeException $e) {
        $formatter = Deployer::get()->getHelper('formatter');
write($dirs);
        $errorMessage = [
            "Unable to setup correct permissions for writable dirs.                  ",
            "You need to configure sudo's sudoers files to not prompt for password,",
            "or setup correct permissions manually.                                  ",
        ];
        write($formatter->formatBlock($errorMessage, 'error', true));

        throw $e;
    }
});
