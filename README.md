api-mobile
==========

## Запуск тестов

1. Убедиться, что создана БД, указанная в файле app/config/parameters_test.yml
2. Запустить тесты в шторме: 
  * a) ПКМ на phpunit.xml.dist -> "Run"
  * b) ```php vendor/phpunit/phpunit/phpunit --configuration phpunit.xml.dist```
  * c) git push ветки с префиксом ```UM-```

