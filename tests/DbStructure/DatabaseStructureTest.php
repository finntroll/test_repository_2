<?php

namespace Tests\Unit;

require_once dirname(__DIR__) . '/../app/AppKernel.php';

use Symfony;
use Doctrine;
use Doctrine\ORM\Tools\SchemaTool;
use Doctrine\ORM\EntityManager;
use PHPUnit\Framework\TestCase;

class DatabaseStructureTest extends TestCase
{
    /**
     * @var Symfony\Component\HttpKernel\Kernel $kernel
     */
    protected $kernel;

    /**
     * @var EntityManager $entityManager
     */
    protected $entityManager;

    /**
     * @var Symfony\Component\DependencyInjection\Container $container
     */
    protected $container;


    public function setUp()
    {
        $this->kernel = new \AppKernel('test', true);
        $this->kernel->boot();

        $this->container = $this->kernel->getContainer();
        $this->entityManager = $this->container->get('doctrine')->getManager();

        $this->generateSchema();

        parent::setUp();
    }

    public function tearDown()
    {
        $this->kernel->shutdown();

        parent::tearDown();
    }

    protected function generateSchema()
    {
        $metadatas = $this->getMetadatas();

        if ( ! empty($metadatas)) {
            $tool = new SchemaTool($this->entityManager);
            $tool->dropSchema($metadatas);
            $tool->updateSchema($metadatas);
        } else {
            throw new Doctrine\DBAL\Schema\SchemaException('No Metadata Classes to process.');
        }
    }

    /**
     * Overwrite this method to get specific metadatas.
     *
     * @return array
     */
    protected function getMetadatas()
    {
        return $this->entityManager->getMetadataFactory()->getAllMetadata();
    }

    public function testStructureUpdate()
    {
        $this->assertTrue( true);
    }
}