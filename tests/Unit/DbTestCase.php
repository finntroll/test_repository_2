<?php

namespace Tests\Unit;

use Doctrine;
use PHPUnit\DbUnit\Database\DefaultConnection;
use PHPUnit\DbUnit\DataSet\CompositeDataSet;
use PHPUnit\DbUnit\TestCaseTrait;
use Symfony;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;

abstract class DbTestCase extends WebTestCase
{
    use TestCaseTrait;

    public $fixtures = array();

    /**
     * @var \PDO
     */
    static private $pdo;

    /**
     * @var DefaultConnection
     */
    private $conn;

    /**
     * @var null|Symfony\Component\DependencyInjection\ContainerInterface
     */
    protected $container;

    /** @var Symfony\Bundle\FrameworkBundle\Client */
    protected $client;

    /**
     * @inheritdoc
     */
    public function __construct($name = null, array $data = [], $dataName = '')
    {
        parent::__construct($name, $data, $dataName);
        $this->prepare();
    }

    /**
     * тут готовим кухню - коннекшен и всё прочее
     * Можно тут же и зачистить базу или обработать моки.
     * Поскольку общий метод setUpBeforeClass статический - потому тут появился этот новый метод
     */
    protected function prepare()
    {
        /** @var Symfony\Bundle\FrameworkBundle\Client client */
        $this->client    = static::createClient();
        $this->container = $this->client->getContainer();
    }


    /**
     * @return string
     */
    protected function getDsn()
    {
        return sprintf(
            'mysql:host=%s;port=%s;dbname=%s;charset=UTF8',
            $this->container->getParameter('database_host'),
            $this->container->getParameter('database_port'),
            $this->container->getParameter('database_name')
        );
    }

    /**
     * @return null|\PHPUnit\DbUnit\Database\DefaultConnection
     */
    public function getConnection()
    {
        if ($this->conn === null) {
            if (self::$pdo == null) {
                self::$pdo = new \PDO(
                    $this->getDsn(),
                    $this->container->getParameter('database_user'),
                    $this->container->getParameter('database_password')
                );
            }
            $this->conn = $this->createDefaultDBConnection(self::$pdo);
        }

        return $this->conn;
    }

    /**
     * @inheritdoc
     */
    public function getDataSet()
    {
        $compositeDs = new CompositeDataSet();
        $fixturePath = __DIR__ . '/../Fixtures/';

        foreach ($this->fixtures as $fixture) {
            $path = $fixturePath . "$fixture.xml";
            $ds   = $this->createXMLDataSet($path);
            $compositeDs->addDataSet($ds);
        }
        return $compositeDs;
    }
}