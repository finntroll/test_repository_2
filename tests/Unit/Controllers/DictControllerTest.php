<?php

namespace Tests\Unit\Controllers;

use Tests\Unit\DbTestCase;
use PHPUnit\DbUnit\TestCaseTrait;

class DictControllerTest extends DbTestCase
{
    use TestCaseTrait;


    public $fixtures = [
        'attribute_group',
        'attribute',
        'attribute_option',
        'producer',
        'category',
        'product_model',
        'product',
        'country',
        'region',
        'city'
    ];

    /**
     *
     */
    public function testGetAttributeGroupsAction(): void
    {
        $this->client->request('GET', '/dict/attr/groups');
        $response = $this->client->getResponse();

        $responseArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(4, $responseArray);

        $this->assertTrue($response->headers->has('x-pagination-page-count'));
        $this->assertEquals(1, $response->headers->get('x-pagination-page-count'));

        $this->assertTrue($response->headers->has('um-object-hash'));
        $this->assertEquals('6937112801f1efced46bf9e7caa3edd3', $response->headers->get('um-object-hash'));

        $group = $responseArray[0];

        $this->assertObjectHasAttribute('id', $group);
        $this->assertEquals(1, $group->id);

        $this->assertObjectHasAttribute('title', $group);
        $this->assertEquals('Антенна', $group->title);

        $this->assertObjectHasAttribute('sort', $group);
        $this->assertEquals(184, $group->sort);
    }

    public function testGetAttributesAction(): void
    {
        $this->client->request('GET', '/dict/attr/attributes');
        $response = $this->client->getResponse();

        $responseArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(5, $responseArray);

        $this->assertTrue($response->headers->has('x-pagination-page-count'));
        $this->assertEquals(1, $response->headers->get('x-pagination-page-count'));

        $this->assertTrue($response->headers->has('um-object-hash'));
        $this->assertEquals('c1883d3902af5d62a622c4a930dd0600', $response->headers->get('um-object-hash'));

        $attribute = $responseArray[0];

        $this->assertObjectHasAttribute('id', $attribute);
        $this->assertEquals(2, $attribute->id);
        $this->assertObjectHasAttribute('group_id', $attribute);
        $this->assertEquals(70, $attribute->group_id);
        $this->assertObjectHasAttribute('title', $attribute);
        $this->assertEquals('Функция быстрой зарядки', $attribute->title);
        $this->assertObjectHasAttribute('type', $attribute);
        $this->assertEquals(3, $attribute->type);
        $this->assertObjectHasAttribute('unit', $attribute);
        $this->assertEquals('', $attribute->unit);
        $this->assertObjectHasAttribute('sort', $attribute);
        $this->assertEquals(1492, $attribute->sort);

        $attribute = $responseArray[2];

        $this->assertObjectHasAttribute('id', $attribute);
        $this->assertEquals(6, $attribute->id);
        $this->assertObjectHasAttribute('group_id', $attribute);
        $this->assertEquals(124, $attribute->group_id);
        $this->assertObjectHasAttribute('title', $attribute);
        $this->assertEquals('Тип матрицы экрана', $attribute->title);
        $this->assertObjectHasAttribute('type', $attribute);
        $this->assertEquals(2, $attribute->type);
        $this->assertObjectHasAttribute('unit', $attribute);
        $this->assertEmpty($attribute->unit);
        $this->assertEquals('', $attribute->unit);
        $this->assertObjectHasAttribute('sort', $attribute);
        $this->assertEquals(6, $attribute->sort);


    }

    /**
     *
     */
    public function testGetAttributeOptionsAction(): void
    {
        $this->client->request('GET', '/dict/attr/options');
        $response = $this->client->getResponse();

        $responseArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(4, $responseArray);

        $this->assertTrue($response->headers->has('x-pagination-page-count'));
        $this->assertEquals(1, $response->headers->get('x-pagination-page-count'));

        $this->assertTrue($response->headers->has('um-object-hash'));
        $this->assertEquals('6ac65411afcd186cadb8b0e22424752d', $response->headers->get('um-object-hash'));

        $option = $responseArray[0];

        $this->assertObjectHasAttribute('id', $option);
        $this->assertEquals(90, $option->id);
        $this->assertObjectHasAttribute('attribute_id', $option);
        $this->assertEquals(6, $option->attribute_id);
        $this->assertObjectHasAttribute('value', $option);
        $this->assertEquals('AMOLED', $option->value);
    }

    public function testGetCategoriesAction()
    {
        $this->client->request('GET', '/dict/categories');
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);

        $rootCategory = $responseDecodedArray[0];

        $this->assertObjectHasAttribute('id', $rootCategory);
        $this->assertEquals(5, $rootCategory->id);
        $this->assertObjectHasAttribute('title', $rootCategory);
        $this->assertEquals('Телефоны и гаджеты', $rootCategory->title);
        $this->assertObjectHasAttribute('image', $rootCategory);
        $this->assertObjectHasAttribute('sort', $rootCategory);
        $this->assertEquals(21, $rootCategory->sort);

        $childCategory = $responseDecodedArray[1];

        $this->assertObjectHasAttribute('id', $childCategory);
        $this->assertEquals(6, $childCategory->id);
        $this->assertObjectHasAttribute('title', $childCategory);
        $this->assertEquals('Телефоны', $childCategory->title);
        $this->assertObjectHasAttribute('image', $childCategory);
        $this->assertObjectHasAttribute('sort', $childCategory);
        $this->assertEquals(6, $childCategory->sort);
        $this->assertObjectHasAttribute('parent_id', $childCategory);
        $this->assertEquals(5, $childCategory->parent_id);
    }

    public function testGetProducersAction()
    {
        $this->client->request('GET', '/dict/producers');
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);

        $producer = $responseDecodedArray[0];

        $this->assertObjectHasAttribute('id', $producer);
        $this->assertEquals(1, $producer->id);
        $this->assertObjectHasAttribute('name', $producer);
        $this->assertEquals('Doogee', $producer->name);
    }

    public function testGetCountriesAction()
    {

        $this->client->request('GET', '/dict/countries');
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(3, $responseDecodedArray);

        $country = $responseDecodedArray[0];

        $this->assertObjectHasAttribute('id', $country);
        $this->assertEquals(1, $country->id);
        $this->assertObjectHasAttribute('title', $country);
        $this->assertEquals('Россия', $country->title);
    }

    public function testGetRegionsAction()
    {

        $this->client->request('GET', '/dict/regions');
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(5, $responseDecodedArray);

        $region = $responseDecodedArray[0];

        $this->assertObjectHasAttribute('id', $region);
        $this->assertEquals(1053480, $region->id);
        $this->assertObjectHasAttribute('country_id', $region);
        $this->assertEquals(1, $region->country_id);
        $this->assertObjectHasAttribute('title', $region);
        $this->assertEquals('Московская область', $region->title);
    }

    public function testGetImportantCitiesAction()
    {

        $this->client->request('GET', '/dict/cities/important');
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);

        $city = $responseDecodedArray[0];

        $this->assertObjectHasAttribute('id', $city);
        $this->assertEquals(151, $city->id);
        $this->assertObjectHasAttribute('area', $city);
        $this->assertEquals('Уфимский район', $city->area);
        $this->assertObjectHasAttribute('country_id', $city);
        $this->assertEquals(1, $city->country_id);
        $this->assertObjectHasAttribute('region_id', $city);
        $this->assertEquals(1004565, $city->region_id);
        $this->assertObjectHasAttribute('title', $city);
        $this->assertEquals('Уфа', $city->title);
    }

    public function testGetCitiesBySearchAction()
    {

        $this->client->request('POST', '/dict/cities/search', [], [], [],
                         '{
                         "filter_name": "у",
                         "region_id": 1004565,
                         "limit": 10
                         }');
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertCount(1, $responseDecodedArray);

        $city = $responseDecodedArray[0];

        $this->assertObjectHasAttribute('id', $city);
        $this->assertEquals(151, $city->id);
        $this->assertObjectHasAttribute('area', $city);
        $this->assertEquals('Уфимский район', $city->area);
        $this->assertObjectHasAttribute('country_id', $city);
        $this->assertEquals(1, $city->country_id);
        $this->assertObjectHasAttribute('region_id', $city);
        $this->assertEquals(1004565, $city->region_id);
        $this->assertObjectHasAttribute('title', $city);
        $this->assertEquals('Уфа', $city->title);
    }
}
