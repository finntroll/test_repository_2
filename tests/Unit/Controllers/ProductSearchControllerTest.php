<?php

namespace Tests\Unit\Controllers;

use Tests\Unit\DbTestCase;
use PHPUnit\DbUnit\TestCaseTrait;

class ProductSearchControllerTest extends DbTestCase
{
    use TestCaseTrait;

    public $fixtures = [
        'attribute_group',
        'attribute',
        'category',
        'producer',
        'product_model',
        'product',
        'attribute_values',
        'user'
    ];

    public function testGetSearchListAction()
    {
        $this->client->request('POST', '/search', ['category' => 6]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(3, $responseDecodedArray);

        $item = $responseDecodedArray[0]->product;

        $this->assertObjectHasAttribute('id', $item);
        $this->assertEquals(10002, $item->id);
        $this->assertObjectHasAttribute('name', $item);
        $this->assertEquals('Телефон Doogee X9 mini черный', $item->name);
        $this->assertObjectHasAttribute('price', $item);
        $this->assertEquals(4100, $item->price);
        $this->assertObjectHasAttribute('images', $item);
        $this->assertCount(1, $item->images);
        $this->assertObjectHasAttribute('preview', $item->images[0]);
        $this->assertObjectHasAttribute('large', $item->images[0]);
        $this->assertObjectHasAttribute('full', $item->images[0]);
        $this->assertObjectHasAttribute('sort', $item->images[0]);
    }

    public function testInStockListAction()
    {
        $this->client->request('POST', '/search', ['inStock' => false]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(3, $responseDecodedArray);
    }

    public function testProducersListAction()
    {
        $this->client->request('POST', '/search', ['producers' => [2]]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $responseDecodedArray);
    }

    public function testCheckboxListAction()
    {
        $this->client->request('POST', '/search', ['attributes' => [['id' => 2, 'type' => 'checkbox', 'checkbox' => false]]]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $responseDecodedArray);
    }

    public function testBetweenListAction()
    {
        $this->client->request('POST', '/search', ['attributes' => [['id' => 6, 'type' => 'between', 'between' => ['from' => 91, 'to' => 92]]]]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);
    }

    public function testOptionListAction()
    {
        $this->client->request('POST', '/search', ['attributes' => [['id' => 106, 'type' => 'option', 'option' => 'red']]]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $responseDecodedArray);
    }

    public function testOptionsListAction()
    {
        $this->client->request('POST', '/search', ['attributes' => [['id' => 106, 'type' => 'options', 'options' => ['red', 'white']]]]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);
    }

    public function testGetSearchModelListAction()
    {
        $this->client->request('POST', '/search/model', ['category' => 6]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);
    }

    public function testInStockModelListAction()
    {
        $this->client->request('POST', '/search/model', ['inStock' => true]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);
    }

    public function testProducersModelListAction()
    {
        $this->client->request('POST', '/search/model', ['producers' => [1]]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $responseDecodedArray);
    }

    public function testPriceFromModelListAction()
    {
        $this->client->request('POST', '/search/model', ['priceFrom' => 10000]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);
    }

    public function testPriceToModelListAction()
    {
        $this->client->request('POST', '/search/model', ['priceTo' => 10250]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $responseDecodedArray);
    }

    public function testCheckboxModelListAction()
    {
        $this->client->request('POST', '/search', ['attributes' => [['id' => 2, 'type' => 'checkbox', 'checkbox' => false]]]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $responseDecodedArray);
    }

    public function testBetweenModelListAction()
    {
        $this->client->request('POST', '/search', ['attributes' => [['id' => 6, 'type' => 'between', 'between' => ['from' => 91, 'to' => 92]]]]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);
    }

    public function testOptionModelListAction()
    {
        $this->client->request('POST', '/search', ['attributes' => [['id' => 106, 'type' => 'option', 'option' => 'red']]]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $responseDecodedArray);
    }

    public function testOptionsModelListAction()
    {
        $this->client->request('POST', '/search', ['attributes' => [['id' => 106, 'type' => 'options', 'options' => ['red', 'white']]]]);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);
    }

    public function testGetSearchAction()
    {
        $this->client->request('POST', '/search', ['query' => 'телефон']);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(3, $responseDecodedArray);

        $this->client->request('POST', '/search', ['query' => '   телефон     кра   ']);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(3, $responseDecodedArray);

        $this->client->request('POST', '/search', ['query' => '']);
        $response = $this->client->getResponse();
        $expectedJson = json_encode([]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString($expectedJson, $response->getContent());
    }

}
