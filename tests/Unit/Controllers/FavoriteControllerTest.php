<?php

namespace Tests\Unit\Controllers;

use Tests\Unit\DbTestCase;
use PHPUnit\DbUnit\TestCaseTrait;

class FavoriteControllerTest extends DbTestCase
{
    use TestCaseTrait;

    public $fixtures = [
        'attribute_group',
        'attribute',
        'category',
        'producer',
        'product_model',
        'product',
        'attribute_values',
        'user',
        'user_token',
        'user_favorite'
    ];

    public function testGetCartAction()
    {
        $this->client->request('GET', '/favorites', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);
    }

    public function testAddToCartAction()
    {
        $this->client->request('PUT', '/favorites', ['product_id' => 10004], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $this->client->request('GET', '/favorites', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(3, $responseDecodedArray);
    }

    public function testRemoveFromCartAction()
    {
        $this->client->request('DELETE', '/favorites', ['product_id' => 10002], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $this->client->request('GET', '/favorites', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $responseDecodedArray);
    }
}
