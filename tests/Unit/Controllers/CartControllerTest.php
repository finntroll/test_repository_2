<?php

namespace Tests\Unit\Controllers;

use Tests\Unit\DbTestCase;
use PHPUnit\DbUnit\TestCaseTrait;

class CartControllerTest extends DbTestCase
{
    use TestCaseTrait;

    public $fixtures = [
        'attribute_group',
        'attribute',
        'category',
        'producer',
        'product_model',
        'product',
        'rate',
        'attribute_values',
        'user',
        'user_token',
        'cart_product'
    ];

    public function testGetCartAction()
    {
        $this->client->request('GET', '/cart', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertObjectHasAttribute('items', $responseDecodedArray);

        $items = $responseDecodedArray->items;
        $this->assertCount(2, $items);

        $item = $items[0];
        $this->assertObjectHasAttribute('count', $item);
        $this->assertEquals(50, $item->count);
        $this->assertObjectHasAttribute('product', $item);

        $item = $items[1];
        $this->assertObjectHasAttribute('count', $item);
        $this->assertEquals(40, $item->count);
        $this->assertObjectHasAttribute('product', $item);
    }

    public function testAddToCartAction()
    {
        $this->client->request('PUT', '/cart', ['product_id' => 10002, 'count' => 50], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertObjectHasAttribute('items', $responseDecodedArray);

        $items = $responseDecodedArray->items;
        $this->assertCount(2, $items);

        $item = $items[0];
        $this->assertObjectHasAttribute('count', $item);
        $this->assertEquals(100, $item->count);

        $this->client->request('PUT', '/cart', ['product_id' => 10003, 'count' => 25], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertObjectHasAttribute('items', $responseDecodedArray);

        $items = $responseDecodedArray->items;
        $this->assertCount(3, $items);
    }

    public function testUpdateCartAction()
    {
        $this->client->request('POST', '/cart', ['product_id' => 10002, 'count' => 10], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertObjectHasAttribute('items', $responseDecodedArray);

        $items = $responseDecodedArray->items;
        $this->assertCount(2, $items);

        $item = $items[0];
        $this->assertObjectHasAttribute('count', $item);
        $this->assertEquals(10, $item->count);

        $this->client->request('POST', '/cart', ['product_id' => 10003, 'count' => 10], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertObjectHasAttribute('items', $responseDecodedArray);

        $items = $responseDecodedArray->items;
        $this->assertCount(3, $items);
    }

    public function testRemoveFromCartAction()
    {
        $this->client->request('DELETE', '/cart', ['product_id' => 10002], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertObjectHasAttribute('items', $responseDecodedArray);

        $items = $responseDecodedArray->items;
        $this->assertCount(1, $items);
    }
}
