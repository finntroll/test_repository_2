<?php

namespace Tests\Unit\Controllers;

use PHPUnit\DbUnit\TestCaseTrait;
use Tests\Unit\DbTestCase;

/**
 * Class ReviewControllerTest
 *
 * @package Tests\Unit\Controllers
 */
class ReviewControllerTest extends DbTestCase
{
    use TestCaseTrait;

    public $fixtures = [
        'category',
        'producer',
        'product_model',
        'product',
        'user',
        'user_token',
        'product_reviews',
        'review_image',
    ];

    /**
     *
     */
    public function testGetReviewListAction()
    {
        $this->client->request('POST', '/reviews/10002', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();


        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJson($response->getContent());

        $object = json_decode($response->getContent());
        $this->assertCount(3, $object);

        $review = $object[0];

        $this->assertObjectHasAttribute('id', $review);
        $this->assertEquals(801, $review->id);
        $this->assertObjectHasAttribute('product_id', $review);
        $this->assertEquals(10002, $review->product_id);
        $this->assertObjectHasAttribute('user_name', $review);
        $this->assertEquals('Парень', $review->user_name);
        $this->assertObjectHasAttribute('text', $review);
        $this->assertEquals('Было бы круто, если бы не сломалось через 21 секунды', $review->text);
        $this->assertObjectHasAttribute('create_time', $review);
        $this->assertEquals('2018-05-09 18:00:21', $review->create_time);
        $this->assertObjectHasAttribute('rating', $review);
        $this->assertEquals(0, $review->rating);
        $this->assertObjectHasAttribute('images', $review);
    }

    public function testCreateReviewAction()
    {
        $this->client->request('PUT', '/reviews/10002', [
            'rating' => 50,
            'text' => 'Знатный товар'
        ], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);

        $response = $this->client->getResponse();

        $this->assertEquals(400, $response->getStatusCode());

        $this->client->request('PUT', '/reviews/10002', [
            'rating' => 5,
            'text' => 'Знатный товар'
        ], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);

        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $review = json_decode($response->getContent());

        $this->assertObjectHasAttribute('id', $review);
        $this->assertEquals(802, $review->id);
        $this->assertObjectHasAttribute('product_id', $review);
        $this->assertEquals(10002, $review->product_id);
        $this->assertObjectHasAttribute('user_name', $review);
        $this->assertEquals('Рамзаньчик', $review->user_name);
        $this->assertObjectHasAttribute('text', $review);
        $this->assertEquals('Знатный товар', $review->text);
        $this->assertObjectHasAttribute('create_time', $review);
        $this->assertNotEmpty($review->create_time);
        $this->assertObjectHasAttribute('rating', $review);
        $this->assertEquals(5, $review->rating);
        $this->assertObjectHasAttribute('user_is_owner', $review);
        $this->assertTrue($review->user_is_owner);
        $this->assertObjectHasAttribute('images', $review);
    }

    /**
     *
     */
    public function testDeleteReviewAction()
    {
        $this->client->request('DELETE', '/reviews/801', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();

        $this->assertEquals(403, $response->getStatusCode());

        $this->client->request('DELETE', '/reviews/798', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());
    }

    /**
     *
     */
    public function testUpdateReviewAction()
    {
        $this->client->request('PATCH', '/reviews/802', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $this->assertEquals(404, $response->getStatusCode());

        $this->client->request('PATCH', '/reviews/801', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $this->assertEquals(403, $response->getStatusCode());

        $this->client->request('PATCH', '/reviews/799', [
            'product_id' => 10231002,
        ], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $this->assertEquals(403, $response->getStatusCode());

        $this->client->request('PATCH', '/reviews/798', [
            'product_id' => 10231002,
        ], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());

        $this->client->request('PATCH', '/reviews/798', [
            'rating' => 500
        ], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $this->assertEquals(400, $response->getStatusCode());

        $this->client->request('PATCH', '/reviews/798', [
            'product_id' => 10003,
            'rating'     => 2,
            'text'       => 'How are you?',
            'images'     => [
                'c35d723ed920d18aaf419f410cf4baffd6e4c657.jpg',
                '2d1753856983c85800466a60e6f8c5bb73b39d38.jpg'
            ],
        ], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());


        $review = json_decode($response->getContent());

        $this->assertObjectHasAttribute('id', $review);
        $this->assertEquals(798, $review->id);
        $this->assertObjectHasAttribute('product_id', $review);
        $this->assertEquals(10003, $review->product_id);
        $this->assertObjectHasAttribute('user_name', $review);
        $this->assertEquals('Рома', $review->user_name);
        $this->assertObjectHasAttribute('text', $review);
        $this->assertEquals('How are you?', $review->text);
        $this->assertObjectHasAttribute('create_time', $review);
        $this->assertNotEmpty($review->create_time);
        $this->assertObjectHasAttribute('rating', $review);
        $this->assertEquals(2, $review->rating);
        $this->assertObjectHasAttribute('user_is_owner', $review);
        $this->assertTrue($review->user_is_owner);
        $this->assertObjectHasAttribute('images', $review);
        $this->assertCount(2, $review->images);
    }

}
