<?php

namespace Tests\Unit\Controllers;

use Tests\Unit\DbTestCase;
use PHPUnit\DbUnit\TestCaseTrait;

class CatalogControllerTest extends DbTestCase
{
    use TestCaseTrait;

    public $fixtures = [
        'attribute_group',
        'attribute',
        'attribute_option',
        'category',
        'producer',
        'product_model',
        'product',
        'attribute_values',
    ];

    public function testViewAction()
    {
        $this->client->request('POST', '/catalog/6', [], [], [], json_encode([]));
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(3, $responseDecodedArray);

        $item = $responseDecodedArray[0];

        $this->assertObjectHasAttribute('id', $item);
        $this->assertEquals(10002, $item->id);
        $this->assertObjectHasAttribute('name', $item);
        $this->assertEquals('Телефон Doogee X9 mini черный', $item->name);
        $this->assertObjectHasAttribute('price', $item);
        $this->assertEquals(4100, $item->price);
        $this->assertObjectHasAttribute('images', $item);
        $this->assertCount(1, $item->images);
        $this->assertObjectHasAttribute('preview', $item->images[0]);
        $this->assertObjectHasAttribute('large', $item->images[0]);
        $this->assertObjectHasAttribute('full', $item->images[0]);
        $this->assertObjectHasAttribute('sort', $item->images[0]);
    }

    public function testInStockFilter()
    {
        $this->client->request('POST', '/catalog/6', [], [], [], json_encode(['in_stock' => false]));
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(3, $responseDecodedArray);
    }

    public function testProducersFilter()
    {
        $this->client->request('POST', '/catalog/6', [], [], [], json_encode(['producers' => [2]]));
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $responseDecodedArray);
    }

    public function testPriceFilter()
    {
        $this->client->request('POST', '/catalog/6', [], [], [], json_encode(['price' => ['from' => 10000, 'to' => 10250]]));
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);
    }

    public function testCheckboxAttributesFilter()
    {
        $this->client->request('POST', '/catalog/6', [], [], [], json_encode(['attributes' => [['id' => 2, 'type' => 'checkbox', 'checkbox' => false]]]));
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $responseDecodedArray);
    }

    public function testBetweenAttributesFilter()
    {
        $this->client->request('POST', '/catalog/6', [], [], [], json_encode(['attributes' => [['id' => 6, 'type' => 'between', 'between' => ['from' => 91, 'to' => 92]]]]));
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);
    }

    public function testOptionAttributesFilter()
    {
        $this->client->request('POST', '/catalog/6', [], [], [], json_encode(['attributes' => [['id' => 106, 'type' => 'option', 'option' => 'red']]]));
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(1, $responseDecodedArray);
    }

    public function testOptionsAttributesFilter()
    {
        $this->client->request('POST', '/catalog/6', [], [], [], json_encode(['attributes' => [['id' => 106, 'type' => 'options', 'options' => ['red', 'white']]]]));
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);
    }

    public function testSortingFilter()
    {
        $this->client->request('POST', '/catalog/6', [], [], [], json_encode(['sorting' => ['param' => 'id', 'direction' => 'desc']]));
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(3, $responseDecodedArray);

        $item = $responseDecodedArray[0];

        $this->assertObjectHasAttribute('id', $item);
        $this->assertEquals(10004, $item->id);
    }

    public function testFailViewAction()
    {
        $this->client->request('POST', '/catalog/505');
        $response     = $this->client->getResponse();
        $expectedJson = json_encode(['status' => 404, 'message' => 'AppBundle\\Entity\\Category object not found.']);

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString($expectedJson, $response->getContent());
    }
}
