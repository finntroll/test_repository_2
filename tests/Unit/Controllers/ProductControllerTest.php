<?php

namespace Tests\Unit\Controllers;

use Tests\Unit\DbTestCase;
use PHPUnit\DbUnit\TestCaseTrait;

class ProductControllerTest extends DbTestCase
{
    use TestCaseTrait;

    public $fixtures = [
        'attribute_group',
        'attribute',
        'category',
        'producer',
        'product_model',
        'product',
        'rate',
        'attribute_values',
        'user',
        'product_reviews'
    ];

    public function testNotFoundProduct()
    {
        $this->client->request('GET', '/product/100500');
        $response     = $this->client->getResponse();
        $expectedJson = json_encode(['status' => 404, 'message' => 'Product not found']);

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString($expectedJson, $response->getContent());
    }

    public function testViewAction()
    {
        $this->client->request('GET', '/product/10002');
        $response = $this->client->getResponse();
        $this->assertEquals(200, $response->getStatusCode());

        $this->client->request('GET', '/product/10002', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();

        $responseDecodedArray = (array)json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertCount(26, $responseDecodedArray);
        $this->assertArrayHasKey('id', $responseDecodedArray);
        $this->assertArrayHasKey('name', $responseDecodedArray);
        $this->assertArrayHasKey('type_id', $responseDecodedArray);
        $this->assertArrayHasKey('category_id', $responseDecodedArray);
        $this->assertArrayHasKey('sku', $responseDecodedArray);
        $this->assertArrayHasKey('discount', $responseDecodedArray);
        $this->assertArrayHasKey('extra_charge', $responseDecodedArray);
        $this->assertArrayHasKey('description', $responseDecodedArray);
        $this->assertArrayHasKey('producer_id', $responseDecodedArray);
        $this->assertArrayHasKey('height', $responseDecodedArray);
        $this->assertArrayHasKey('width', $responseDecodedArray);
        $this->assertArrayHasKey('length', $responseDecodedArray);
        $this->assertArrayHasKey('weight', $responseDecodedArray);
        $this->assertArrayHasKey('images', $responseDecodedArray);
        $this->assertArrayHasKey('attributes', $responseDecodedArray);
        $this->assertArrayHasKey('in_stock', $responseDecodedArray);
        $this->assertArrayHasKey('external_id', $responseDecodedArray);
        $this->assertArrayHasKey('price', $responseDecodedArray);
        $this->assertArrayHasKey('reviews', $responseDecodedArray);
        $this->assertArrayHasKey('rating', $responseDecodedArray);
        $this->assertArrayHasKey('in_favorites', $responseDecodedArray);

        $imagesArray = (array)$responseDecodedArray['images'][0];

        $this->assertCount(4, $imagesArray);
        $this->assertArrayHasKey('preview', $imagesArray);
        $this->assertArrayHasKey('large', $imagesArray);
        $this->assertArrayHasKey('full', $imagesArray);
        $this->assertArrayHasKey('sort', $imagesArray);


        $this->assertCount(3, $responseDecodedArray['attributes']);
        $this->assertEquals(\gettype([]), \gettype($responseDecodedArray['reviews']));
        $reviews = $responseDecodedArray['reviews'];
        if (\count($reviews) > 0) {
            foreach ($reviews as $review) {
                $this->assertObjectHasAttribute('user_name', $review);
                $this->assertObjectHasAttribute('text', $review);
            }
        }
    }

    public function testNotFoundSimilarAction()
    {
        $this->client->request('GET', '/product/similar/100500');
        $response     = $this->client->getResponse();
        $expectedJson = json_encode(['status' => 404, 'message' => 'Similar products not found']);

        $this->assertEquals(404, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString($expectedJson, $response->getContent());
    }

    public function testSimilar()
    {
        $this->client->request('GET', '/product/similar/10002');
        $response     = $this->client->getResponse();
        $expectedJson = json_encode(['attributes' => ['14033' => ['106' => "black"], '14034' => ['106' => "beige"], '14036' => ['106' => "blue"]]]);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertJsonStringEqualsJsonString($expectedJson, $response->getContent());
    }
}
