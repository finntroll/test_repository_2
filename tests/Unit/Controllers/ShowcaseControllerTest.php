<?php

namespace Tests\Unit\Controllers;

use Tests\Unit\DbTestCase;
use PHPUnit\DbUnit\TestCaseTrait;

class ShowcaseControllerTest extends DbTestCase
{
    use TestCaseTrait;

    public $fixtures = [
        'category',
        'producer',
        'product_model',
        'product',
        'showcase'
    ];

    public function testViewAction()
    {

        $this->client->request('GET', '/showcase/2');
        $response = $this->client->getResponse();

        $responseDecodedArray = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(2, $responseDecodedArray);

        $item = $responseDecodedArray[0];

        $this->assertObjectHasAttribute('id', $item);
        $this->assertEquals(10002, $item->id);
        $this->assertObjectHasAttribute('name', $item);
        $this->assertEquals('Телефон Doogee X9 mini черный', $item->name);
        $this->assertObjectHasAttribute('price', $item);
        $this->assertEquals(4100, $item->price);
        $this->assertObjectHasAttribute('images', $item);
        $this->assertCount(1, $item->images);
        $this->assertObjectHasAttribute('preview', $item->images[0]);
        $this->assertObjectHasAttribute('large', $item->images[0]);
        $this->assertObjectHasAttribute('full', $item->images[0]);
        $this->assertObjectHasAttribute('sort', $item->images[0]);
    }
}
