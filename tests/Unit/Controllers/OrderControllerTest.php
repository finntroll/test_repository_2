<?php

namespace Tests\Unit\Controllers;

use Tests\Unit\DbTestCase;
use PHPUnit\DbUnit\TestCaseTrait;

class OrderControllerTest extends DbTestCase
{
    use TestCaseTrait;

    public $fixtures = [
        'user',
        'order',
        'user_token',
        'attribute_group',
        'attribute',
        'category',
        'producer',
        'product_model',
        'product',
        'attribute_values',
        'cart_product'
    ];


    public function testGetListAction()
    {
        $this->client->request('GET', '/orders', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $responseDecodedObject = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertCount(4, $responseDecodedObject);

        $order = $responseDecodedObject[0];

        $this->assertObjectHasAttribute('id', $order);
        $this->assertEquals(41929, $order->id);
        $this->assertObjectHasAttribute('status', $order);
        $this->assertEquals('deleted', $order->status);
        $this->assertObjectHasAttribute('date_time', $order);
        $this->assertEquals('2018-02-08 03:45:04', $order->date_time);
        $this->assertObjectHasAttribute('cost', $order);
        $this->assertEquals(8410, $order->cost);
    }

    public function testSubmitOrderAction()
    {
        $this->client->request('PUT', '/orders',
                               [
                                   'address_city' => 'test',
                                   'address_country' => 'test',
                                   'address_full_address' => 'test',
                                   'address_region' => 'test',
                                   'address_zip_code' => '123123',
                                   'contact_email' => 'test@test.ru',
                                   'contact_full_name' => 'test test',
                                   'contact_phone' => '+79999999999',
                                   'contact_tin' => '1234567891'
                               ], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $order = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertObjectHasAttribute('id', $order);
        $this->assertEquals(41986, $order->id);
        $this->assertObjectHasAttribute('status', $order);
        $this->assertEquals('new', $order->status);
        $this->assertObjectHasAttribute('cost', $order);
        $this->assertEquals(369000, $order->cost);
    }

    public function testGetOrderAction()
    {
        $this->client->request('GET', '/orders/1', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $this->assertEquals(404, $response->getStatusCode());

        $this->client->request('GET', '/orders/41936', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $order = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertObjectHasAttribute('id', $order);
        $this->assertEquals(41936, $order->id);
        $this->assertObjectHasAttribute('status', $order);
        $this->assertEquals('in_work', $order->status);
        $this->assertObjectHasAttribute('date_time', $order);
        $this->assertEquals('2018-02-08 03:45:03', $order->date_time);
        $this->assertObjectHasAttribute('cost', $order);
        $this->assertEquals(1600, $order->cost);
        $this->assertObjectHasAttribute('url', $order);
        $this->assertEquals('2cce6eeeba0c606054f0aea504815f1f', $order->url);
        $this->assertObjectHasAttribute('address', $order);
        $this->assertObjectHasAttribute('contact', $order);

        $address = $order->address;
        $contact = $order->contact;

        $this->assertObjectHasAttribute('city', $address);
        $this->assertEquals('Москва', $address->city);
        $this->assertObjectHasAttribute('country', $address);
        $this->assertEquals('Россия', $address->country);
        $this->assertObjectHasAttribute('full_address', $address);
        $this->assertEquals('Московская, д.15, кв.234', $address->full_address);
        $this->assertObjectHasAttribute('region', $address);
        $this->assertEquals('холмы', $address->region);
        $this->assertObjectHasAttribute('zip_code', $address);
        $this->assertEquals('321123', $address->zip_code);
        $this->assertObjectHasAttribute('email', $contact);
        $this->assertEquals('ie@6.ru', $contact->email);
        $this->assertObjectHasAttribute('full_name', $contact);
        $this->assertEquals('Ишак Каши', $contact->full_name);
        $this->assertObjectHasAttribute('phone', $contact);
        $this->assertEquals('+76969696969', $contact->phone);
        $this->assertObjectHasAttribute('tin', $contact);
        $this->assertEquals(6969696969, $contact->tin);
    }

    public function testDeleteOrderAction()
    {
        $this->client->request('DELETE', '/orders/1', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $this->assertEquals(404, $response->getStatusCode());

        $this->client->request('DELETE', '/orders/41936', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();
        $order = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertObjectHasAttribute('status', $order);
        $this->assertEquals('deleted', $order->status);
    }
}
