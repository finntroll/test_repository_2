<?php

namespace Tests\Unit\Controllers;

use Tests\Unit\DbTestCase;
use PHPUnit\DbUnit\TestCaseTrait;

class StateControllerTest extends DbTestCase
{
    use TestCaseTrait;

    public $fixtures = [];


    public function testIndexAction()
    {
        $this->client->request('GET', '/state');
        $response = $this->client->getResponse();

        $responseDecodedObject = json_decode($response->getContent());
        $this->assertObjectHasAttribute('object_hashes', $responseDecodedObject);

        $objectHashes = $responseDecodedObject->object_hashes;

        $this->assertObjectHasAttribute('delivery_times', $responseDecodedObject);
        $deliveryTimes = $responseDecodedObject->delivery_times;

        $this->assertObjectHasAttribute('attr_groups', $objectHashes);
        $this->assertEquals('6937112801f1efced46bf9e7caa3edd3', $objectHashes->attr_groups);
        $this->assertObjectHasAttribute('attr_attributes', $objectHashes);
        $this->assertEquals('c1883d3902af5d62a622c4a930dd0600', $objectHashes->attr_attributes);
        $this->assertObjectHasAttribute('attr_options', $objectHashes);
        $this->assertEquals('6ac65411afcd186cadb8b0e22424752d', $objectHashes->attr_options);
        $this->assertObjectHasAttribute('categories', $objectHashes);
        $this->assertEquals('cd80fc85ffe27c8743ce4a97bd88ce33', $objectHashes->categories);
        $this->assertObjectHasAttribute('cities', $objectHashes);
        $this->assertEquals('7e0b8200f8f91660a53a73c1e7b6015c', $objectHashes->cities);
        $this->assertObjectHasAttribute('countries', $objectHashes);
        $this->assertEquals('012129372010eb8661cf63622d4ab7cf', $objectHashes->countries);
        $this->assertObjectHasAttribute('producers', $objectHashes);
        $this->assertEquals('50ab72c765e5459d873fc48c17f688cc', $objectHashes->producers);
        $this->assertObjectHasAttribute('regions', $objectHashes);
        $this->assertEquals('4708b2ab289494c695c380d22cc821d5', $objectHashes->regions);

        $this->assertObjectHasAttribute('global_crossborder', $deliveryTimes);

        $this->assertObjectHasAttribute('from', $deliveryTimes->global_crossborder);
        $this->assertEquals(15, $deliveryTimes->global_crossborder->from);
        $this->assertObjectHasAttribute('to', $deliveryTimes->global_crossborder);
        $this->assertEquals(60, $deliveryTimes->global_crossborder->to);

        $this->assertObjectHasAttribute('global_fast', $deliveryTimes);

        $this->assertObjectHasAttribute('from', $deliveryTimes->global_fast);
        $this->assertEquals(5, $deliveryTimes->global_fast->from);
        $this->assertObjectHasAttribute('to', $deliveryTimes->global_fast);
        $this->assertEquals(21, $deliveryTimes->global_fast->to);

    }
}
