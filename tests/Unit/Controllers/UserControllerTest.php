<?php

namespace Tests\Unit\Controllers;

use Tests\Unit\DbTestCase;
use PHPUnit\DbUnit\TestCaseTrait;

/**
 * Class UserControllerTest
 *
 * @package Tests\Unit\Controllers
 */
class UserControllerTest extends DbTestCase
{
    use TestCaseTrait;

    public $fixtures = [
        'user',
        'user_token'
    ];

    public function testLoginAction(): void
    {
        $params = [
            'email'    => 'ramzan@ya.ru',
            'password' => 'ramzan666'
        ];
        $this->client->request('POST', '/auth/login', $params);
        $response = $this->client->getResponse();

        $responseDecodedObject = json_decode($response->getContent());
        $this->assertObjectHasAttribute('cookie_token', $responseDecodedObject);
        $this->assertObjectHasAttribute('api_token', $responseDecodedObject);
        $this->assertObjectHasAttribute('expire_time', $responseDecodedObject);
    }

    /**
     *
     */
    public function testAuthDeviceIdAction(): void
    {
        $params = [
            'device_id' => 12345678
        ];

        $this->client->request('POST', '/auth/device_id', $params);
        $response = $this->client->getResponse();

        $this->assertEquals(200, $response->getStatusCode());

        $object = json_decode($response->getContent());
        $this->assertObjectHasAttribute('cookie_token', $object);
        $this->assertObjectHasAttribute('api_token', $object);
        $this->assertObjectHasAttribute('expire_time', $object);

        $this->client->request('GET', '/profile', [], [], ['HTTP_Um-Access-Token' => $object->api_token]);
        $response = $this->client->getResponse();

        $user = json_decode($response->getContent());
        $this->assertObjectHasAttribute('id', $user);
        $this->assertEquals(888, $user->id);
    }

    /**
     *
     */
    public function testProfileAction(): void
    {
        $this->client->request('GET', '/profile', [], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $response = $this->client->getResponse();

        $object = json_decode($response->getContent());

        $this->assertEquals(200, $response->getStatusCode());

        $this->assertObjectHasAttribute('id', $object);
        $this->assertInternalType('int', $object->id);

        $this->assertObjectHasAttribute('account_type', $object);
        $this->assertInternalType('int', $object->account_type);
        $this->assertContains($object->account_type, [0, 1, 2]);

        $this->assertObjectHasAttribute('first_name', $object);
        $this->assertInternalType('string', $object->first_name);
        $this->assertNotEmpty($object->first_name);

        $this->assertObjectHasAttribute('nick_name', $object);

        $this->assertObjectHasAttribute('email', $object);
        $this->assertInternalType('string', $object->email);
        $this->assertRegExp('/^.+\@\S+\.\S+$/', $object->email);

        $this->assertObjectHasAttribute('gender', $object);
        $this->assertInternalType('int', $object->gender);
        $this->assertContains($object->gender, [0, 1, 2]);

        $this->assertObjectHasAttribute('status', $object);
        $this->assertInternalType('int', $object->status);
        $this->assertContains($object->status, [0, 1, 2, 3]);

        $this->assertObjectHasAttribute('access_level', $object);
        $this->assertInternalType('int', $object->access_level);
        $this->assertContains($object->access_level, [0, 1]);

        $this->assertObjectHasAttribute('update_time', $object);

        $this->assertObjectHasAttribute('create_time', $object);
    }

    /**
     *
     */
    public function testEditProfileAction(): void
    {
        $this->client->request(
            'PUT',
            '/profile',
            [
                'first_name'  => 'Иван',
                'last_name'   => 'Иванов',
                'middle_name' => 'Иванович',
                'nick_name'   => 'ivan',
                'email'       => 'ivan@gmail.com',
                'gender'      => 2,
                'phone'       => '+79000000000',
                'birth_date'  => [
                    'year'  => '1985',
                    'month' => '1',
                    'day'   => '15',
                ]
            ],
            [],
            ['HTTP_Um-Access-Token' => 'api_t0ken']
        );

        $this->assertEquals(204, $this->client->getResponse()->getStatusCode());

        $this->client->request('PUT', '/profile', ['email' => 'wrong_email'], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->client->request('PUT', '/profile', ['first_name' => ''], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->client->request('PUT', '/profile', ['gender' => 100500], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->client->request('PUT', '/profile', ['phone' => 'NO PHONE HERE'], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());

        $this->client->request('PUT', '/profile', ['wrong_field' => 1337], [], ['HTTP_Um-Access-Token' => 'api_t0ken']);
        $this->assertEquals(400, $this->client->getResponse()->getStatusCode());
    }

}
